module Struct.List.Base where

open import Data.List public using (List; []; _∷_; [_]; _++_) renaming (map to Lmap)
open import Data.List using (reverse)
open import Data.List.Relation.Unary.Any public using (here; there)
open import Relation.Nullary using (¬_)
open import Base.Decidable

open import Data.List.Membership.Propositional
     public using (_∈_)

_∈?_[_] : {A : Set}(x : A) → (xs : List A) → ((x y : A) → Dec (x ≡ y)) → Dec (x ∈ xs)
x ∈? [] [ _≟_ ] = no λ ()
x ∈? y ∷ xs [ _≟_ ] with x ≟ y
(x ∈? .x ∷ xs [ _≟_ ]) | yes refl = yes (here refl)
(x ∈? y ∷ xs [ _≟_ ]) | no ¬x=y with x ∈? xs [ _≟_ ]
(x ∈? y ∷ xs [ _≟_ ]) | no ¬x=y | yes x∈xs = yes (there x∈xs)
(x ∈? y ∷ xs [ _≟_ ]) | no ¬x=y | no ¬x∈xs = no (¬cong-∈ ¬x∈xs ¬x=y)
  where
  ¬cong-∈ : {A : Set} → {x y : A} → {xs : List A} → ¬ (x ∈ xs) → ¬ (x ≡ y) → ¬ (x ∈ y ∷ xs)
  ¬cong-∈ ¬x∈xs ¬x=y (here refl) = ¬x=y refl
  ¬cong-∈ ¬x∈xs ¬x=y (there x)   = ¬x∈xs x

unique : {A : Set} → ((x y : A) → Dec (x ≡ y)) → List A → List A
unique _≟_ L = reverse (unique-acc _≟_ L [])
  where
  unique-acc : {A : Set} → ((x y : A) → Dec (x ≡ y)) → List A → List A → List A
  unique-acc _≟_ [] acc = acc
  unique-acc _≟_ (x ∷ L) acc with x ∈? acc [ _≟_ ]
  unique-acc _≟_ (x ∷ L) acc | yes p = unique-acc _≟_ L acc
  unique-acc _≟_ (x ∷ L) acc | no ¬p = unique-acc _≟_ L (x ∷ acc)

open import Data.List.Relation.Binary.Sublist.Propositional
     public using (_⊆_) renaming ([] to none; _∷ʳ_ to skip; _∷_ to take)

infix 10 _≡_+_
infixr 10 -o_ o-_
data _≡_+_ {A : Set} : (X A B : List A) → Set where
  [-]  : [] ≡ [] + []
  o-_ : ∀ {x}{X A B} → X ≡ A + B → (x ∷ X) ≡ (x ∷ A) + B
  -o_ : ∀ {x}{X A B} → X ≡ A + B → (x ∷ X) ≡ A + (x ∷ B)

mapL+ : {A B : Set}(f : A → List B) → List A → List B
mapL+ f [] = []
mapL+ f (x ∷ xs) = f x ++ (mapL+ f xs)
