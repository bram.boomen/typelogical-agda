module Struct.List.Decidable where
open import Struct.List
open import Base.Decidable public

private
  variable
    A : Set

_≡l_ : (t s : List A) → ((x y : A) → Dec (x ≡ y)) → Dec (t ≡ s)
([] ≡l []) _≟_ = yes refl
((x ∷ t) ≡l ( y ∷  s)) _≟_ with x ≟ y
((x ∷ t) ≡l (.x ∷  s)) _≟_ | yes refl with (t ≡l s) _≟_
((x ∷ t) ≡l (.x ∷ .t)) _≟_ | yes refl | yes refl = yes refl
((x ∷ t) ≡l (.x ∷  s)) _≟_ | yes refl | no ¬p = no (¬cong-v ¬p)
  where
  ¬cong-v : {x : A}{v1 v2 : List A} → ¬ (v1 ≡ v2) → ¬ ((x ∷ v1) ≡ (x ∷ v2))
  ¬cong-v ¬v=v refl = ¬v=v refl
((x ∷ t) ≡l ( y ∷  s)) _≟_ | no ¬p = no (¬cong-x ¬p)
  where
  ¬cong-x : {x1 x2 : A}{v1 v2 : List A} → ¬ (x1 ≡ x2) → ¬ ((x1 ∷ v1) ≡ (x2 ∷ v2))
  ¬cong-x ¬x=x refl = ¬x=x refl
([] ≡l (_ ∷ _)) _ = no (λ ())
((_ ∷ _) ≡l []) _ = no (λ ())
