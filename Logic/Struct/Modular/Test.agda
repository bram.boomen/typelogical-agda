module Struct.Modular.Test where

open import Agda.Builtin.Bool
open import Agda.Builtin.List
open import Agda.Builtin.Nat hiding (_==_)

_&&_ : Bool → Bool → Bool
false && q = false
true && q = q

record Eq (A : Set) : Set where
  field
    _==_ : A → A → Bool

open Eq {{...}} public

instance
  eqList : ∀ {A : Set} {{_ : Eq A}} → Eq (List A)
  _==_ {{eqList}} []       []       = true
  _==_ {{eqList}} (x ∷ xs) (y ∷ ys) = (x == y) && (xs == ys)
  _==_ {{eqList}} _        _        = false

  eqNat : Eq Nat
  _==_ {{eqNat}} = Agda.Builtin.Nat._==_

ex : ∀ {A} → {{_ : Eq A}} → A → A → Bool
ex x y = {!!}
