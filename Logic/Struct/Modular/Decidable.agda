module Struct.Modular.Decidable where

open import Relation.Binary.Structures using (IsEquivalence; IsPartialOrder)
open import Struct.Modular.Base
open import Relation.Nullary
open import Relation.Nullary.Decidable
open import Relation.Binary.PropositionalEquality

open isStruct {{...}}

record isDecStruct (Struct : Set → Set)(A : Set) : Set₁ where
  field
    {{IsStruct}} : isStruct Struct A
    _`∈?_   : (x : A)(X : Struct A) → Dec (x `∈ X)
    _`∈ʳ?_  : (x : A)(X : Struct A) → Dec (x `∈ʳ X)
    _`∈ˡ?_  : (x : A)(X : Struct A) → Dec (x `∈ˡ X)
    _`⊆?_   : (X Y : Struct A)      → Dec (X `⊆ Y)
    _`≅?_   : (X Y : Struct A)      → Dec (X `≅ Y)
    _`>?_/_ : (X Y Z : Struct A)    → Dec (X `> Y / Z)
