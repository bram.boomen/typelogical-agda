module Struct.Modular.Base where

record isStruct (Struct : Set → Set)(A : Set) : Set₁ where
  field
    `_𝟙_   : A → Struct A → Set
    _`≡_+_ : Struct A → Struct A → Struct A → Set
    _`∷_   : A → Struct A → Struct A
    _`∷ʳ_  : Struct A → A → Struct A

open isStruct
