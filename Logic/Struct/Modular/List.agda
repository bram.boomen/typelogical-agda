module Struct.Modular.List where

open import Base.Imports
open import Struct.List
open import Struct.Modular.Base

data _𝟙_ {A : Set}(x : A) : (Γ : List A) → Set where
  single : x 𝟙 (x ∷ [])

_∷ᴿ_ : {A : Set} → List A → A → List A
[] ∷ᴿ x = x ∷ []
(y ∷ X) ∷ᴿ x = y ∷ (X ∷ᴿ x)

instance
  List-isStruct : ∀ {A} → isStruct List A
  List-isStruct = record
    { `_𝟙_   = _𝟙_
    ; _`≡_+_ = _≡_+_
    ; _`∷_   = _∷_
    ; _`∷ʳ_  = _∷ᴿ_ }
