module Struct.Modular.eTree where

open import Struct.Tree.ETree
open import Struct.Modular.Base

data _𝟙_ {A : Set}(x : A) : (Γ : Tree A) → Set where
  single : x 𝟙 [ x ]

data _≡_+_ {A : Set} : (Ω Γ Δ : Tree A) → Set where
  concat : {Γ Δ : Tree A} → (Γ ∘ Δ) ≡ Γ + Δ

instance
  Tree-isStruct : ∀ {A} → isStruct Tree A
  Tree-isStruct = record
    { `_𝟙_   = _𝟙_
    ; _`≡_+_ = _≡_+_
    ; _`∷_   = λ x Γ → [ x ] ∘ Γ
    ; _`∷ʳ_  = λ Γ x → Γ ∘ [ x ] }
