{-# OPTIONS --rewriting #-}
module Struct.Tree.ETree where

open import Data.Nat
open import Base.Decidable
open import Struct.List using (List; []; _∷_; _++_)

data Tree (A : Set) : Set where
  ø   : Tree A
  [_] : (x : A) → Tree A
  _∙_ : Tree A → Tree A → Tree A

_∘_ : {A : Set} → Tree A → Tree A → Tree A
ø ∘ Y = Y
[ x ] ∘ ø = [ x ]
[ x ] ∘ [ y ] = [ x ] ∙ [ y ]
[ x ] ∘ (Y ∙ Y₁) = [ x ] ∙ (Y ∙ Y₁)
(X ∙ X₁) ∘ ø = X ∙ X₁
(X ∙ X₁) ∘ [ x ] = (X ∙ X₁) ∙ [ x ]
(X ∙ X₁) ∘ (Y ∙ Y₁) = (X ∙ X₁) ∙ (Y ∙ Y₁)

module Rewrite where
  open import Agda.Builtin.Equality
  open import Agda.Builtin.Equality.Rewrite

  ørefl : {A : Set}{X : Tree A} → (ø ∘ X) ≡ X
  ørefl = refl
  
  ørefl' : {A : Set}{X : Tree A} → (X ∘ ø) ≡ X
  ørefl' {X = ø} = refl
  ørefl' {X = [ x ]} = refl
  ørefl' {X = X ∙ X₁} = refl

  {-# REWRITE ørefl' #-}

data ¬ø {A : Set} : Tree A → Set where
  leaf : {x : A} → ¬ø [ x ]
  node : {Γ Δ : Tree A} → ¬ø (Γ ∙ Δ)

comp-¬ø : {A : Set}{Γ Δ : Tree A} → ¬ø Γ → ¬ø Δ → (Γ ∘ Δ) ≡ (Γ ∙ Δ)
comp-¬ø leaf leaf = refl
comp-¬ø leaf node = refl
comp-¬ø node leaf = refl
comp-¬ø node node = refl

is-¬ø : {A : Set}(X : Tree A) → Dec (¬ø X)
is-¬ø ø = no (λ ())
is-¬ø [ x ] = yes leaf
is-¬ø (X ∙ X₁) = yes node

data _⊆_ {A : Set} : Tree A → Tree A → Set where
  here   : ∀ {X : Tree A} → X ⊆ X
  thereˡ : ∀ {x : Tree A}{X : Tree A}{Y : Tree A}
         → x ⊆ X → x ⊆ (X ∙ Y)
  thereʳ : ∀ {x : Tree A}{X : Tree A}{Y : Tree A}
         → x ⊆ X → x ⊆ (Y ∙ X)

_∈_ : {A : Set}(x : A)(Γ : Tree A) → Set
x ∈ Γ = [ x ] ⊆ Γ

data _[_/_]≡_ {A : Set} : {Δ' : Tree A}(Δ : Tree A)(p : Δ' ⊆ Δ)
                          (Ω : Tree A)(Γ : Tree A) → Set where
  here   : {Γ Δ : Tree A} → Δ [ here / Γ ]≡ Γ
  thereʳ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⊆ Δ} → Δ [ p / Ω ]≡ Γ
         → (Ψ ∙ Δ) [ thereʳ p / Ω ]≡ (Ψ ∙ Γ)
  thereˡ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⊆ Δ} → Δ [ p / Ω ]≡ Γ
         → (Δ ∙ Ψ) [ thereˡ p / Ω ]≡ (Γ ∙ Ψ)

_⊇_ : {A : Set}→ Tree A → Tree A → Set
X ⊇ Y = Y ⊆ X

record _≅_ {A : Set}(X : Tree A)(Y : Tree A) : Set where
  constructor _↔_
  field
    sub : X ⊆ Y
    bus : X ⊇ Y

map : ∀ {A B} → (f : A → B) → Tree A → Tree B
map f ø = ø
map f [ x ] = [ f x ]
map f (X ∙ Y) = (map f X) ∙ (map f Y)

f-collect : {A : Set} → (f : A → List A) → Tree A → List A
f-collect f ø = []
f-collect f [ x ] = f x
f-collect f (x ∙ y) = f-collect f x ++ f-collect f y

collect : {A : Set} → Tree A → List A
collect Γ = f-collect (λ x → x ∷ []) Γ
