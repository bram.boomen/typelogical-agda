module Struct.Tree.Tree where

open import Base.Imports
open import Base.Decidable
open import Struct.List using (List; []; _∷_; _++_)

data Tree (A : Set) : Set where
  [_] : (x : A) → Tree A
  _∙_ : Tree A → Tree A → Tree A

data _⊆_ {A : Set}(Γ : Tree A) : Tree A → Set where
  here   : Γ ⊆ Γ
  thereˡ : {Δ Ω : Tree A} → Γ ⊆ Δ → Γ ⊆ (Δ ∙ Ω)
  thereʳ : {Δ Ω : Tree A} → Γ ⊆ Δ → Γ ⊆ (Ω ∙ Δ)

_∈_ : {A : Set}(x : A)(Γ : Tree A) → Set
x ∈ Γ = [ x ] ⊆ Γ

_[_]≔_ : {A : Set}{x : Tree A}(Γ : Tree A) → (p : x ⊆ Γ) → Tree A → Tree A
Γ [ here ]≔ Ω = Ω
(Γ ∙ Δ) [ thereˡ p ]≔ Ω = (Γ [ p ]≔ Ω) ∙ Δ
(Γ ∙ Δ) [ thereʳ p ]≔ Ω = Γ ∙ (Δ [ p ]≔ Ω)

cong≔ : {A : Set}{Γ Δ Ω : Tree A}(p : Δ ⊆ Γ) → Ω ⊆ (Γ [ p ]≔ Ω)
cong≔ here = here
cong≔ (thereˡ p) = thereˡ (cong≔ p)
cong≔ (thereʳ p) = thereʳ (cong≔ p)

trans≔ : {A : Set}{Δ Δ' Γ Ω : Tree A}(p : Ω ⊆ Γ) → ((Γ [ p ]≔ Δ') [ cong≔ p ]≔ Δ) ≡ (Γ [ p ]≔ Δ)
trans≔ here = refl
trans≔ (thereˡ {Ω = Ω} p) = cong (_∙ Ω) (trans≔ p)
trans≔ (thereʳ {Ω = Ω} p) = cong (Ω ∙_) (trans≔ p)

data _[_/_]≡_ {A : Set} : {Δ' : Tree A}(Δ : Tree A)(p : Δ' ⊆ Δ)
                          (Ω : Tree A)(Γ : Tree A) → Set where
  here   : {Γ Δ : Tree A} → Δ [ here / Γ ]≡ Γ
  thereʳ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⊆ Δ} → Δ [ p / Ω ]≡ Γ
         → (Ψ ∙ Δ) [ thereʳ p / Ω ]≡ (Ψ ∙ Γ)
  thereˡ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⊆ Δ} → Δ [ p / Ω ]≡ Γ
         → (Δ ∙ Ψ) [ thereˡ p / Ω ]≡ (Γ ∙ Ψ)

_⊇_ : {A : Set}→ Tree A → Tree A → Set
X ⊇ Y = Y ⊆ X

record _≅_ {A : Set}(X : Tree A)(Y : Tree A) : Set where
  constructor _↔_
  field
    sub : X ⊆ Y
    bus : X ⊇ Y

map : ∀ {A B} → (f : A → B) → Tree A → Tree B
map f [ x ] = [ f x ]
map f (X ∙ Y) = (map f X) ∙ (map f Y)

f-collect : {A : Set} → (f : A → List A) → Tree A → List A
f-collect f [ x ] = f x
f-collect f (x ∙ y) = f-collect f x ++ f-collect f y

collect : {A : Set} → Tree A → List A
collect Γ = f-collect (λ x → x ∷ []) Γ
