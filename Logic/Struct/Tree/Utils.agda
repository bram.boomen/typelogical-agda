module Struct.Tree.Utils where

open import Struct.Tree.Base

∈map : ∀ {A B}{x : A}{X : Tree A} → (f : A → B) → x ∈ X → (f x) ∈ (map f X)
∈map f here = here
∈map f (thereˡ x) = thereˡ (∈map f x)
∈map f (thereʳ x) = thereʳ (∈map f x)

⊆map : ∀ {A B}{X Y : Tree A} → (f : A → B) → X ⊆ Y → (map f X) ⊆ (map f Y)
⊆map f here = here
⊆map f (thereˡ x) = thereˡ (⊆map f x)
⊆map f (thereʳ x) = thereʳ (⊆map f x)
