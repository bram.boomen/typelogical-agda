module Struct.Tree.Properties where

open import Relation.Binary.PropositionalEquality
  using (_≡_; refl; cong; subst; sym)

open import Struct.Tree.Tree
open import Relation.Nullary
open import Data.Empty

-- properties

id : ∀ {A} → Tree A → Tree A
id x = x

-- ⊆ properties

⊆-refl : ∀ {A}{X : Tree A} → X ⊆ X
⊆-refl = here
⊆-reflexive : ∀ {A}{X Y : Tree A} → X ≅ Y → X ⊆ Y
⊆-reflexive (sub ≃ bus) = sub

⊆-trans : ∀ {A}{X Y Z : Tree A} → X ⊆ Y → Y ⊆ Z → X ⊆ Z
⊆-trans p here = p
⊆-trans p (thereˡ q) = thereˡ (⊆-trans p q)
⊆-trans p (thereʳ q) = thereʳ (⊆-trans p q)

-- ≅ properties

open _≅_

≅-refl : ∀ {A}{X : Tree A} → X ≅ X
≅-refl = ⊆-refl & ⊆-refl

≅-sym : ∀ {A}{X Y : Tree A} → X ≅ Y → Y ≅ X
≅-sym X≅Y = bus X≅Y
          & sub X≅Y

≅-trans : ∀ {A}{X Y Z : Tree A} → X ≅ Y → Y ≅ Z → X ≅ Z
sub (≅-trans X≅Y Y≅Z) = ⊆-trans (sub X≅Y) (sub Y≅Z)
bus (≅-trans Y≅X Z≅Y) = ⊆-trans (bus Z≅Y) (bus Y≅X)

⊆-antisym : ∀ {A}{X Y : Tree A} → X ⊆ Y → Y ⊆ X → X ≅ Y
⊆-antisym X⊆Y Y⊆X = X⊆Y & Y⊆X

open import Relation.Binary.Structures using (IsEquivalence; IsPartialOrder; IsPreorder)
≅-isEquivalence : ∀ {A} → IsEquivalence (_≅_ {A})
≅-isEquivalence = record { refl  = ≅-refl;
                           sym   = ≅-sym;
                           trans = ≅-trans}

⊆-isPreorder : ∀ {A} → IsPreorder {A = Tree A} _≅_ _⊆_
⊆-isPreorder = record { isEquivalence = ≅-isEquivalence
                      ; reflexive = ⊆-reflexive
                      ; trans = ⊆-trans }

⊆-isPartialOrder : ∀ {A} → IsPartialOrder {A = Tree A} _≅_ _⊆_ 
⊆-isPartialOrder = record { isPreorder = ⊆-isPreorder
                          ; antisym    = ⊆-antisym}

open import Struct.Modular.Base

instance
  Tree-isStruct : ∀ {A} → isStruct Tree A
  Tree-isStruct = record
    { `[_] = [_]
    ; `⟨_⟩ = id
    ; _`++_ = _∙_
    -- ; _`>_/_ = _>_/_
    -- ; _`∈_ = _∈_
    -- ; _`∈ʳ_ = _∈ʳ_
    -- ; _`∈ˡ_ = _∈ˡ_
    ; _`⊆_ = _⊆_
    ; _`≅_ = _≅_
    ; `map = map
    ; `≅-isEquivalence = ≅-isEquivalence
    ; `⊆-isPartialOrder = ⊆-isPartialOrder
    }
