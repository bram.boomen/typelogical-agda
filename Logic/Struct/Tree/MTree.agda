module Struct.Tree.MTree where

open import Struct.Tree.sTree
open import Base.Imports using (Maybe; yes; no)
open import Relation.Nullary public using (Dec; yes; no; ¬_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

private
 variable
   A : Set
   s : Shape

MTree : (A : Set) → Shape → Set
MTree A s = Tree (Maybe A) s

∅ : MTree A s
∅ {s = %} = [ no ]
∅ {s = s ∙ s'} = ∅ ∙ ∅

is-∅ : (Γ : MTree A s) → Maybe (Γ ≡ ∅)
is-∅ [ no ] = yes refl
is-∅ [ yes x ] = no
is-∅ (Γ ∙ Δ) with is-∅ Γ | is-∅ Δ
is-∅ (Γ ∙ Δ) | _  | no = no
is-∅ (Γ ∙ Δ) | no | _  = no
is-∅ (.∅ ∙ .∅) | yes refl | yes refl = yes refl

𝟙 : Path s → (x : A) → MTree A s
𝟙 h A = [ yes A ]
𝟙 (l p) A = 𝟙 p A ∙ ∅
𝟙 (r p) A = ∅ ∙ 𝟙 p A

is-𝟙 : (_≃_ : (x y : A) → Dec (x ≡ y))
     → (Γ : MTree A s) → (p : Path s) → (x : A) → Maybe (Γ ≡ (𝟙 p x))
is-𝟙 _≃_ [ no ] h A = no
is-𝟙 _≃_ [ yes A' ] h A with A ≃ A'
is-𝟙 _≃_ [ yes .A ] h A | yes refl = yes refl
is-𝟙 _≃_ [ yes A' ] h A | no ¬p = no
is-𝟙 _≃_ (L ∙ R) (l p) A with is-𝟙 _≃_ L p A | is-∅ R
is-𝟙 _≃_ (L ∙ R) (l p) A | no | _  = no
is-𝟙 _≃_ (L ∙ R) (l p) A | _  | no = no
is-𝟙 _≃_ (.(𝟙 p A) ∙ .∅) (l p) A | yes refl | yes refl = yes refl
is-𝟙 _≃_ (L ∙ R) (r p) A with is-𝟙 _≃_ R p A | is-∅ L
is-𝟙 _≃_ (L ∙ R) (r p) A | no | _  = no
is-𝟙 _≃_ (L ∙ R) (r p) A | _  | no = no
is-𝟙 _≃_ (.∅ ∙ .(𝟙 p A)) (r p) A | yes refl | yes refl = yes refl

_⊕_ : MTree A s → MTree A s → MTree A s
[ no ]    ⊕ [ A ] = [ A ]
[ yes x ] ⊕ [ A ] = [ yes x ]
(T ∙ T₁) ⊕ (S ∙ S₁) = (T ⊕ S) ∙ (T₁ ⊕ S₁)

data _≡_+_ {A : Set} : MTree A s → MTree A s → MTree A s → Set where
  o-  : {x : Maybe A} → [ x ] ≡ [ x ] + [ no ]
  -o  : {x : Maybe A} → [ x ] ≡ [ no ] + [ x ]
  _⊗_ : {Γ Δ Ω Γ' Δ' Ω' : MTree A s} → Ω ≡ Γ + Δ → Ω' ≡ Γ' + Δ'
      → (Ω ∙ Ω') ≡ (Γ ∙ Γ') + (Δ ∙ Δ')
