module Struct.Tree.sTree where

open import Relation.Nullary public using (¬_)
open import Data.Nat
open import Struct.List using (List; []; _∷_; _++_)

data Shape : Set where
  %   : Shape
  _∙_ : Shape → Shape → Shape

data Tree (A : Set) : Shape → Set where
  [_] : (x : A) → Tree A %
  _∙_ : {s s' : Shape} → Tree A s → Tree A s' → Tree A (s ∙ s')

infixr 10 l_ r_
data Path : Shape → Set where
  h : Path %
  l_ : {S S' : Shape} → Path S → Path (S ∙ S')
  r_ : {S S' : Shape} → Path S → Path (S' ∙ S)

data _∈_ {A : Set}(x : A) : {S : Shape} → Tree A S → Set where
  here  : x ∈ [ x ]
  thereˡ : ∀ {S S' : Shape}{X : Tree A S}{Y : Tree A S'} → x ∈ X → x ∈ (X ∙ Y)
  thereʳ : ∀ {S S' : Shape}{X : Tree A S}{Y : Tree A S'} → x ∈ X → x ∈ (Y ∙ X)

data _[_]=_ {A : Set} : {S : Shape} → Tree A S → Path S → A → Set where
  here  : ∀ {x} → [ x ] [ h ]= x
  thereˡ : ∀ {S S' : Shape}{X : Tree A S}{Y : Tree A S'}{P : Path S}{x : A}
         → X [ P ]= x → (X ∙ Y) [ l P ]= x
  thereʳ : ∀ {S S' : Shape}{X : Tree A S}{Y : Tree A S'}{P : Path S}{x : A}
         → X [ P ]= x → (Y ∙ X) [ r P ]= x

data _⊆_ {A : Set} : {S S' : Shape} → Tree A S → Tree A S' → Set where
  here   : ∀ {S : Shape}{X : Tree A S} → X ⊆ X
  thereˡ : ∀ {s S S'}{x : Tree A s}{X : Tree A S}{Y : Tree A S'}
         → x ⊆ X → x ⊆ (X ∙ Y)
  thereʳ : ∀ {s S S'}{x : Tree A s}{X : Tree A S}{Y : Tree A S'}
         → x ⊆ X → x ⊆ (Y ∙ X)

_⊇_ : {A : Set}{S S' : Shape} → Tree A S → Tree A S' → Set
X ⊇ Y = Y ⊆ X

record _≅_ {A : Set}{S S' : Shape}(X : Tree A S)(Y : Tree A S') : Set where
  constructor _↔_
  field
    sub : X ⊆ Y
    bus : X ⊇ Y

map : ∀ {A B}{S : Shape} → (f : A → B) → Tree A S → Tree B S
map f [ x ] = [ f x ]
map f (X ∙ Y) = (map f X) ∙ (map f Y)

f-collect : {A : Set}{S : Shape} → (f : A → List A) → Tree A S → List A
f-collect f [ x ] = f x
f-collect f (x ∙ y) = f-collect f x ++ f-collect f y

collect : {A : Set}{S : Shape} → Tree A S → List A
collect Γ = f-collect (λ x → x ∷ []) Γ
