module Struct.STree.Order where

open import Struct.STree.Base

data _≻_ {A : Set} : Tree A → Tree A → Set where
  commʳ : {Γ Δ Ω : Tree A} → ((Γ ∙ ⟨ Ω ⟩) ∙ Δ) ≻ ((Γ ∙ Δ) ∙ ⟨ Ω ⟩)
  commˡ : {Γ Δ Ω : Tree A} → (Δ ∙ (⟨ Γ ⟩ ∙ Ω)) ≻ (⟨ Γ ⟩ ∙ (Δ ∙ Ω))
  assʳ  : {Γ Δ Ω : Tree A} → (Γ ∙ (Δ ∙ ⟨ Ω ⟩)) ≻ ((Γ ∙ Δ) ∙ ⟨ Ω ⟩)
  assˡ  : {Γ Δ Ω : Tree A} → ((⟨ Γ ⟩ ∙ Δ) ∙ Ω) ≻ (⟨ Γ ⟩ ∙ (Δ ∙ Ω))
  trans : {Γ Δ Ω : Tree A} → Γ ≻ Δ → Δ ≻ Ω → Γ ≻ Ω
  addˡ  : {Γ Δ Ω : Tree A} → Γ ≻ Δ → (Ω ∙ Γ) ≻ (Ω ∙ Δ)
  addʳ  : {Γ Δ Ω : Tree A} → Γ ≻ Δ → (Γ ∙ Ω) ≻ (Δ ∙ Ω)

data _≻ˡ_ {A : Set} : Tree A → Tree A → Set where
  comm  : {Γ Δ Ω : Tree A} → (Δ ∙ (⟨ Γ ⟩ ∙ Ω)) ≻ˡ (⟨ Γ ⟩ ∙ (Δ ∙ Ω))
  ass   : {Γ Δ Ω : Tree A} → ((⟨ Γ ⟩ ∙ Δ) ∙ Ω) ≻ˡ (⟨ Γ ⟩ ∙ (Δ ∙ Ω))
  trans : {Γ Δ Ω : Tree A} → Γ ≻ˡ Δ → Δ ≻ˡ Ω → Γ ≻ˡ Ω
  addˡ  : {Γ Δ Ω : Tree A} → Γ ≻ˡ Δ → (Ω ∙ Γ) ≻ˡ (Ω ∙ Δ)
  addʳ  : {Γ Δ Ω : Tree A} → Γ ≻ˡ Δ → (Γ ∙ Ω) ≻ˡ (Δ ∙ Ω)

data _≻ʳ_ {A : Set} : Tree A → Tree A → Set where
  comm  : {Γ Δ Ω : Tree A} → ((Γ ∙ ⟨ Ω ⟩) ∙ Δ) ≻ʳ ((Γ ∙ Δ) ∙ ⟨ Ω ⟩)
  ass   : {Γ Δ Ω : Tree A} → (Γ ∙ (Δ ∙ ⟨ Ω ⟩)) ≻ʳ ((Γ ∙ Δ) ∙ ⟨ Ω ⟩)
  trans : {Γ Δ Ω : Tree A} → Γ ≻ʳ Δ → Δ ≻ʳ Ω → Γ ≻ʳ Ω
  addˡ  : {Γ Δ Ω : Tree A} → Γ ≻ʳ Δ → (Ω ∙ Γ) ≻ʳ (Ω ∙ Δ)
  addʳ  : {Γ Δ Ω : Tree A} → Γ ≻ʳ Δ → (Γ ∙ Ω) ≻ʳ (Δ ∙ Ω)
