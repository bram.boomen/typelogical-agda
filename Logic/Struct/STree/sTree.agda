module Struct.STree.sTree where

data Shape : Set where
  %   : Shape
  ⟨_⟩ : Shape → Shape
  _∙_ : Shape → Shape → Shape

data Tree (A : Set) : Shape → Set where
  [_] : (x : A) → Tree A %
  ⟨_⟩ : {s    : Shape} → Tree A s → Tree A ⟨ s ⟩
  _∙_ : {s s' : Shape} → Tree A s → Tree A s' → Tree A (s ∙ s')

infixr 10 l_ r_ d_
data Path : Shape → Set where
  h : Path %
  d_ : {S : Shape} → Path S → Path ⟨ S ⟩
  l_ : {S S' : Shape} → Path S → Path (S ∙ S')
  r_ : {S S' : Shape} → Path S → Path (S' ∙ S)

data _≽_ {A : Set} : {s s' : Shape} → Tree A s → Tree A s' → Set where
  refl  : {s : Shape}{Γ : Tree A s} → Γ ≽ Γ
  commʳ : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → ((Γ ∙ Δ) ∙ ⟨ Ω ⟩) ≽ ((Γ ∙ ⟨ Ω ⟩) ∙ Δ)
  commˡ : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → (⟨ Γ ⟩ ∙ (Δ ∙ Ω)) ≽ (Δ ∙ (⟨ Γ ⟩ ∙ Ω))
  assʳ  : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → ((Γ ∙ Δ) ∙ ⟨ Ω ⟩) ≽ (Γ ∙ (Δ ∙ ⟨ Ω ⟩))
  assˡ  : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → (⟨ Γ ⟩ ∙ (Δ ∙ Ω)) ≽ ((⟨ Γ ⟩ ∙ Δ) ∙ Ω)

data _≽ˡ_ {A : Set} : {s s' : Shape} → Tree A s → Tree A s' → Set where
  refl  : {s : Shape}{Γ : Tree A s} → Γ ≽ˡ Γ
  commˡ : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → (⟨ Γ ⟩ ∙ (Δ ∙ Ω)) ≽ˡ (Δ ∙ (⟨ Γ ⟩ ∙ Ω))
  assˡ  : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → (⟨ Γ ⟩ ∙ (Δ ∙ Ω)) ≽ˡ ((⟨ Γ ⟩ ∙ Δ) ∙ Ω)

data _≽ʳ_ {A : Set} : {s s' : Shape} → Tree A s → Tree A s' → Set where
  refl  : {s : Shape}{Γ : Tree A s} → Γ ≽ʳ Γ
  commʳ : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → ((Γ ∙ Δ) ∙ ⟨ Ω ⟩) ≽ʳ ((Γ ∙ ⟨ Ω ⟩) ∙ Δ)
  assʳ  : {γ δ ω : Shape}{Γ : Tree A γ}{Δ : Tree A δ}{Ω : Tree A ω}
        → ((Γ ∙ Δ) ∙ ⟨ Ω ⟩) ≽ʳ (Γ ∙ (Δ ∙ ⟨ Ω ⟩))

data _[_]=_ {A : Set} : {s : Shape} → Tree A s → (p : Path s) → A → Set where
  here  : {x : A} → [ x ] [ h ]= x
  thereˡ : {s s' : Shape}{Γ : Tree A s}{Δ : Tree A s'}{p : Path s}{x : A}
         → Γ [ p ]= x → (Γ ∙ Δ) [ l p ]= x
  thereʳ : {s s' : Shape}{Γ : Tree A s}{Δ : Tree A s'}{p : Path s}{x : A}
         → Γ [ p ]= x → (Δ ∙ Γ) [ r p ]= x
  bury   : {s : Shape}{Γ : Tree A s}{p : Path s}{x : A}
         → Γ [ p ]= x → ⟨ Γ ⟩ [ d p ]= x

data _⊆_ {A : Set}{s : Shape}(Γ : Tree A s) : {s' : Shape} → Tree A s' → Set where
  here   : Γ ⊆ Γ
  thereˡ : {δ ω : Shape}{Δ : Tree A δ}{Ω : Tree A ω}
         → Γ ⊆ Δ → Γ ⊆ (Δ ∙ Ω)
  thereʳ : {δ ω : Shape}{Δ : Tree A δ}{Ω : Tree A ω}
         → Γ ⊆ Δ → Γ ⊆ (Ω ∙ Δ)

data _<_ (s : Shape) : Shape → Set where
  here : s < s
  thereˡ : {u v : Shape} → s < u → s < (u ∙ v)
  thereʳ : {u v : Shape} → s < u → s < (v ∙ u)

⊆→< : {A : Set}{γ δ : Shape}{Γ : Tree A γ}{Δ : Tree A δ} → Γ ⊆ Δ → γ < δ
⊆→< here = here
⊆→< (thereˡ x) = thereˡ (⊆→< x)
⊆→< (thereʳ x) = thereʳ (⊆→< x)

subShape : {s s' : Shape} → s' < s → Shape → Shape
subShape here S = S
subShape {_ ∙ s} (thereˡ x) S = (subShape x S) ∙ s
subShape {s ∙ _} (thereʳ x) S = s ∙ (subShape x S)

data _[_/_]≡_ {A : Set} : {δ δ' ω : Shape}{Δ' : Tree A δ'}(Δ : Tree A δ)
  (p : Δ' ⊆ Δ)(Ω : Tree A ω)(Γ : Tree A (subShape (⊆→< p) ω)) → Set where
  here : {γ δ : Shape}{Γ : Tree A γ}{Δ : Tree A δ} → Δ [ here / Γ ]≡ Γ
  thereʳ : {δ δ' ω ψ : Shape}{Δ : Tree A δ}{Δ' : Tree A δ'}{Ω : Tree A ω}{Ψ : Tree A ψ}
         → {p : Δ' ⊆ Δ} → {Γ : Tree A (subShape (⊆→< p) ω)} → Δ [ p / Ω ]≡ Γ → (Ψ ∙ Δ) [ thereʳ p / Ω ]≡ (Ψ ∙ Γ)
  thereˡ : {δ δ' ω ψ : Shape}{Δ : Tree A δ}{Δ' : Tree A δ'}{Ω : Tree A ω}{Ψ : Tree A ψ}
         → {p : Δ' ⊆ Δ} → {Γ : Tree A (subShape (⊆→< p) ω)} → Δ [ p / Ω ]≡ Γ → (Δ ∙ Ψ) [ thereˡ p / Ω ]≡ (Γ ∙ Ψ)

lookup : {A : Set}{s : Shape} → Tree A s → Path s → A
lookup [ x ] h = x
lookup ⟨ Γ ⟩ (d p) = lookup Γ p
lookup (Γ ∙ _) (l p) = lookup Γ p
lookup (_ ∙ Γ) (r p) = lookup Γ p

map : {A B : Set}{s : Shape} → (f : A → B) → Tree A s → Tree B s
map f [ x ] = [ f x ]
map f ⟨ X ⟩ = ⟨ map f X ⟩
map f (X ∙ Y) = (map f X) ∙ (map f Y)
