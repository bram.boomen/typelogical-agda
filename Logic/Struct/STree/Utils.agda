module Struct.STree.Utils where

open import Struct.STree hiding (map)
open import Struct.STree.Order
open import Struct.List hiding ([_])
open import Data.List using (map)
open import Base.Imports

private
  variable
    A : Set

