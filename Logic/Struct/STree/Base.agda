module Struct.STree.Base where
open import Base.Decidable
open import Struct.List using (List; []; _∷_; _++_)
open import Base.Unification

data Tree (A : Set) : Set where
  [_] : (x : A) → Tree A
  ⟨_⟩ : Tree A → Tree A
  _∙_ : Tree A → Tree A → Tree A

map : {A B : Set} → (f : A → B) → Tree A → Tree B
map f [ x ] = [ f x ]
map f ⟨ X ⟩ = ⟨ map f X ⟩
map f (X ∙ Y) = (map f X) ∙ (map f Y)
                        
-- Operations that do not go through ⟨_⟩

data _⊆_ {A : Set}(Γ : Tree A) : Tree A → Set where
  here   : Γ ⊆ Γ
  thereˡ : {Δ Ω : Tree A} → Γ ⊆ Δ → Γ ⊆ (Δ ∙ Ω)
  thereʳ : {Δ Ω : Tree A} → Γ ⊆ Δ → Γ ⊆ (Ω ∙ Δ)

_∈_ : {A : Set}(x : A)(Γ : Tree A) → Set
x ∈ Γ = [ x ] ⊆ Γ

thereʳ+ : {A : Set}{Γ Δ Ω : Tree A} → (Γ ∙ Δ) ⊆ Ω → Δ ⊆ Ω
thereʳ+ here = thereʳ here
thereʳ+ (thereˡ x) = thereˡ (thereʳ+ x)
thereʳ+ (thereʳ x) = thereʳ (thereʳ+ x)

thereˡ+ : {A : Set}{Γ Δ Ω : Tree A} → (Γ ∙ Δ) ⊆ Ω → Γ ⊆ Ω
thereˡ+ here = thereˡ here
thereˡ+ (thereˡ x) = thereˡ (thereˡ+ x)
thereˡ+ (thereʳ x) = thereʳ (thereˡ+ x)

_[_]≔_ : {A : Set}{x : Tree A}(Γ : Tree A) → (p : x ⊆ Γ) → Tree A → Tree A
Γ [ here ]≔ Ω = Ω
(Γ ∙ Δ) [ thereˡ p ]≔ Ω = (Γ [ p ]≔ Ω) ∙ Δ
(Γ ∙ Δ) [ thereʳ p ]≔ Ω = Γ ∙ (Δ [ p ]≔ Ω)

thereʳ+cong : {A : Set}{Γ Δ Ω Ψ : Tree A}(p : (Δ ∙ Ω) ⊆ Γ)
            → (Γ [ thereʳ+ p ]≔ Ψ) ≡ (Γ [ p ]≔ (Δ ∙ Ψ)) 
thereʳ+cong here = refl
thereʳ+cong (thereˡ {Ω = Ω} p) = cong (_∙ Ω) (thereʳ+cong p)
thereʳ+cong (thereʳ {Ω = Ω} p) = cong (Ω ∙_) (thereʳ+cong p)

thereˡ+cong : {A : Set}{Γ Δ Ω Ψ : Tree A}(p : (Ω ∙ Δ) ⊆ Γ)
            → (Γ [ thereˡ+ p ]≔ Ψ) ≡ (Γ [ p ]≔ (Ψ ∙ Δ)) 
thereˡ+cong here = refl
thereˡ+cong (thereˡ {Ω = Ω} p) = cong (_∙ Ω) (thereˡ+cong p)
thereˡ+cong (thereʳ {Ω = Ω} p) = cong (Ω ∙_) (thereˡ+cong p)

data _[_/_]≡_ {A : Set} : {Δ' : Tree A}(Δ : Tree A)(p : Δ' ⊆ Δ)
                          (Ω : Tree A)(Γ : Tree A) → Set where
  here   : {Γ Δ : Tree A} → Δ [ here / Γ ]≡ Γ
  thereʳ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⊆ Δ} → Δ [ p / Ω ]≡ Γ
         → (Ψ ∙ Δ) [ thereʳ p / Ω ]≡ (Ψ ∙ Γ)
  thereˡ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⊆ Δ} → Δ [ p / Ω ]≡ Γ
         → (Δ ∙ Ψ) [ thereˡ p / Ω ]≡ (Γ ∙ Ψ)

cong≔ : {A : Set}{Γ Δ Ω : Tree A}(p : Δ ⊆ Γ) → Ω ⊆ (Γ [ p ]≔ Ω)
cong≔ here = here
cong≔ (thereˡ p) = thereˡ (cong≔ p)
cong≔ (thereʳ p) = thereʳ (cong≔ p)

trans≔ : {A : Set}{Δ Δ' Γ Ω : Tree A}(p : Ω ⊆ Γ) → ((Γ [ p ]≔ Δ') [ cong≔ p ]≔ Δ) ≡ (Γ [ p ]≔ Δ)
trans≔ here = refl
trans≔ (thereˡ {Ω = Ω} p) = cong (_∙ Ω) (trans≔ p)
trans≔ (thereʳ {Ω = Ω} p) = cong (Ω ∙_) (trans≔ p)

subeq : {A : Set}{Δ Δ' Ω : Tree A}{p : Δ' ⊆ Δ} → (Δ [ p ]≔ Ω) [ cong≔ p / Δ' ]≡ Δ
subeq {p = here} = here
subeq {p = thereˡ p} = thereˡ subeq
subeq {p = thereʳ p} = thereʳ subeq

subseq : {A : Set}{Γ Δ Ω δ : Tree A}{p : δ ⊆ Γ} → Γ [ p / Δ ]≡ Ω → (Γ [ p ]≔ Δ) ≡ Ω
subseq here = refl
subseq (thereʳ {Ψ = Ψ} x) = cong (Ψ ∙_) (subseq x)
subseq (thereˡ {Ψ = Ψ} x) = cong (_∙ Ψ) (subseq x)

f-collect : {A : Set} → (f : A → List A) → Tree A → List A
f-collect f [ x ] = f x
f-collect f ⟨ _ ⟩ = []
f-collect f (x ∙ y) = f-collect f x ++ f-collect f y

collect : {A : Set} → Tree A → List A
collect Γ = f-collect (λ x → x ∷ []) Γ

-- Operations that do go through ⟨_⟩

data _⋆⊆_ {A : Set}(Γ : Tree A) : Tree A → Set where
  here   : Γ ⋆⊆ Γ
  bury   : {Δ : Tree A} → Γ ⋆⊆ Δ → Γ ⋆⊆ ⟨ Δ ⟩
  thereˡ : {Δ Ω : Tree A} → Γ ⋆⊆ Δ → Γ ⋆⊆ (Δ ∙ Ω)
  thereʳ : {Δ Ω : Tree A} → Γ ⋆⊆ Δ → Γ ⋆⊆ (Ω ∙ Δ)

_[⋆_]≔_ : {A : Set}{x : Tree A}(Γ : Tree A) → (p : x ⋆⊆ Γ) → Tree A → Tree A
Γ [⋆ here ]≔ Ω = Ω
⟨ Γ ⟩ [⋆ bury p ]≔ Ω = ⟨ Γ [⋆ p ]≔ Ω ⟩
(Γ ∙ Δ) [⋆ thereˡ p ]≔ Ω = (Γ [⋆ p ]≔ Ω) ∙ Δ
(Γ ∙ Δ) [⋆ thereʳ p ]≔ Ω = Γ ∙ (Δ [⋆ p ]≔ Ω)

data _[⋆_/_]≡_ {A : Set} : {Δ' : Tree A}(Δ : Tree A)(p : Δ' ⋆⊆ Δ)
                           (Ω : Tree A)(Γ : Tree A) → Set where
  here   : {Γ Δ : Tree A} → Δ [⋆ here / Γ ]≡ Γ
  bury   : {Γ Δ Δ' Ω : Tree A} → {p : Δ' ⋆⊆ Δ} → Δ [⋆ p / Ω ]≡ Γ
         → ⟨ Δ ⟩ [⋆ bury p / Ω ]≡ ⟨ Γ ⟩
  thereʳ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⋆⊆ Δ} → Δ [⋆ p / Ω ]≡ Γ
         → (Ψ ∙ Δ) [⋆ thereʳ p / Ω ]≡ (Ψ ∙ Γ)
  thereˡ : {Γ Δ Δ' Ω Ψ : Tree A} → {p : Δ' ⋆⊆ Δ} → Δ [⋆ p / Ω ]≡ Γ
         → (Δ ∙ Ψ) [⋆ thereˡ p / Ω ]≡ (Γ ∙ Ψ)

cong⋆≔ : {A : Set}{Γ Δ Ω : Tree A}(p : Δ ⋆⊆ Γ) → Ω ⋆⊆ (Γ [⋆ p ]≔ Ω)
cong⋆≔ here = here
cong⋆≔ (bury p) = bury (cong⋆≔ p)
cong⋆≔ (thereˡ p) = thereˡ (cong⋆≔ p)
cong⋆≔ (thereʳ p) = thereʳ (cong⋆≔ p)

trans⋆≔ : {A : Set}{Δ Δ' Γ Ω : Tree A}(p : Ω ⋆⊆ Γ) → ((Γ [⋆ p ]≔ Δ') [⋆ cong⋆≔ p ]≔ Δ) ≡ (Γ [⋆ p ]≔ Δ)
trans⋆≔ here = refl
trans⋆≔ (bury p) = cong ⟨_⟩ (trans⋆≔ p)
trans⋆≔ (thereˡ {Ω = Ω} p) = cong (_∙ Ω) (trans⋆≔ p)
trans⋆≔ (thereʳ {Ω = Ω} p) = cong (Ω ∙_) (trans⋆≔ p)

subeq⋆ : {A : Set}{Δ Δ' Ω : Tree A}{p : Δ' ⋆⊆ Δ} → (Δ [⋆ p ]≔ Ω) [⋆ cong⋆≔ p / Δ' ]≡ Δ
subeq⋆ {p = here} = here
subeq⋆ {p = bury p} = bury subeq⋆
subeq⋆ {p = thereˡ p} = thereˡ subeq⋆
subeq⋆ {p = thereʳ p} = thereʳ subeq⋆

f-collect⋆ : {A : Set} → (f : A → List A) → Tree A → List A
f-collect⋆ f [ x ] = f x
f-collect⋆ f ⟨ x ⟩ = f-collect⋆ f x
f-collect⋆ f (x ∙ y) = f-collect⋆ f x ++ f-collect⋆ f y

collect⋆ : {A : Set} → Tree A → List A
collect⋆ Γ = f-collect⋆ (λ x → x ∷ []) Γ
