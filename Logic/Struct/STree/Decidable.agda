module Struct.STree.Decidable where
open import Struct.STree
open import Base.Decidable public

_≡t_ : {A : Set}(t s : Tree A) → ((x y : A) → Dec (x ≡ y)) → Dec (t ≡ s)
([ t1 ] ≡t [  t2 ]) _≟_ with t1 ≟ t2
([ t1 ] ≡t [ .t1 ]) _≟_ | yes refl = yes refl
([ t1 ] ≡t [  t2 ]) _≟_ | no ¬t1=t2 = no (¬cong-M ¬t1=t2)
  where
  ¬cong-M : {A : Set}{t1 t2 : A} → ¬ (t1 ≡ t2) → ¬ ([ t1 ] ≡ [ t2 ])
  ¬cong-M ¬m=m refl = ¬m=m refl
(⟨ t ⟩ ≡t ⟨  s ⟩) _≟_ with (t ≡t s) _≟_
(⟨ t ⟩ ≡t ⟨ .t ⟩) _≟_ | yes refl = yes refl
(⟨ t ⟩ ≡t ⟨  s ⟩) _≟_ | no ¬t=s = no (¬cong-⟨⟩ ¬t=s)
  where
  ¬cong-⟨⟩ : {A : Set}{e1 e2 : Tree A} → ¬ (e1 ≡ e2) → ¬ (⟨ e1 ⟩ ≡ ⟨ e2 ⟩)
  ¬cong-⟨⟩ ¬e=e refl = ¬e=e refl
((t1 ∙ t2) ≡t ( s1 ∙  s2)) _≟_ with (t1 ≡t s1) _≟_
((t1 ∙ t2) ≡t ( s1 ∙  s2)) _≟_ | yes p with (t2 ≡t s2) _≟_
((t1 ∙ t2) ≡t (.t1 ∙ .t2)) _≟_ | yes refl | yes refl = yes refl
((t1 ∙ t2) ≡t ( s1 ∙  s2)) _≟_ | _ | no ¬t2=s2 = no (¬cong-R ¬t2=s2)
  where
  ¬cong-R : {A : Set}{e1 e2 e3 e4 : Tree A} → ¬ (e3 ≡ e4) → ¬ ((e1 ∙ e3) ≡ (e2 ∙ e4))
  ¬cong-R ¬e=e refl = ¬e=e refl
((t1 ∙ t2) ≡t (s1 ∙ s2)) _≟_ | no ¬t1=s1 = no (¬cong-L ¬t1=s1)
  where
  ¬cong-L : {A : Set}{e1 e2 e3 e4 : Tree A} → ¬ (e1 ≡ e2) → ¬ ((e1 ∙ e3) ≡ (e2 ∙ e4))
  ¬cong-L ¬e=e refl = ¬e=e refl

([ _ ]   ≡t ⟨ _ ⟩)   _ = no (λ ())
(⟨ _ ⟩   ≡t [ _ ])   _ = no (λ ())
([ _ ]   ≡t (_ ∙ _)) _ = no (λ ())
(⟨ _ ⟩   ≡t (_ ∙ _)) _ = no (λ ())
((_ ∙ _) ≡t [ _ ])   _ = no (λ ())
((_ ∙ _) ≡t ⟨ _ ⟩)   _ = no (λ ())
