{-# OPTIONS --rewriting #-}

module Struct.Vec.Base where

open import Base.Imports
open import Data.Vec public using
  (Vec; []; [_]; _∷_; _∷ʳ_; _++_
  ; _[_]=_; _[_]≔_; lookup; tail; replicate
  ; insert; remove)
  renaming (map to Vmap)
open import Data.Vec.Relation.Unary.Any public using (here; there)

private
 variable
   n m : ℕ

module Rewrite where
  open import Agda.Builtin.Equality
  open import Agda.Builtin.Equality.Rewrite

  cong : ∀ {A B : Set}{x y : A} → (f : A → B) → x ≡ y → f x ≡ f y
  cong f refl = refl
  
  suc+ : {m n : ℕ} → (suc m) + n ≡ suc (m + n)
  suc+ = refl
  
  +suc : {m n : ℕ} → m + (suc n) ≡ suc (m + n)
  +suc {zero}  = refl
  +suc {suc m} = cong suc +suc

  {-# REWRITE +suc #-}

open import Data.Vec.Membership.Propositional public using (_∈_)
open import Data.Vec.Membership.Propositional.Properties public using (∈-lookup)
open import Agda.Builtin.Equality using (refl)

index : {A : Set}{x : A}{Γ : Vec A m} → x ∈ Γ → Fin m
index (here px) = f0
index (there x) = fs (index x)

A∈Γ→Γ[x]A : {A : Set}{x : A}{Γ : Vec A m} → (p : x ∈ Γ) → Γ [ index p ]= x
A∈Γ→Γ[x]A (here refl) = _[_]=_.here
A∈Γ→Γ[x]A (there p) = _[_]=_.there (A∈Γ→Γ[x]A p)

Γ[x]A→A∈Γ : {A : Set}{x : A}{i : Fin m}{Γ : Vec A m} → Γ [ i ]= x → x ∈ Γ
Γ[x]A→A∈Γ _[_]=_.here = here refl
Γ[x]A→A∈Γ (_[_]=_.there x) = there (Γ[x]A→A∈Γ x)

_[_]lookup : {A : Set}(Γ : Vec A m)(x : Fin m) → Γ [ x ]= (lookup Γ x)
(A ∷ Γ) [ f0 ]lookup = _[_]=_.here
(A ∷ Γ) [ fs p ]lookup = _[_]=_.there (Γ [ p ]lookup)

infix 10 _≡_+_
infixr 10 -o_ o-_
data _≡_+_ {A : Set} : Vec A (n + m) → Vec A n → Vec A m → Set where
  [-] : [] ≡ [] + []
  -o_ : {Γ : Vec A n}{Δ : Vec A m}{Ω : Vec A (n + m)}{x : A}
      → Ω ≡ Γ + Δ → (x ∷ Ω) ≡ Γ + (x ∷ Δ)
  o-_ : {Γ : Vec A n}{Δ : Vec A m}{Ω : Vec A (n + m)}{x : A}
      → Ω ≡ Γ + Δ → (x ∷ Ω) ≡ (x ∷ Γ) + Δ
