module Struct.Vec.Decidable where
open import Base.Imports
open import Struct.Vec
open import Base.Decidable public

private
  variable
    A : Set
    m : ℕ

_≡v_ : (t s : Vec A m) → ((x y : A) → Dec (x ≡ y)) → Dec (t ≡ s)
([] ≡v []) _≟_ = yes refl
((x ∷ t) ≡v ( y ∷  s)) _≟_ with x ≟ y
((x ∷ t) ≡v (.x ∷  s)) _≟_ | yes refl with (t ≡v s) _≟_
((x ∷ t) ≡v (.x ∷ .t)) _≟_ | yes refl | yes refl = yes refl
((x ∷ t) ≡v (.x ∷  s)) _≟_ | yes refl | no ¬p = no (¬cong-v ¬p)
  where
  ¬cong-v : {x : A}{v1 v2 : Vec A m} → ¬ (v1 ≡ v2) → ¬ ((x ∷ v1) ≡ (x ∷ v2))
  ¬cong-v ¬v=v refl = ¬v=v refl
((x ∷ t) ≡v ( y ∷  s)) _≟_ | no ¬p = no (¬cong-x ¬p)
  where
  ¬cong-x : {x1 x2 : A}{v1 v2 : Vec A m} → ¬ (x1 ≡ x2) → ¬ ((x1 ∷ v1) ≡ (x2 ∷ v2))
  ¬cong-x ¬x=x refl = ¬x=x refl

