{-# OPTIONS --rewriting #-}

module Struct.Vec.MaybeVec where

open import Struct.Vec.Base public hiding (-o_; o-_; [-])
open import Base.Imports
open import Relation.Nullary public using (Dec; yes; no; ¬_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

private
 variable
   n m : ℕ

MVec : (A : Set) → ℕ → Set
MVec A n = Vec (Maybe A) n

𝟘 : {A : Set}{m : ℕ} → MVec A m
𝟘 = replicate no

is-𝟘 : {A : Set}(Γ : MVec A m) → Maybe (Γ ≡ 𝟘)
is-𝟘 [] = yes refl
is-𝟘 (no ∷ Γ) with is-𝟘 Γ
is-𝟘 (no ∷ Γ) | no = no
is-𝟘 (no ∷ .𝟘) | yes refl = yes refl
is-𝟘 (yes x ∷ Γ) = no

𝟙 : {A : Set}{m : ℕ} → Fin m → (x : A) → MVec A m
𝟙 f0 A = (yes A) ∷ 𝟘
𝟙 (fs f) A = no ∷ 𝟙 f A

is-𝟙 : {A : Set}(_≃_ : (x y : A) → Dec (x ≡ y))
     → (Γ : MVec A m) → (f : Fin m) → (x : A) → Maybe (Γ ≡ (𝟙 f x))
is-𝟙 _≃_ (no ∷ Γ) f0 A = no
is-𝟙 _≃_ (yes A' ∷ Γ) f0 A with A ≃ A' | is-𝟘 Γ
is-𝟙 _≃_ (yes A' ∷ .𝟘) f0 .A' | yes refl | yes refl = yes refl
is-𝟙 _≃_ (yes A' ∷ Γ) f0 .A' | yes refl | no = no
is-𝟙 _≃_ (yes A' ∷ Γ) f0 A    | no ¬p | _ = no
is-𝟙 _≃_ (no ∷ Γ) (fs x) A with is-𝟙 _≃_ Γ x A
is-𝟙 _≃_ (no ∷ Γ) (fs x) A | no = no
is-𝟙 _≃_ (no ∷ .(𝟙 x A)) (fs x) A | yes refl = yes refl
is-𝟙 _≃_ (yes _ ∷ Γ) (fs x) A = no

_⊞⊞_ : {A : Set}{n : ℕ} → MVec A n → MVec A n → MVec A n
[] ⊞⊞ [] = []
(no ∷ Γ) ⊞⊞ (no ∷ Δ) = no ∷ (Γ ⊞⊞ Δ)
(no ∷ Γ) ⊞⊞ (yes y ∷ Δ) = yes y ∷ (Γ ⊞⊞ Δ)
(yes x ∷ Γ) ⊞⊞ (y ∷ Δ) = yes x ∷ (Γ ⊞⊞ Δ)

infixr 20 -o_ o-_
data _≡_⊞_ {A : Set} : MVec A n → MVec A n → MVec A n → Set where
  [-] : [] ≡ [] ⊞ []
  -o_ : {Γ Δ Ω : MVec A n}{x : Maybe A}
      → Ω ≡ Γ ⊞ Δ → (x ∷ Ω) ≡ (no ∷ Γ) ⊞ (x ∷ Δ)
  o-_ : {Γ Δ Ω : MVec A n}{x : Maybe A}
      → Ω ≡ Γ ⊞ Δ → (x ∷ Ω) ≡ (x ∷ Γ) ⊞ (no ∷ Δ)
