module Base.Imports where

open import Data.Fin public using (Fin; toℕ)
                            renaming (0F to f0; suc to fs) 
open import Data.Nat public using (ℕ; zero; suc; _+_; pred; _∸_)
open import Data.Maybe public using (Maybe)
                       renaming (just to yes; nothing to no; map to mmap)
open import Struct.Pair

_&_ : {A B : Set} → Maybe A → Maybe B → Maybe (Pair A B)
no & B = no
yes A & no = no
yes A & yes B = yes (A × B)

_⅋_ : {A : Set} → Maybe A → A → A
no ⅋ B = B
yes A ⅋ B = A

_<$>_ : {A B C : Set} → (B → C) → (A → B) → (A → C)
(f <$> g) x = f (g x)

infixr 3 ∃_
data ∃_ {S : Set}(T : S → Set) : Set where
  ⟨_,_⟩ : (s : S) → (t : T s) → ∃ T

∃₂_ : ∀ {R : Set}{S : R → Set}(T : (r : R) → S r → Set) → Set
∃₂ T = ∃ (λ r → ∃ (λ s → T r s))

targ : {S : Set}{T : S → Set} → ∃ T → S
targ ⟨ n , t ⟩ = n

∃map : {a b : Set}{A : a → Set}{B : b → Set}(f : a → b)
       (g : ∀ {s} → A s → B (f s)) → ∃ A → ∃ B
∃map f g ⟨ s , t ⟩ = ⟨ (f s) , g t ⟩

open import Agda.Builtin.Bool public
