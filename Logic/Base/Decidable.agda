module Base.Decidable where

open import Base.Imports
open import Data.Nat.Properties using (_≟_)
open import Relation.Nullary public using (Dec; yes; no; ¬_)
open import Relation.Binary.PropositionalEquality public using (_≡_; refl; cong; subst)

_≡ℕ_ : (n m : ℕ) → Dec (n ≡ m)
_≡ℕ_ = _≟_

_≡F_ : {n : ℕ} → (f1 f2 : Fin n) → Dec (f1 ≡ f2)
f0 ≡F f0 = yes refl
f0 ≡F fs g = no (λ ())
fs f ≡F f0 = no (λ ())
fs f ≡F fs g with f ≡F g
(fs f ≡F fs g) | yes f=g = yes (cong fs f=g)
(fs f ≡F fs g) | no ¬f=g = no (¬n=m→¬fn=fm ¬f=g)
  where
  ¬n=m→¬fn=fm : ∀ {n : ℕ}{f g : Fin n} → ¬ (f ≡ g) → ¬ ((fs f) ≡ (fs g))
  ¬n=m→¬fn=fm ¬f=g refl = ¬f=g refl
