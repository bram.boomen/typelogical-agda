module Base.Unification where

open import Base.Imports

_∘_ = _<$>_

▸_ : {S T : Set}(f : S → T) → (S → Maybe T)
▸ f = yes ∘ f

_◂ : {S T : Set}(f : S → Maybe T) → (Maybe S → Maybe T)
(f ◂) no = no
(f ◂) (yes s) = f s

▸_◂¹  : {S T : Set}(f : S → T) → (Maybe S → Maybe T)
▸ f ◂¹ = (▸ f) ◂

▸_◂²  : {R S T : Set}(f : R → S → T) → (Maybe R → Maybe S → Maybe T)
▸ f ◂² no no = no
▸ f ◂² no (yes t) = no
▸ f ◂² (yes r) no = no
▸ f ◂² (yes r) (yes s) = yes (f r s)

thin : {n : ℕ}(x : Fin (suc n))(y : Fin n) → Fin (suc n)
thin f0 y = fs y
thin (fs x) f0 = f0
thin (fs x) (fs y) = fs (thin x y)

thick : {n : ℕ}(x y : Fin (suc n)) → Maybe (Fin n)
thick f0 f0 = no
thick f0 (fs y) = yes y
thick {suc n} (fs x) f0 = yes f0
thick {suc n} (fs x) (fs y) = ▸ fs ◂¹ (thick x y)

