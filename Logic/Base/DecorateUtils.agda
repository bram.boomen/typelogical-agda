module Base.DecorateUtils where


open import Data.Nat using (ℕ; zero; suc; _<_; s≤s; z≤n; _+_)
open import Data.Nat.Properties public using (+-comm; ≤-step; ≤-refl)
open import Data.Fin using (Fin; toℕ; inject+)
                     renaming (0F to f0; suc to fs) public
open import Relation.Binary.PropositionalEquality as Eq public using (_≡_; refl; cong; subst)

fromℕ< : (n f : ℕ) → n < f → Fin f
fromℕ< zero (suc f) (s≤s z≤n) = f0
fromℕ< (suc n) (suc f) (s≤s n<f) = fs (fromℕ< n f n<f)

fin-subst : ∀ {n o} → (p : n ≡ o) → Fin n → Fin o
fin-subst = subst Fin

-- toℕ (raise x f) = x + (toℕ f)
raise+ : ∀ {n : ℕ} → (o : ℕ) → Fin n → Fin (o + n)
raise+ zero f = f
raise+ (suc o) f = fs (raise+ o f)

raise : ∀ {n : ℕ} → (o : ℕ) → Fin n → Fin (n + o)
raise {n} o f = fin-subst (+-comm o n) (raise+ o f)

-- toℕ (inject x f) = (toℕ f)
inject : ∀ {n : ℕ} → (o : ℕ) → Fin n → Fin (o + n)
inject {n} o f = fin-subst (+-comm n o) (inject+ o f)

ℕ-raise : ∀ {m : ℕ}(n : ℕ)(f : Fin m) → toℕ (raise+ n f) ≡ n + (toℕ f)
ℕ-raise zero f = refl
ℕ-raise (suc n) f = cong suc (ℕ-raise n f)

ℕ-inject+ : ∀ {m : ℕ}(n : ℕ)(f : Fin m) → toℕ (inject+ n f) ≡ toℕ f
ℕ-inject+ n f0 = refl
ℕ-inject+ n (fs f) = cong suc (ℕ-inject+ n f)
