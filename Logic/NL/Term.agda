module NL.Term where

open import NL.Base

private
  variable
    n m o p : ℕ
    A B C : Type n

-- module Associative&Commutative where

--   open import ILL.Term
--   open Associative&Commutative public

module Associative&Commutative where

  open import Struct.Vec public

  Env : (n m : ℕ) → Set
  Env n m = Vec (Type n) m

  data Term : (Γ : Env n m) → (A : Type n) → Set where
    $      : {A : Type n} → Term [ A ] A
    _◂_[_] : {Γ : Env n m}{Δ : Env n o}{Ω : Env n (m + o)}
           → Term Γ (B ⟪ A) → Term Δ A → Ω ≡ Γ + Δ → Term Ω B
    _▸_[_] : {Γ : Env n m}{Δ : Env n o}{Ω : Env n (m + o)}
           → Term Γ A → Term Δ (A ⟫ B) → Ω ≡ Γ + Δ → Term Ω B
    ƛ_▹_   : {Γ : Env n m} → (A : Type n)
           → Term (A ∷  Γ) B → Term Γ (A ⟫ B)
    ƛ_◃_   : {Γ : Env n m} → (A : Type n)
           → Term (Γ ∷ʳ A) B → Term Γ (B ⟪ A)

module NonAssociative&NonCommutative where

  open import Struct.Tree public

  Env : (n : ℕ) → Set
  Env n = Tree (Type n)

  data Term : (Γ : Env n) → (A : Type n) → Set where
    $      : {A : Type n} → Term [ A ] A
    _◂_    : {Γ : Env n}{Δ : Env n}
           → Term Γ (B ⟪ A) → Term Δ A → Term (Γ ∙ Δ) B
    _▸_    : {Γ : Env n}{Δ : Env n}
           → Term Γ A → Term Δ (A ⟫ B) → Term (Γ ∙ Δ) B
    ƛ_▹_   : {Γ : Env n}(A : Type n)
           → Term ([ A ] ∙ Γ) B → Term Γ (A ⟫ B)
    ƛ_◃_   : {Γ : Env n}(A : Type n)
           → Term (Γ ∙ [ A ]) B → Term Γ (B ⟪ A)

module NonAssociative&NonCommutative&Empty where

  open import Struct.Tree.ETree public

  Env : (n : ℕ) → Set
  Env n = Tree (Type n)

  data Term : (Γ : Env n) → (A : Type n) → Set where
    $      : {A : Type n} → Term [ A ] A
    _◂_    : {Γ : Env n}{Δ : Env n}
           → Term Γ (B ⟪ A) → Term Δ A → Term (Γ ∘ Δ) B
    _▸_    : {Γ : Env n}{Δ : Env n}
           → Term Γ A → Term Δ (A ⟫ B) → Term (Γ ∘ Δ) B
    ƛ_▹_   : {Γ : Env n}(A : Type n)
           → Term ([ A ] ∘ Γ) B → Term Γ (A ⟫ B)
    ƛ_◃_   : {Γ : Env n}(A : Type n)
           → Term (Γ ∘ [ A ]) B → Term Γ (B ⟪ A)
  
module NonAssociative&NonCommutative-deBruijn where

  open import Struct.Tree.sTree public
  open import Struct.Tree.MTree public

  private
    variable
      s : Shape

  Env : (n : ℕ)(s : Shape) → Set
  Env n s = MTree (Type n) s

  data Term : (Γ : Env n s) → (A : Type n) → Set where
    $      : {A : Type n}{p : Path s} → Term (𝟙 p A) A
    _◂_[_] : {Γ Δ Ω : Env n s} → Term Γ (B ⟪ A) → Term Δ A
           → Ω ≡ Γ + Δ → Term Ω B
    _▸_[_] : {Γ Δ Ω : Env n s} → Term Γ A → Term Δ (A ⟫ B)
           → Ω ≡ Γ + Δ → Term Ω B
    ƛ_▹_   : {Γ : Env n s}(A : Type n)
           → Term ([ yes A ] ∙ Γ) B → Term Γ (A ⟫ B)
    ƛ_◃_   : {Γ : Env n s}(A : Type n)
           → Term (Γ ∙ [ yes A ]) B → Term Γ (B ⟪ A)
