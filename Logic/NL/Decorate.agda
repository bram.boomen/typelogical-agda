module NL.Decorate where

open import NL
open import Base.DecorateUtils
open import Struct.Pair

private
  variable
    n m : ℕ

data RawTerm (m : ℕ) : Set where
  $_  : Fin m → RawTerm m
  _▸_ : RawTerm m → RawTerm m → RawTerm m
  _◂_ : RawTerm m → RawTerm m → RawTerm m
  ƛ◃_  : RawTerm (suc m) → RawTerm m
  ƛ▹_  : RawTerm (suc m) → RawTerm m

data DecTerm (m n : ℕ) : Set where
  _,$_   : (A : Type n) → Fin m → DecTerm m n
  _,_◂_  : (A : Type n) → (N : DecTerm m n) → (M : DecTerm m n) → DecTerm m n
  _,_▸_  : (A : Type n) → (M : DecTerm m n) → (N : DecTerm m n) → DecTerm m n
  _,ƛ_▹_ : (A : Type n) → Type n → (M : DecTerm (suc m) n) → DecTerm m n
  _,ƛ_◃_ : (A : Type n) → Type n → (M : DecTerm (suc m) n) → DecTerm m n

dec-type : DecTerm m n → Type n
dec-type (A ,$ τ) = A
dec-type (A , t ◂ t₁) = A
dec-type (A , t ▸ t₁) = A
dec-type (A ,ƛ B ▹ t) = A
dec-type (A ,ƛ B ◃ t) = A

abs-type : DecTerm (suc m) n → Maybe (Type n)
abs-type dec = lookup-dec-type dec f0
  where
  lookup-dec-type : DecTerm m n → Fin m → Maybe (Type n)
  lookup-dec-type (A ,$ x) f with x ≡F f
  lookup-dec-type (A ,$ x) .x | yes refl = yes A
  lookup-dec-type (A ,$ x) f  | no ¬p = no
  lookup-dec-type (A , t₁ ◂ t₂) f with lookup-dec-type t₁ f
  lookup-dec-type (A , t₁ ◂ t₂) f | yes T = yes T
  lookup-dec-type (A , t₁ ◂ t₂) f | no = lookup-dec-type t₂ f
  lookup-dec-type (A , t₁ ▸ t₂) f with lookup-dec-type t₁ f
  lookup-dec-type (A , t₁ ▸ t₂) f | yes T = yes T
  lookup-dec-type (A , t₁ ▸ t₂) f | no = lookup-dec-type t₂ f
  lookup-dec-type (A ,ƛ _ ▹ t) f = lookup-dec-type t (fs f)
  lookup-dec-type (A ,ƛ _ ◃ t) f = lookup-dec-type t (fs f)

dec-t-map : {m n o : ℕ} → (f : Type n → Type o) → DecTerm m n → DecTerm m o
dec-t-map f (A ,$ x) = (f A) ,$ x
dec-t-map f (A , N ◂ M) = (f A) , (dec-t-map f N) ◂ (dec-t-map f M)
dec-t-map f (A , M ▸ N) = (f A) , (dec-t-map f M) ▸ (dec-t-map f N)
dec-t-map f (A ,ƛ B ▹ M) = (f A) ,ƛ (f B) ▹ (dec-t-map f M)
dec-t-map f (A ,ƛ B ◃ M) = (f A) ,ƛ (f B) ▹ (dec-t-map f M)

type-n-map : {n o : ℕ} → (f : Fin n → Fin o) → Type n → Type o
type-n-map f (var x) = var (f x)
type-n-map f (A ⟫ B) = type-n-map f A ⟫ type-n-map f B
type-n-map f (B ⟪ A) = type-n-map f B ⟪ type-n-map f A

dec-n-map : ∀ {n o : ℕ} → (f : Fin n → Fin o) → DecTerm m n → DecTerm m o
dec-n-map f = dec-t-map (type-n-map f)

inject-dec : (o : ℕ) → DecTerm m n → DecTerm m (n + o)
inject-dec o = dec-n-map (inject+ o)

raise-dec : (o : ℕ) → DecTerm m n → DecTerm m (o + n)
raise-dec o = dec-n-map (raise+ o)

decorate : (ρ : RawTerm m) → Maybe (∃ (DecTerm m))
decorate ($ x) = yes ⟨ 1 , (var f0 ,$ x) ⟩
decorate (a ▸ f) with (decorate a) & (decorate f)
decorate (a ▸ f) | no = no
decorate (a ▸ f) | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩)
  = let F' = raise-dec (1 + na) F
        A' = raise-dec 1 (inject-dec nf A)
  in yes ⟨ suc (na + nf) , (var f0) , A' ▸ F' ⟩
decorate (f ◂ a) with (decorate a) & (decorate f)
decorate (f ◂ a) | no = no
decorate (f ◂ a) | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩)
  = let F' = raise-dec (1 + na) F
        A' = raise-dec 1 (inject-dec nf A)
  in yes ⟨ suc (na + nf) , (var f0) , F' ◂ A' ⟩
decorate (ƛ◃ f) with decorate f
decorate (ƛ◃ f) | no = no
decorate (ƛ◃ f) | yes ⟨ nf , F ⟩ with raise-dec 1 F | abs-type F
decorate (ƛ◃ f) | yes ⟨ nf , F ⟩ | F' | no = no
decorate (ƛ◃ f) | yes ⟨ nf , F ⟩ | F' | yes B
  = yes ⟨ (suc nf) , (var f0 ,ƛ type-n-map (inject 1) B ◃ F') ⟩
decorate (ƛ▹ f) with decorate f
decorate (ƛ▹ f) | no = no
decorate (ƛ▹ f) | yes ⟨ nf , F ⟩ with raise-dec 1 F | abs-type F
decorate (ƛ▹ f) | yes ⟨ nf , F ⟩ | F' | no = no
decorate (ƛ▹ f) | yes ⟨ nf , F ⟩ | F' | yes B
  = yes ⟨ (suc nf) , (var f0 ,ƛ type-n-map (inject 1) B ▹ F') ⟩
