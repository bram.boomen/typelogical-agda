{-# OPTIONS --rewriting #-}

module NL.Solve.Check where

open import NL
open import Struct.Pair

private
  variable
    n m : ℕ

data RawTerm (n : ℕ) : Set where
  $    : RawTerm n
  _◂_  : RawTerm n → RawTerm n → RawTerm n
  _▸_  : RawTerm n → RawTerm n → RawTerm n
  ƛ_◃_ : Type n → RawTerm n → RawTerm n
  ƛ_▹_ : Type n → RawTerm n → RawTerm n

module Check-NonAssociative&NonCommutative where
  open NonAssociative&NonCommutative

  erase : {Γ : Env n}{A : Type n} → Term Γ A → RawTerm n
  erase $ = $
  erase (N ◂ M) = (erase N) ◂ (erase M)
  erase (M ▸ N) = erase M ▸ erase N
  erase (ƛ A ◃ M) = ƛ A ◃ (erase M)
  erase (ƛ A ▹ M) = ƛ A ▹ (erase M)

  data Infer (Γ : Env n) : RawTerm n → Set where
    ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
    bad : {r : RawTerm n} → Infer Γ r

  check : (Γ : Env n) → (ρ : RawTerm n) → Infer Γ ρ

  check [ A ] $ = ok A $
  check (_ ∙ _) $ = bad

  check (Γ ∙ Δ) (N ◂ M) with check Γ N | check Δ M
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok (B ⟪ A') n | ok A m with A ≡T A'
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok (B ⟪ .A) n | ok A m | yes refl = ok B (n ◂ m)
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok (B ⟪ A') n | ok A m | no ¬p = bad
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok _        n | ok B m = bad
  check (Γ ∙ Δ) (.(erase n) ◂ M) | ok A n | bad = bad
  check (Γ ∙ Δ) (N ◂ M) | bad | q = bad
  check _ (N ◂ M) = bad

  check (Γ ∙ Δ) (N ▸ M) with check Γ N | check Δ M
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok A n | ok (A' ⟫ B) m with A ≡T A'
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok A n | ok (.A ⟫ B) m | yes refl = ok B (n ▸ m)
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok A n | ok (A' ⟫ B) m | no ¬p = bad
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok B n | ok _        m = bad
  check (Γ ∙ Δ) (.(erase n) ▸ M) | ok A n | bad = bad
  check (Γ ∙ Δ) (N ▸ M) | bad | q = bad
  check _ (N ▸ M) = bad

  check Γ (ƛ A ◃ M) with check (Γ ∙ [ A ]) M
  check Γ (ƛ A ◃ .(erase m)) | ok B m = ok (B ⟪ A) (ƛ A ◃ m)
  check Γ (ƛ A ◃ M) | bad = bad

  check Γ (ƛ A ▹ M) with check ([ A ] ∙ Γ) M
  check Γ (ƛ A ▹ .(erase m)) | ok B m = ok (A ⟫ B) (ƛ A ▹ m)
  check Γ (ƛ A ▹ M) | bad = bad

module Check-NonAssociative&NonCommutative&Empty where
  open NonAssociative&NonCommutative&Empty

  erase : {Γ : Env n}{A : Type n} → Term Γ A → RawTerm n
  erase $ = $
  erase (N ◂ M) = (erase N) ◂ (erase M)
  erase (M ▸ N) = erase M ▸ erase N
  erase (ƛ A ◃ M) = ƛ A ◃ (erase M)
  erase (ƛ A ▹ M) = ƛ A ▹ (erase M)

  data Infer (Γ : Env n) : RawTerm n → Set where
    ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
    bad : {r : RawTerm n} → Infer Γ r

  ≡-subst : {Γ Δ : Env n} → Γ ≡ Δ → {ρ : RawTerm n} → Infer Γ ρ → Infer Δ ρ
  ≡-subst refl inf = inf

  check : (Γ : Env n) → (ρ : RawTerm n) → Infer Γ ρ

  check-◂-split : (Γ Δ : Env n) → (ρ ρ' : RawTerm n) → Maybe (Infer (Γ ∘ Δ) (ρ ◂ ρ'))
  check-◂-split Γ Δ n m with check Γ n | check Δ m
  check-◂-split Γ Δ .(erase n) .(erase m) | ok (C ⟪  B) n | ok A m with B ≡T A
  check-◂-split Γ Δ .(erase n) .(erase m) | ok (C ⟪ .A) n | ok A m | yes refl = yes (ok C (n ◂ m))
  check-◂-split Γ Δ .(erase n) .(erase m) | ok (C ⟪  B) n | ok A m | no ¬p = no
  check-◂-split Γ Δ .(erase n) .(erase m) | ok (var x)  n | ok A m = no
  check-◂-split Γ Δ .(erase n) .(erase m) | ok (_ ⟫ _)  n | ok A m = no
  check-◂-split Γ Δ N M | _ | bad = no
  check-◂-split Γ Δ N M | bad | _ = no
  check-▸-split : (Γ Δ : Env n) → (ρ ρ' : RawTerm n) → Maybe (Infer (Γ ∘ Δ) (ρ ▸ ρ'))
  check-▸-split Γ Δ n m with check Γ n | check Δ m
  check-▸-split Γ Δ .(erase n) .(erase m) | ok A n | ok (B ⟫  C) m with B ≡T A
  check-▸-split Γ Δ .(erase n) .(erase m) | ok A n | ok (.A ⟫ C) m | yes refl = yes (ok C (n ▸ m))
  check-▸-split Γ Δ .(erase n) .(erase m) | ok A n | ok (B ⟫  C) m | no ¬p = no
  check-▸-split Γ Δ .(erase n) .(erase m) | ok A n | ok (var x)  m = no
  check-▸-split Γ Δ .(erase n) .(erase m) | ok A n | ok (_ ⟪ _)  m = no
  check-▸-split Γ Δ N M | _ | bad = no
  check-▸-split Γ Δ N M | bad | _ = no

  check [ A ] $ = ok A $
  check (_ ∙ _) $ = bad
  check ø $ = bad

  check Γ (N ◂ M) with check-◂-split ø Γ N M
  check Γ (N ◂ M) | yes p = p
  check Γ (N ◂ M) | no with check-◂-split Γ ø N M
  check Γ (N ◂ M) | no | yes p = p
  check (Γ ∙ Δ) (N ◂ M) | no | no with is-¬ø Γ | is-¬ø Δ
  check (Γ ∙ Δ) (N ◂ M) | no | no | yes p | yes q with check-◂-split Γ Δ N M
  check (Γ ∙ Δ) (N ◂ M) | no | no | yes p | yes q | yes x = ≡-subst (comp-¬ø p q) x
  check (Γ ∙ Δ) (N ◂ M) | no | no | yes p | yes q | no = bad
  check (Γ ∙ Δ) (N ◂ M) | no | no | yes p | no ¬q = bad
  check (Γ ∙ Δ) (N ◂ M) | no | no | no ¬p | _     = bad
  check _ (N ◂ M) | no | no = bad

  check Γ (N ▸ M) with check-▸-split ø Γ N M
  check Γ (N ▸ M) | yes p = p
  check Γ (N ▸ M) | no with check-▸-split Γ ø N M
  check Γ (N ▸ M) | no | yes p = p
  check (Γ ∙ Δ) (N ▸ M) | no | no with is-¬ø Γ | is-¬ø Δ
  check (Γ ∙ Δ) (N ▸ M) | no | no | yes p | yes q with check-▸-split Γ Δ N M
  check (Γ ∙ Δ) (N ▸ M) | no | no | yes p | yes q | yes x = ≡-subst (comp-¬ø p q) x
  check (Γ ∙ Δ) (N ▸ M) | no | no | yes p | yes q | no = bad
  check (Γ ∙ Δ) (N ▸ M) | no | no | yes p | no ¬q = bad
  check (Γ ∙ Δ) (N ▸ M) | no | no | no ¬p | _     = bad
  check _ (N ▸ M) | no | no = bad

  check Γ (ƛ A ◃ M) with check (Γ ∘ [ A ]) M
  check Γ (ƛ A ◃ .(erase m)) | ok B m = ok (B ⟪ A) (ƛ A ◃ m)
  check Γ (ƛ A ◃ M) | bad = bad

  check Γ (ƛ A ▹ M) with check ([ A ] ∘ Γ) M
  check Γ (ƛ A ▹ .(erase m)) | ok B m = ok (A ⟫ B) (ƛ A ▹ m)
  check Γ (ƛ A ▹ M) | bad = bad
