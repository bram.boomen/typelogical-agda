module NL.Solve.Infer where

open import NL
open import NL.Decorate
open import NL.Solve.Check renaming (RawTerm to ChkTerm)

private
  variable
    n m : ℕ

morris-mgu : (t : DecTerm m n) → Maybe (∃ AList n) → Maybe (∃ AList n)
morris-mgu t no = no
morris-mgu (A ,$ τ)     (yes sub) = yes sub
morris-mgu (A , N ◂ M)  (yes sub) = let subs = amgu (dec-type N) (A ⟪ (dec-type M)) sub
                                    in morris-mgu N (morris-mgu M subs)
morris-mgu (A , M ▸ N)  (yes sub) = let subs = amgu (dec-type N) ((dec-type M) ⟫ A) sub
                                    in morris-mgu N (morris-mgu M subs)
morris-mgu (A ,ƛ B ▹ M) (yes sub) = let subs = amgu A (B ⟫ dec-type M) sub
                                    in morris-mgu M subs
morris-mgu (A ,ƛ B ◃ M) (yes sub) = let subs = amgu A (dec-type M ⟪ B) sub
                                    in morris-mgu M subs

morris : (t : DecTerm m n) → Maybe (∃ AList n)
morris t = morris-mgu t (yes ø-sub)

module Infer-NonAssociative&NonCommutative where
  open NonAssociative&NonCommutative
  open Check-NonAssociative&NonCommutative
  open import Struct.Pair

  Chk : (n : ℕ) → Set
  Chk n = Pair (ChkTerm n) (Env n)

  left right : {A : Set} → Tree A → Tree A
  left [ x ] = [ x ]
  left (x ∙ _) = x
  right [ x ] = [ x ]
  right (_ ∙ x) = x

  decChk : DecTerm m n → Chk n
  decChk (A ,$ x) = $ × [ A ]
  decChk (A , N ◂ M) with decChk N | decChk M
  decChk (A , N ◂ M) | N' × Γ | M' × Δ = (N' ◂ M') × (Γ ∙ Δ)
  decChk (A , N ▸ M) with decChk N | decChk M
  decChk (A , N ▸ M) | N' × Γ | M' × Δ = (N' ▸ M') × (Γ ∙ Δ)
  decChk (A ,ƛ B ▹ M) with decChk M
  decChk (A ,ƛ B ▹ M) | M' × Γ = (ƛ B ▹ M' ) × (right Γ)
  decChk (A ,ƛ B ◃ M) with decChk M
  decChk (A ,ƛ B ◃ M) | M' × Γ = (ƛ B ◃ M' ) × (left Γ)

  morris-chk : RawTerm m → Maybe (∃ Chk)
  morris-chk raw with decorate raw
  morris-chk raw | no = no
  morris-chk raw | yes ⟨ n , dec ⟩ with morris dec
  morris-chk raw | yes ⟨ n , dec ⟩ | no = no
  morris-chk raw | yes ⟨ n , dec ⟩ | yes ⟨ n' , subs ⟩
    = yes ⟨ n' , decChk (dec-t-map (apply subs) dec) ⟩

  ∃Infer : Set
  ∃Infer = ∃ λ n → ∃ λ r → ∃ λ Γ → Infer {n} Γ r

  infer : (r : RawTerm m) → Maybe ∃Infer
  infer r with morris-chk r
  infer r | no = no
  infer r | yes ⟨ n , chk × Γ ⟩
    = yes ⟨ n , ⟨ chk , ⟨ Γ , (check Γ chk) ⟩ ⟩ ⟩
