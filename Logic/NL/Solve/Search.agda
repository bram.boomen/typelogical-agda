module NL.Solve.Search where

open import NL hiding (_+_)
open import Struct.Pair

private
  variable
    n m : ℕ

open NonAssociative&NonCommutative public

open import Struct.List hiding ([_])

data Polarity : Set where
  + : Polarity
  - : Polarity

polarity : Polarity → Type n → List (Type n)
polarity + (var x) = []
polarity + (A ⟫ B) = (polarity - A) ++ (polarity + B)
polarity + (B ⟪ A) = (polarity + B) ++ (polarity - A)
polarity - (var x) = []
polarity - (A ⟫ B) = A ∷ ((polarity + A) ++ (polarity - B))
polarity - (B ⟪ A) = A ∷ ((polarity - B) ++ (polarity + A))

arg : Type n → List (Type n)
arg (var x) = []
arg (A ⟫ B) = A ∷ []
arg (B ⟪ A) = A ∷ []

all : Type n → List (Type n)
all (var x) = var x ∷ []
all (A ⟫ B) = A ⟫ B ∷ (all A ++ all B)
all (B ⟪ A) = B ⟪ A ∷ (all A ++ all B)

Judgement : (n : ℕ) → Set
Judgement n = Pair (Env n) (Type n)

_≡J_ : (j1 j2 : Judgement n) → Dec (j1 ≡ j2)
(env1 × type1) ≡J (env2 × type2) with type1 ≡T type2
((env1 × type1) ≡J ( env2 ×  type2)) | no ¬t=t = no (¬cong-T ¬t=t)
  where
  ¬cong-T : {t1 t2 : Type n}{e1 e2 : Env n} → ¬ (t1 ≡ t2) → ¬ (e1 × t1 ≡ e2 × t2)
  ¬cong-T ¬t=t refl = ¬t=t refl
((env1 × type1) ≡J ( env2 × .type1)) | yes refl with env1 ≡E env2
((env1 × type1) ≡J (.env1 × .type1)) | yes refl | yes refl = yes refl
((env1 × type1) ≡J ( env2 × .type1)) | yes refl | no ¬e=e = no (¬cong-E ¬e=e)
  where
  ¬cong-E : {t1 t2 : Type n}{e1 e2 : Env n} → ¬ (e1 ≡ e2) → ¬ (e1 × t1 ≡ e2 × t2)
  ¬cong-E ¬e=e refl = ¬e=e refl

_∈J_ : (j : Judgement n) → (js : List (Judgement n)) → Dec (j Struct.List.∈ js)
j ∈J js = j ∈? js [ _≡J_ ]


module SearchBU where

  zip-⟫ : {Γ Δ : Env n}{A B : Type n} → List (Term Γ A)
        → List (Term Δ (A ⟫ B)) → List (Term (Γ ∙ Δ) B)
  zip-⟫ [] _ = []
  zip-⟫ (A ∷ As) A⟫Bs = (Lmap (λ N → A ▸ N) A⟫Bs) ++ (zip-⟫ As A⟫Bs)
  zip-⟪ : {Γ Δ : Env n}{A B : Type n} → List (Term Γ (B ⟪ A))
        → List (Term Δ A) → List (Term (Γ ∙ Δ) B)
  zip-⟪ [] _ = []
  zip-⟪ (B⟪A ∷ B⟪As) As = (Lmap (λ M → B⟪A ◂ M) As) ++ (zip-⟪ B⟪As As)

  get-hypotheses : Env n → Type n → List (Type n)
  get-hypotheses Γ A = unique _≡T_ (all A ++ (f-collect all Γ))

  {-# TERMINATING #-}
  search : (Γ : Env n)(A : Type n)(o : List (Judgement n)) → List (Term Γ A)
  
  search-app : (A : Type n) → {Γ Δ : Env n} → List (Judgement n) → List (Term (Γ ∙ Δ) A)
  search-app A {Γ} {Δ} o = search-app' Γ Δ A (get-hypotheses (Γ ∙ Δ) A) o
    where
    search-app' : (Γ Δ : Env n) → (A : Type n) → List (Type n)
                → List (Judgement n) → List (Term (Γ ∙ Δ) A)
    search-app' Γ Δ A [] o = []
    search-app' Γ Δ A (H ∷ xs) o
      =  (zip-⟫ (search Γ H o) (search Δ (H ⟫ A) o))
      ++ (zip-⟪ (search Γ (A ⟪ H) o) (search Δ H o))
      ++ search-app' Γ Δ A xs o
  
  search-ax : (A : Type n) → {Γ : Env n} → List (Term Γ A)
  search-ax A {[ A' ]} with A' ≡T A
  search-ax A {[ .A ]} | yes refl = $ ∷ []
  search-ax A {[ A' ]} | no ¬p = []
  search-ax A {_ ∙ _} = []

  search-abs◃ : {Γ : Env n} → (A : Type n) → {B : Type n} → List (Judgement n) → List (Term Γ (B ⟪ A))
  search-abs◃ {Γ = Γ} A {B} o = Lmap (ƛ A ◃_) (search (Γ ∙ [ A ]) B o)
  search-abs▹ : {Γ : Env n} → (A : Type n) → {B : Type n} → List (Judgement n) → List (Term Γ (A ⟫ B))
  search-abs▹ {Γ = Γ} A {B} o = Lmap (ƛ A ▹_) (search ([ A ] ∙ Γ) B o)
  
  search Γ A o with (Γ × A) ∈J o
  search Γ A o | yes p = []
  search Γ A o | no ¬p with (Γ × A ∷ o)
  
  search [ C ]   (var x) _ | no _ | os = search-ax (var x)
  search (Γ ∙ Δ) (var x) _ | no _ | os = search-app (var x) os
  
  search [ C ]   (A ⟫ B) _ | no _ | os
    =  search-ax (A ⟫ B)
    ++ search-abs▹ A os
  search (Γ ∙ Δ) (A ⟫ B) _ | no _ | os
    =  search-abs▹ A os
    ++ search-app (A ⟫ B) os

  search [ C ]   (B ⟪ A) _ | no _ | os
    =  search-ax (B ⟪ A)
    ++ search-abs◃ A os
  search (Γ ∙ Δ) (B ⟪ A) _ | no _ | os
    =  search-abs◃ A os
    ++ search-app (B ⟪ A) os

-------------------------------------------------------------------
module SearchTD where
  open import Struct.List.Decidable

  ∃Term : (n : ℕ) → Set
  ∃Term n = ∃₂ λ Γ A → Term {n} Γ A

  axioms : Judgement n → List (∃Term n)
  axioms (Γ × A) = Lmap (λ A → ⟨ [ A ] , ⟨ A , $ ⟩ ⟩)
    (unique _≡T_ (A ∷ collect Γ ++ f-collect (polarity +) Γ ++ polarity - A))

  _t∈_ : (A : Type n) → List (∃Term n) → List (∃ λ Γ → Term Γ A)
  A t∈ [] = []
  A t∈ (⟨ Γ , ⟨ A' , t ⟩ ⟩ ∷ xs) with A ≡T A'
  A t∈ (⟨ Γ , ⟨ .A , t ⟩ ⟩ ∷ xs) | yes refl = ⟨ Γ , t ⟩ ∷ A t∈ xs
  A t∈ (⟨ Γ , ⟨ A' , t ⟩ ⟩ ∷ xs) | no ¬p = A t∈ xs

  _j∈_ : (J : Judgement n) → List (∃Term n) → Maybe (Term (fst J) (snd J))
  _ j∈ [] = no
  (Γ × A) j∈ (⟨ Γ' , ⟨ A' , t ⟩ ⟩ ∷ xs) with (Γ × A) ≡J (Γ' × A')
  (Γ × A) j∈ (⟨ .Γ , ⟨ .A , t ⟩ ⟩ ∷ xs) | yes refl = (Γ × A) j∈ xs
  (Γ × A) j∈ (⟨ Γ' , ⟨ A' , t ⟩ ⟩ ∷ xs) | no ¬p = (Γ × A) j∈ xs

  _++>_ : List (∃Term n) → List (∃Term n) → List (∃Term n)
  [] ++> L = L
  (⟨ Γ , ⟨ A , t ⟩ ⟩ ∷ xs) ++> L with (Γ × A) j∈ L
  (⟨ Γ , ⟨ A , t ⟩ ⟩ ∷ xs) ++> L | no = ⟨ Γ , ⟨ A , t ⟩ ⟩ ∷ (xs ++> L)
  (⟨ Γ , ⟨ A , t ⟩ ⟩ ∷ xs) ++> L | yes x = xs ++> L
  _+>_ : ∃Term n → List (∃Term n) → List (∃Term n)
  t +> L = t ∷ [] ++> L

  SearchI : List (∃Term n) → List (∃Term n)
  SearchI D = SearchI' D D
    where
    SearchI' : List (∃Term n) → List (∃Term n) → List (∃Term n)
    SearchI' [] lib = []
    SearchI' (⟨ Γ ∙ [ A ] , ⟨ B , t ⟩ ⟩ ∷ D) lib
      = ⟨ Γ , ⟨ B ⟪ A , (ƛ A ◃ t) ⟩ ⟩ +> SearchI' D lib
    SearchI' (⟨ [ A ] ∙ Γ , ⟨ B , t ⟩ ⟩ ∷ D) lib
      = ⟨ Γ , ⟨ A ⟫ B , (ƛ A ▹ t) ⟩ ⟩ +> SearchI' D lib
    SearchI' (_ ∷ D) lib = SearchI' D lib

  zip-⟪ : {Γ : Env n}{A B : Type n} → Term Γ (B ⟪ A) → (∃ λ Δ → Term Δ A) → ∃Term n
  zip-⟪ {Γ = Γ} {B = B} f ⟨ Δ , a ⟩ = ⟨ Γ ∙ Δ , ⟨ B , f ◂ a ⟩ ⟩
  zip-⟫ : {Γ : Env n}{A B : Type n} → Term Γ (A ⟫ B) → (∃ λ Δ → Term Δ A) → ∃Term n
  zip-⟫ {Γ = Γ} {B = B} f ⟨ Δ , a ⟩ = ⟨ Δ ∙ Γ , ⟨ B , a ▸ f ⟩ ⟩

  SearchE : List (∃Term n) → List (∃Term n)
  SearchE D = SearchE' D D
    where
    SearchE' : List (∃Term n) → List (∃Term n) → List (∃Term n)
    SearchE' [] lib = []
    SearchE' (⟨ Γ , ⟨ B ⟪ A , t ⟩ ⟩ ∷ D) lib with A t∈ lib
    SearchE' (⟨ Γ , ⟨ B ⟪ A , t ⟩ ⟩ ∷ D) lib | terms = (Lmap (zip-⟪ t) terms) ++> (SearchE' D lib)
    SearchE' (⟨ Γ , ⟨ A ⟫ B , t ⟩ ⟩ ∷ D) lib with A t∈ lib
    SearchE' (⟨ Γ , ⟨ A ⟫ B , t ⟩ ⟩ ∷ D) lib | terms = (Lmap (zip-⟫ t) terms) ++> (SearchE' D lib)
    SearchE' (⟨ Γ , ⟨ _ , t ⟩ ⟩ ∷ D) lib = SearchE' D lib

  {-# TERMINATING #-}
  iter : List (∃Term n) → List (∃Term n)
  iter lib with SearchI lib ++ SearchE lib
  iter lib | [] = lib
  iter lib | nlib = iter (nlib ++ lib)

  filter : (Γ : Env n)(A : Type n) → List (∃Term n) → List (Term Γ A)
  filter Γ A [] = []
  filter Γ A (⟨ Γ' , ⟨ A' , t ⟩ ⟩ ∷ xs) with (Γ × A) ≡J (Γ' × A')
  filter Γ A (⟨ .Γ , ⟨ .A , t ⟩ ⟩ ∷ xs) | yes refl = t ∷ filter Γ A xs
  filter Γ A (⟨ Γ' , ⟨ A' , t ⟩ ⟩ ∷ xs) | no ¬p = filter Γ A xs

  search : (Γ : Env n)(A : Type n) → List (Term Γ A)
  search Γ A = filter Γ A (iter (axioms (Γ × A)))

  _≡L_ : (s t : List (Type n)) → Dec (s ≡ t)
  s ≡L t = (s ≡l t) _≡T_

  axioms' : List (Type n) → Type n → List (∃Term n)
  axioms' γ A = Lmap (λ A → ⟨ [ A ] , ⟨ A , $ ⟩ ⟩)
    (unique _≡T_ (A ∷ γ ++ Lmap+ (polarity +) γ ++ polarity - A))
    where
    Lmap+ : {A : Set}(f : A → List A) → List A → List A
    Lmap+ f [] = []
    Lmap+ f (x ∷ xs) = f x ++ Lmap+ f xs

  data Infer : (γ : List (Type n))(A : Type n) → Set where
    ok : {Γ : Env n}(A : Type n)(t : Term Γ A) → Infer (collect Γ) A
    bad : {γ : List (Type n)}{A : Type n} → Infer γ A

  infer? : (γ : List (Type n))(A : Type n) → List (∃Term n) → List (Infer γ A)
  infer? γ A [] = []
  infer? γ A (⟨ Γ , ⟨ A' , t ⟩ ⟩ ∷ xs) with A ≡T A' | (collect Γ) ≡L γ
  infer? γ A (⟨ Γ , ⟨ .A , t ⟩ ⟩ ∷ xs) | yes refl | yes refl = (ok A t) ∷ (infer? γ A xs)
  infer? γ A (⟨ Γ , ⟨ A' , t ⟩ ⟩ ∷ xs) | _ | _ = bad ∷ (infer? γ A xs)

  filter' : {γ : List (Type n)}{A : Type n} → List (Infer γ A) → List (Infer γ A)
  filter' [] = []
  filter' (ok A t ∷ xs) = (ok A t) ∷ filter' xs
  filter' (bad ∷ xs)    = filter' xs

  search' : (γ : List (Type n)) → (A : Type n) → List (Infer γ A)
  search' γ A = filter' (infer? γ A (iter (axioms' γ A)))
