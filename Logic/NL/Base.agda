module NL.Base where

open import Base.Imports public

infix 11 var
infix 10 _⟫_ _⟪_
data Type (n : ℕ) : Set where
  var : Fin n → Type n
  _⟫_ : Type n → Type n → Type n
  _⟪_ : Type n → Type n → Type n
