module IL.Decidable where

open import IL.Base
open import Base.Decidable public

_≡T_ : {n : ℕ}(t s : Type n) → Dec (t ≡ s)
var x ≡T var y = cong-var (x ≡F y)
  where
  cong-var : {o : ℕ}{n m : Fin o} → Dec (n ≡ m) → Dec ((var n) ≡ (var m))
  cong-var (yes refl) = yes refl
  cong-var (no ¬p) = no (¬cong-var ¬p)
    where
    ¬cong-var : {o : ℕ}{n m : Fin o} → ¬ (n ≡ m) → ¬ ((var n) ≡ (var m))
    ¬cong-var ¬n=m refl = ¬n=m refl
(t ⊸ t₁) ≡T (s ⊸ s₁) = cong-⊸ (t ≡T s) (t₁ ≡T s₁)
  where
  cong-⊸ : {o : ℕ}{t s tt ss : Type o} → Dec (t ≡ s) → Dec (tt ≡ ss) → Dec ((t ⊸ tt) ≡ (s ⊸ ss))
  cong-⊸ (yes refl) (yes refl) = yes refl
  cong-⊸ (yes refl) (no ¬q) = no (¬cong-q ¬q)
    where
    ¬cong-q : {o : ℕ}{t s tt ss : Type o} → ¬ (tt ≡ ss) → ¬ ((t ⊸ tt) ≡ (s ⊸ ss))
    ¬cong-q ¬t=s refl = ¬t=s refl
  cong-⊸ (no ¬p) dq = no (¬cong-p ¬p)
    where
    ¬cong-p : {o : ℕ}{t s tt ss : Type o} → ¬ (t ≡ s) → ¬ ((t ⊸ tt) ≡ (s ⊸ ss))
    ¬cong-p ¬t=s refl = ¬t=s refl
-- otherwise
var x ≡T (t ⊸ t₁) = no (λ ())
(s ⊸ s₁) ≡T var x = no (λ ())

open import Struct.Vec
open import Struct.Vec.Decidable

_≡E_ : {n m : ℕ}(t s : Vec (Type n) m) → Dec (t ≡ s)
t ≡E s = (t ≡v s) _≡T_
