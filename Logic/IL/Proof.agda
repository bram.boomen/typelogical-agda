module IL.Proof where

open import IL.Base

open import Struct.Vec public

Env : (n m : ℕ) → Set
Env n m = Vec (Type n) m

private
  variable
    n m : ℕ
    A B : Type n
    Γ : Env n m
  
infix 5 _⊢_
data Judgement : Set where
  _⊢_ : Env n m → Type n → Judgement

infix 3 Proof_
data Proof_ : Judgement → Set where
  ax : A ∈ Γ → Proof Γ ⊢ A
  ⊸E : Proof Γ ⊢ B ⊸ A → Proof Γ ⊢ B → Proof Γ ⊢ A
  ⊸I : Proof (A ∷ Γ) ⊢ A → Proof Γ ⊢ A ⊸ B
