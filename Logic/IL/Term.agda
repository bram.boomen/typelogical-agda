module IL.Term where

open import IL.Base

private
  variable
    n m : ℕ
    A B : Type n

module Associative&Commutative where

  open import Struct.Vec public

  Env : (n m : ℕ) → Set
  Env n m = Vec (Type n) m

  private
    variable
      Γ : Env n m
  
  data Term : (Γ : Env n m)(A : Type n) → Set where
    $_[_] : (x : Fin m) → Γ [ x ]= A → Term Γ A
    _∙_   : Term Γ (A ⊸ B) → Term Γ A → Term Γ B
    ƛ_∘_  : (A : Type n) → Term (A ∷ Γ) B → Term Γ (A ⊸ B)

module NonAssociative&NonCommutative where

  open import Struct.Tree.sTree public

  Env : (n : ℕ)(s : Shape) → Set
  Env n s = Tree (Type n) s

  private
    variable
      s : Shape
      Γ : Env n s
    
  data Term : (Γ : Env n s)(A : Type n) → Set where
    $_[_] : (x : Path s) → Γ [ x ]= A → Term Γ A
    _∙_   : Term Γ (A ⊸ B) → Term Γ A → Term Γ B
    ƛ_∘_  : (A : Type n) → Term ([ A ] ∙ Γ) B → Term Γ (A ⊸ B)
