module IL.Solve.Check where

open import IL

private
  variable
    n m : ℕ
  
data RawTerm n m : Set where
  $_   : Fin m → RawTerm n m
  _∙_  : RawTerm n m → RawTerm n m → RawTerm n m
  ƛ_∘_ : Type n → RawTerm n (suc m) → RawTerm n m

module Check-Associative&Commutative where

  open Associative&Commutative
  
  erase : ∀ {Γ : Env n m}{A : Type n} → Term Γ A → RawTerm n m
  erase $ x [ p ] = $ x
  erase (N ∙ M) = (erase N) ∙ (erase M)
  erase (ƛ A ∘ M) = ƛ A ∘ (erase M)
  
  data Infer (Γ : Env n m) : RawTerm n m → Set where
    ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
    bad : {r : RawTerm n m} → Infer Γ r
  
  check : (Γ : Env n m)(r : RawTerm n m) → Infer Γ r
  check Γ ($ x) with lookup Γ x | Γ [ x ]lookup
  check Γ ($ x) | A | Γ[x]=A = ok A $ x [ Γ[x]=A ]
  
  check Γ (N ∙ M) with check Γ N | check Γ M
  check Γ (.(erase n) ∙ .(erase m)) | ok (B ⊸ C)  n | ok A m with A ≡T B
  check Γ (.(erase n) ∙ .(erase m)) | ok (.A ⊸ C) n | ok A m | yes refl = ok C (n ∙ m)
  check Γ (.(erase n) ∙ .(erase m)) | ok (B ⊸ C)  n | ok A m | no ¬p = bad
  check Γ (.(erase n) ∙ .(erase m)) | ok (var x)  n | ok A m = bad
  check Γ _ | _ | bad = bad
  check Γ _ | bad | _ = bad
  
  check Γ (ƛ A ∘ M) with check (A ∷ Γ) M
  check Γ (ƛ A ∘ .(erase m)) | ok B m = ok (A ⊸ B) (ƛ A ∘ m)
  check Γ (ƛ A ∘ M) | bad = bad
