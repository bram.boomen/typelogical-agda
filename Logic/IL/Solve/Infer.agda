module IL.Solve.Infer where

open import IL
open import IL.Decorate
open import IL.Solve.Check renaming (RawTerm to ChkTerm)

private
  variable
    n m : ℕ

morris-mgu : (t : DecTerm m n) → Maybe (∃ AList n) → Maybe (∃ AList n)
morris-mgu t no = no
morris-mgu (A ,$ x)     (yes sub) = yes sub
morris-mgu (A , N ∙ M)  (yes sub) = let subs = amgu (dec-type N) ((dec-type M) ⊸ A) sub
                                in morris-mgu N (morris-mgu M subs)
morris-mgu (A ,ƛ B ∘ M) (yes sub) = let subs = amgu A (B ⊸ dec-type M) sub
                                in morris-mgu M subs

morris : (t : DecTerm m n) → Maybe (∃ AList n)
morris t = morris-mgu t (yes ø-sub)

module Infer-Associative&Commutative where
  open Associative&Commutative
  open Check-Associative&Commutative
  open import Struct.Pair

  Chk : (n m : ℕ) → Set
  Chk n m = Pair (ChkTerm n m) (Env n m)

  decChk : DecTerm m (suc n) → Chk (suc n) m
  decChk dec = decChk' dec (replicate (var f0))
    where
    decChk' : DecTerm m n → Env n m → Chk n m
    decChk' (A ,$ x) acc = ($ x) × (acc [ x ]≔ A)
    decChk' (A , N ∙ M) acc with decChk' N acc
    decChk' (A , N ∙ M) _ | N' × acc with decChk' M acc
    decChk' (A , N ∙ M) _ | N' × _ | M' × acc = (N' ∙ M') × acc
    decChk' (A ,ƛ B ∘ M) acc with decChk' M (B ∷ acc)
    decChk' (A ,ƛ B ∘ M) _ | M' × acc = (ƛ B ∘ M') × (tail acc)

  morris-chk : RawTerm m → Maybe (∃ λ n → Chk n m)
  morris-chk raw with decorate raw
  morris-chk raw | no = no
  morris-chk raw | yes ⟨ n , dec ⟩ with morris dec
  morris-chk raw | yes ⟨ n , dec ⟩ | no = no
  morris-chk raw | yes ⟨ n , dec ⟩ | yes ⟨ 0 , t ⟩ = no -- no type variables
  morris-chk raw | yes ⟨ n , dec ⟩ | yes ⟨ suc n' , subs ⟩
    = yes ⟨ suc n' , decChk (dec-t-map (apply subs) dec) ⟩

  infer-chk : (C : Chk n m) → Infer (snd C) (fst C)
  infer-chk (r × Γ) = check Γ r

  ∃Infer : (m : ℕ) → Set
  ∃Infer m = ∃ λ n → (∃ λ r → (∃ λ Γ → Infer {n} {m} Γ r))
  
  infer : (r : RawTerm (suc m)) → Maybe (∃Infer (suc m))
  infer r with morris-chk r
  infer r | no = no
  infer r | yes ⟨ n , chk × Γ ⟩ = 
    yes ⟨ n , ⟨ chk , ⟨ Γ , check Γ chk ⟩ ⟩ ⟩
