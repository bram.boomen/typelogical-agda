open import Struct.Modular.Base
open import Modular.Type

module Modular.Unification (Struct : Set → Set)
                           (IsStruct : ∀ {A} → isStruct Struct A)
                           (Type : ℕ → Set)
                           (IsType : isType Type)
                           where

open import Modular.Env Struct IsStruct Type IsType
open import Data.Maybe public using (Maybe) renaming (nothing to no; just to yes)
open import Agda.Builtin.Bool public

open isStruct {{...}}
open isType {{...}}

_∘_ : {A B C : Set} → (B → C) → (A → B) → (A → C)
(f ∘ g) x = f (g x)

_▿ : {A B C : Set} → (A → B → C) → (B → A → C)
(f ▿) a b = f b a

▸_ : {S T : Set}(f : S → T) → (S → Maybe T)
▸ f = yes ∘ f

_◂ : {S T : Set}(f : S → Maybe T) → (Maybe S → Maybe T)
(f ◂) no = no
(f ◂) (yes s) = f s

▸_◂¹  : {S T : Set}(f : S → T) → (Maybe S → Maybe T)
▸ f ◂¹ = (▸ f) ◂

▸_◂²  : {R S T : Set}(f : R → S → T) → (Maybe R → Maybe S → Maybe T)
▸ f ◂² no no = no
▸ f ◂² no (yes t) = no
▸ f ◂² (yes r) no = no
▸ f ◂² (yes r) (yes s) = yes (f r s)

▹_ : {m n : ℕ} → (Fin m → Fin n) → (Fin m → Type n)
▹ r = `var ∘ r

▹map : {m n : ℕ} → ((Fin m → Fin n) → Type n → Type m) → ((Fin m → Type n) → Type n → Type m)
▹map map x₁ x₂ = {!!}

_◃ : {{_ : isType Type}}{m n : ℕ} → (Fin m → Type n) → (Type m → Type n)
_◃ ⦃ x ⦄ f t = `map {!!} {!!}
-- (f ◃) (`var x) = f x
-- (f ◃) (fun `⟫ arg) = ((f ◃) fun) ⊸ ((f ◃) arg)
-- TODO isType needs a =map= function

▹_◃ : {m n : ℕ} → (Fin m → Fin n) → (Type m → Type n)
▹ f ◃ = (▹ f) ◃

_◇_ : {l m n : ℕ}(f : Fin m → Type n) → (g : Fin l → Type m)
    → (Fin l → Type n)
f ◇ g = (f ◃) ∘ g

-----

thin : {n : ℕ}(x : Fin (suc n))(y : Fin n) → Fin (suc n)
thin f0 y = fs y
thin (fs x) f0 = f0
thin (fs x) (fs y) = fs (thin x y)

thick : {n : ℕ}(x y : Fin (suc n)) → Maybe (Fin n)
thick f0 f0 = no
thick f0 (fs y) = yes y
thick {suc n} (fs x) f0 = yes f0
thick {suc n} (fs x) (fs y) = ▸ fs ◂¹ (thick x y)

check : {n : ℕ}(x : Fin (suc n))(t : Type (suc n)) → Maybe (Type n)
check x t = {!!}
-- check x (var v) = ▸ var ◂¹ (thick x v)
-- check x (f ⊸ a) = ▸ _⊸_ ◂² (check x f) (check x a)

_for_ : {n : ℕ}(t' : Type n)(x : Fin (suc n)) → (Fin (suc n) → Type n)
(t' for x) y with thick x y
... | yes y' = `var y'
... | no     = t'

----------

data AList : (m n : ℕ) → Set where
  []     : {m n : ℕ} → AList n n
  _[_/_] : {m n : ℕ}(σ : AList m n)(t' : Type m)(x : Fin (suc m))
            → AList (suc m) n

sub : {m n : ℕ}(σ : AList m n) → (Fin m → Type n)
sub [] = `var
sub (σ [ t' / x ]) = (sub σ) ◇ (t' for x)

_+A+_ : {l m n : ℕ}(ρ : AList m n)(σ : AList l m) → AList l n
ρ +A+ [] = ρ
ρ +A+ (σ [ t / x ]) = (ρ +A+ σ) [ t / x ]

data ∃_ {S : Set}(T : S → Set) : Set where
  ⟨_,_⟩ : (s : S) → (t : T s) → ∃ T

_[_/_]' : {m : ℕ}(a : ∃ (AList m))(t' : Type m)(x : Fin (suc m))
         → ∃ (AList (suc m))
⟨ n , σ ⟩ [ t' / x ]' = ⟨ n , σ [ t' / x ] ⟩

ø-sub : {m : ℕ} → ∃ AList m
ø-sub {m} = ⟨ m , [] {m} ⟩

targ : {S : Set}{T : S → Set} → ∃ T → S
targ ⟨ n , t ⟩ = n
targ? : {S : Set}{T : S → Set} → Maybe (∃ T) → Maybe S
targ? no = no
targ? (yes ⟨ s , t ⟩) = yes s
targ' : {m : ℕ} → ∃ (AList m) → ℕ
targ' ⟨ n , t ⟩ = n

∃⁺ : {S : Set}{T : S → Set}{s : S}(t : T s) → ∃ T
∃⁺ {S}{T}{s} t = ⟨ s , t ⟩

∃⁻ : {S : Set}{T : S → Set} → (e : ∃ T) → T (targ e)
∃⁻ ⟨ s , t ⟩ = t

flexFlex : {m : ℕ}(x y : Fin m) → ∃ (AList m)
flexFlex {zero} () y
flexFlex {suc m} x y with thick x y
flexFlex {suc m} x y | yes y' = ⟨ m , [] {m} [ `var y' / x ] ⟩
flexFlex {suc m} x y | no = ⟨ (suc m) , [] {suc m} ⟩

flexRigid : {m : ℕ}(x : Fin m)(t : Type m) → Maybe (∃ (AList m))
flexRigid {zero} () t
flexRigid {suc m} x t with check x t
... | yes t' = yes ⟨ m , [] {m} [ t' / x ] ⟩
... | no = no

----------
