open import Struct.Modular.Base
open import Modular.Type

module Modular.ILL.Proof (Struct : Set → Set)
                         (IsStruct : ∀ {A} → isStruct Struct A)
                         (Type : ℕ → Set)
                         (IsType : isType Type)
                         where

open import Modular.Env Struct IsStruct Type IsType
open isStruct {{...}}
open isType {{...}}
open Map

private
  variable
    n m : ℕ
    A B : Type n
    Γ Δ Ω : Env n m

infix 5 _⊢_
data Sequent : Set where
  _⊢_ : Env n m → Type n → Sequent

infix 3 Proof_
data Proof_ : Sequent → Set where
  ax : (A : Map n m) → Proof `[ A ] ⊢ (type A)
  ⊸E : {A B : Type n}{Ω Γ Δ : Env n m} → Ω `> Γ / Δ → Proof Γ ⊢ A `⟫ B → Proof Δ ⊢ A → Proof Ω ⊢ B
  ⊸I : (A : Map n m) → A `∈ Γ → Proof Γ ⊢ B → Proof Γ ⊢ (type A) `⟫ B
