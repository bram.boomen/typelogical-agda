open import Struct.Modular.Base
open import Modular.Type

module Modular.ILL.Term (Struct : Set → Set)
                       (IsStruct : ∀ {A} → isStruct Struct A)
                       (Type : ℕ → Set)
                       (IsType : isType Type)
                       where

open import Modular.Env Struct IsStruct Type IsType
open isStruct {{...}}
open isType {{...}}

private
  variable
    n m : ℕ
    A B : Type n
    Γ Δ Ω : Env n m

data Term {n m : ℕ} : (Γ : Env n m)(α : Type n) → Set where
  ι_      : (τ : Tfin m A) → Term `[ τ *tm ] A
  _∙_[_]  : Term Γ (A `⟫ B) → Term Δ A → Ω `> Γ / Δ → Term Ω B
  ƛ_∘_[_] : (τ : Tfin m A) → Term Γ B → τ ∈⋆ Γ → Term Γ (A `⟫ B)
