open import Struct.Modular.Base

module Modular.Type where

open import Data.Fin as 𝔽 public using (Fin) renaming (zero to f0; suc to fs) 
open import Data.Nat as ℕ public

private
  variable
    n m : ℕ

record isType (Type : ℕ → Set) : Set where
  field
    `var : Fin n → Type n
    _`⟫_ : Type n → Type n → Type n
    _`⟪_ : Type n → Type n → Type n
    `map : (Fin n → Fin m) → Type n → Type m
