module Modular.Proofs where

module IL where

  module ASet where

    open import Struct.ASet
    open import Struct.ASet.Properties
    open import Modular.IL.Type ASet ASet-isStruct
    open import Modular.IL.Proof ASet ASet-isStruct Type IsType public

  module Tree where

    open import Struct.Tree
    open import Struct.Tree.Properties
    open import Modular.IL.Type Tree Tree-isStruct
    open import Modular.IL.Proof Tree Tree-isStruct Type IsType public

module ILL where

  module MSet where

    open import Struct.MSet
    open import Struct.MSet.Properties
    open import Modular.IL.Type MSet MSet-isStruct
    open import Modular.ILL.Proof MSet MSet-isStruct Type IsType public

  module Tree where

    open import Struct.Tree
    open import Struct.Tree.Properties
    open import Modular.IL.Type Tree Tree-isStruct
    open import Modular.ILL.Proof Tree Tree-isStruct Type IsType public
