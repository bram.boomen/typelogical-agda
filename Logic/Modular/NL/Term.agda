open import Struct.Modular.Base

module Modular.NL.Term (Struct : Set → Set)
                       (IsStruct : ∀ {A} → isStruct Struct A)
                       where

open import NL.Base
open isStruct {{...}}

private
  variable
    n m : ℕ
    A B : Type n
    Γ Δ Ω : Struct (Type n)

instance
  isEnv = IsStruct

data Term {n : ℕ} : (Γ : Struct (Type n))(A : Type n) → Set where
  ι_      : ` A 𝟙 Γ → Term Γ A
  _◂_[_]  : Term Γ (B ⟪ A) → Term Δ A → Ω `≡ Γ + Δ → Term Ω B
  _▸_[_]  : Term Γ A → Term Δ (A ⟫ B) → Ω `≡ Γ + Δ → Term Ω B
  ƛ_▹_[_] : (A : Type n) → Term (A `∷ Γ) B → Term Γ (A ⟫ B)
  ƛ_◃_[_] : (A : Type n) → Term (Γ `∷ʳ A) B → Term Γ (B ⟪ A)
