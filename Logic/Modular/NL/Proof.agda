open import Struct.Modular.Base
open import Modular.Type

module Modular.NL.Proof (Struct : Set → Set)
                        (IsStruct : ∀ {A} → isStruct Struct A)
                        (Type : ℕ → Set)
                        (IsType : isType Type)
                        where

open import Modular.Env Struct IsStruct Type IsType

open isStruct {{...}}
open isType {{...}}
open Map

private
  variable
    n m : ℕ
    A B : Type n
    f   : Fin m
    Γ Δ Ω : Env n m

infix 5 _⊢_
data Sequent : Set where
  _⊢_ : Env n m → Type n → Sequent

infix 3 Proof_
data Proof_ : Sequent → Set where
  ax : (A : Map n m) → Proof `[ A ] ⊢ (type A)
  ⟫E : {A B : Type n}{Ω Γ Δ : Env n m} → Ω `> Γ / Δ → Proof Γ ⊢ B → Proof Δ ⊢ B `⟫ A → Proof Ω ⊢ A
  ⟪E : {A B : Type n}{Ω Γ Δ : Env n m} → Ω `> Γ / Δ → Proof Γ ⊢ A `⟪ B → Proof Δ ⊢ B → Proof Ω ⊢ A
  ⟫I : (B : Map n m) → B `∈ˡ Γ → Proof Γ ⊢ A → Proof Γ ⊢ (type B) `⟫ A
  ⟪I : (B : Map n m) → B `∈ʳ Γ → Proof Γ ⊢ A → Proof Γ ⊢ A `⟪ (type B)
