open import Struct.Modular.Base

module Modular.NL.Type (Struct : Set → Set)(IsStruct : ∀ {A} → isStruct Struct A) where

open import Modular.Type

infix 11 var
infix 10 _⟫_ _⟪_
data Type (n : ℕ) : Set where
  var : Fin n → Type n
  _⟫_ : Type n → Type n → Type n
  _⟪_ : Type n → Type n → Type n

IsType : ∀ {n : ℕ} → isType Type
IsType = record { `var = var
                ; _`⟫_ = _⟫_
                ; _`⟪_ = _⟪_ }
