open import Struct.Modular.Base
open import Modular.Type

module Modular.Env (Struct : Set → Set)
                   (IsStruct : ∀ {A} → isStruct Struct A)
                   (Type : ℕ → Set)
                   (IsType : isType Type)
                   where

record Map (n m : ℕ) : Set where
  constructor _:=_
  field
    fin  : Fin m
    type : Type n

data Tfin {n : ℕ}(m : ℕ)(A : Type n) : Set where
  fin : ∀ (f : Fin m) → Tfin m A

newfin : ∀ {n m} → (A : Type n) → Fin m → Tfin m A
newfin {n} {m} A = fin {n} {m} {A} 

_⋆t : {m n : ℕ}{A : Type n} → Tfin m A → Type n
_⋆t {m} {n} {A} tf = A
_⋆f : {m n : ℕ}{A : Type n} → Tfin m A → Fin m
_⋆f (fin f) = f

_*tm : {m n : ℕ}{A : Type n} → Tfin m A → Map n m
_*tm τ = (τ ⋆f) := (τ ⋆t)
_*mt : {m n : ℕ}{A : Type n} → Map n m → Tfin m A
_*mt {m} {n} {A} (f := type) = fin {n} {m} {A} f

Env : (n m : ℕ) → Set
Env n m = Struct (Map n m)

open isStruct {{...}}

instance
  S : ∀ {A} → isStruct Struct A
  S = IsStruct
instance
  T : isType Type
  T = IsType

_∈⋆_ : {n m : ℕ}{A : Type n}(τ : Tfin m A) → Env n m → Set
τ ∈⋆ env = (τ *tm) `∈ env

_∈*_ : {n m : ℕ}(x : Type n) → Env n m → Set
x ∈* env = x `∈ (`map Map.type env)
