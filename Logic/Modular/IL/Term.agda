open import Struct.Modular.Base
open import Modular.Type

module Modular.IL.Term (Struct : Set → Set)
                       (IsStruct : ∀ {A} → isStruct Struct A)
                       (Type : ℕ → Set)
                       (IsType : isType Type)
                       where

open import Modular.Env Struct IsStruct Type IsType
open isStruct {{...}}
open isType {{...}}

private
  variable
    n m : ℕ
    A B : Type n
    Γ : Env n m

data Term {n m : ℕ} : (Γ : Env n m)(α : Type n) → Set where
  ι_   : (τ : Tfin m A) → τ ∈⋆ Γ → Term Γ A
  _∙_  : Term Γ (A `⟫ B) → Term Γ A → Term Γ B
  ƛ_∘_ : (τ : Tfin m A) → Term Γ B → τ ∈⋆ Γ → Term Γ (A `⟫ B)
