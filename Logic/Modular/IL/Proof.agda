open import Struct.Modular.Base
open import Modular.Type

module Modular.IL.Proof (Struct : Set → Set)
                        (IsStruct : ∀ {A} → isStruct Struct A)
                        (Type : ℕ → Set)
                        (IsType : isType Type)
                        where

open import Modular.Env Struct IsStruct Type IsType
open isStruct {{...}}
open isType {{...}}

private
  variable
    n m : ℕ
    A B : Type n
    Γ Δ : Env n m

infix 5 _⊢_
data Sequent : Set where
  _⊢_ : Env n m → Type n → Sequent

infix 3 Proof_
data Proof_ : Sequent → Set where
  ax : (A : Type n) → A ∈* Γ → Proof Γ ⊢ A
  ⊸I : {A B : Type n} → B ∈* Γ → Proof Γ ⊢ A → Proof Γ ⊢ B `⟫ A
  ⊸E : {Γ Δ : Env n m} → Proof Γ ⊢ B `⟫ A → Proof Δ ⊢ B → Proof (Γ `++ Δ) ⊢ A
