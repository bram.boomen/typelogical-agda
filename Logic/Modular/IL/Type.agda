open import Struct.Modular.Base

module Modular.IL.Type (Struct : Set → Set)(IsStruct : ∀ {A} → isStruct Struct A) where

open import Modular.Type

infix 11 var
infix 10 _⊸_
data Type (n : ℕ) : Set where
  var : Fin n → Type n
  _⊸_ : Type n → Type n → Type n

↺_ : {n : ℕ} → (Type n → Type n → Type n) → (Type n → Type n → Type n)
(↺ f) t₁ t₂ = f t₂ t₁

IsType : isType Type
IsType = record { `var = var
                ; _`⟫_ = _⊸_
                ; _`⟪_ = ↺ (_⊸_) }

-- justification

-- A ∙ A `⟫ B = B
-- A ∙ A ⊸ B = B
