module NLD.Unification where

open import Base.Unification public hiding (_∘_)
open import Base.Imports using () renaming (_<$>_ to _∘_)

open import NLD.Base
open import NLD.Decidable

▹_ : {m n : ℕ} → (Fin m → Fin n) → (Fin m → Type n)
▹ r = var ∘ r

_◃ : {m n : ℕ} → (Fin m → Type n) → (Type m → Type n)
(f ◃) (var x) = f x
(f ◃) (fun ⟪ arg) = ((f ◃) fun) ⟪ ((f ◃) arg)
(f ◃) (arg ⟫ fun) = ((f ◃) arg) ⟫ ((f ◃) fun)
(f ◃) (□ x) = □ ((f ◃)x)
(f ◃) (◇ x) = ◇ ((f ◃)x)

▹_◃ : {m n : ℕ} → (Fin m → Fin n) → (Type m → Type n)
▹ f ◃ = (▹ f) ◃

_◇_ : {l m n : ℕ}(f : Fin m → Type n) → (g : Fin l → Type m)
    → (Fin l → Type n)
f ◇ g = (f ◃) ∘ g

check : {n : ℕ}(x : Fin (suc n))(t : Type (suc n)) → Maybe (Type n)
check x (var v) = ▸ var ◂¹ (thick x v)
check x (f ⟪ a) = ▸ _⟪_ ◂² (check x f) (check x a)
check x (f ⟫ a) = ▸ _⟫_ ◂² (check x f) (check x a)
check x (□ a) = ▸ □_ ◂¹ (check x a)
check x (◇ a) = ▸ ◇_ ◂¹ (check x a)

_for_ : {n : ℕ}(t' : Type n)(x : Fin (suc n)) → (Fin (suc n) → Type n)
(t' for x) y with thick x y
... | yes y' = var y'
... | no     = t'

data AList : (m n : ℕ) → Set where
  []     : {m n : ℕ} → AList n n
  _[_/_] : {m n : ℕ}(σ : AList m n)(t' : Type m)(x : Fin (suc m))
            → AList (suc m) n

sub : {m n : ℕ}(σ : AList m n) → (Fin m → Type n)
sub [] = var
sub (σ [ t' / x ]) = (sub σ) ◇ (t' for x)

_+A+_ : {l m n : ℕ}(ρ : AList m n)(σ : AList l m) → AList l n
ρ +A+ [] = ρ
ρ +A+ (σ [ t / x ]) = (ρ +A+ σ) [ t / x ]

_[_/_]' : {m : ℕ}(a : ∃ (AList m))(t' : Type m)(x : Fin (suc m))
         → ∃ (AList (suc m))
⟨ n , σ ⟩ [ t' / x ]' = ⟨ n , σ [ t' / x ] ⟩

ø-sub : {m : ℕ} → ∃ AList m
ø-sub {m} = ⟨ m , [] {m} ⟩

flexFlex : {m : ℕ}(x y : Fin m) → ∃ (AList m)
flexFlex {zero} () y
flexFlex {suc m} x y with thick x y
flexFlex {suc m} x y | yes y' = ⟨ m , [] {m} [ var y' / x ] ⟩
flexFlex {suc m} x y | no = ⟨ (suc m) , [] {suc m} ⟩

flexRigid : {m : ℕ}(x : Fin m)(t : Type m) → Maybe (∃ (AList m))
flexRigid {zero} () t
flexRigid {suc m} x t with check x t
... | yes t' = yes ⟨ m , [] {m} [ t' / x ] ⟩
... | no = no

amgu : {m : ℕ}(s t : Type m)(acc : ∃ (AList m)) → Maybe (∃ (AList m))
amgu (◇ _) (□ _) acc = no
amgu (□ _) (◇ _) acc = no
amgu (◇ _) (_ ⟪ _) acc = no
amgu (□ _) (_ ⟪ _) acc = no
amgu (◇ _) (_ ⟫ _) acc = no
amgu (□ _) (_ ⟫ _) acc = no
amgu (_ ⟪ _) (◇ _) acc = no
amgu (_ ⟪ _) (□ _) acc = no
amgu (_ ⟫ _) (◇ _) acc = no
amgu (_ ⟫ _) (□ _) acc = no
amgu (_ ⟫ _) (_ ⟪ _) acc = no
amgu (_ ⟪ _) (_ ⟫ _) acc = no
amgu (◇ a) (◇ b) acc = amgu a b acc
amgu (□ a) (□ b) acc = amgu a b acc
amgu (a ⟫ f) (b ⟫ g) acc = (amgu f g ◂) (amgu a b acc)
amgu (f ⟪ a) (g ⟪ b) acc = (amgu f g ◂) (amgu a b acc)
amgu (var v) (var w) ⟨ m , [] ⟩ = yes (flexFlex v w)
amgu (var x) t       ⟨ m , [] ⟩ = flexRigid x t
amgu t       (var x) ⟨ m , [] ⟩ = flexRigid x t
amgu t₁ t₂ ⟨ n , σ [ r / z ] ⟩  = let subs x = ((r for z) ◃) x
                                  in ▸ (λ σ → (σ [ r / z ]')) ◂¹
                                    (amgu (subs t₁) (subs t₂) ⟨ n , σ ⟩)

mgu : {m : ℕ}(s t : Type m) → Maybe (∃ (AList m))
mgu {m} s t = amgu s t ⟨ m , [] {m} ⟩

apply : {n m : ℕ} → AList n m → Type n → Type m
apply subs = ((sub subs) ◃)

mgu-sub : {m : ℕ}(s t : Type m) → Maybe (∃ Type)
mgu-sub type type' with mgu type type'
mgu-sub _ _ | no = no
mgu-sub type _ | yes ⟨ n , subs ⟩ = yes ⟨ n , apply subs type ⟩
