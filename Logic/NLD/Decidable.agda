module NLD.Decidable where

open import NLD.Base
open import Struct.STree
open import Struct.STree.Decidable
open import Base.Decidable public

_≡T_ : {n : ℕ}(t s : Type n) → Dec (t ≡ s)
var x ≡T var y = cong-var (x ≡F y)
  where
  cong-var : {o : ℕ}{n m : Fin o} → Dec (n ≡ m) → Dec ((var n) ≡ (var m))
  cong-var (yes refl) = yes refl
  cong-var (no ¬p) = no (¬cong-var ¬p)
    where
    ¬cong-var : {o : ℕ}{n m : Fin o} → ¬ (n ≡ m) → ¬ ((var n) ≡ (var m))
    ¬cong-var ¬n=m refl = ¬n=m refl
(t ⟫ t₁) ≡T (s ⟫ s₁) = cong-⟫ (t ≡T s) (t₁ ≡T s₁)
  where
  cong-⟫ : {o : ℕ}{t s tt ss : Type o} → Dec (t ≡ s) → Dec (tt ≡ ss) → Dec ((t ⟫ tt) ≡ (s ⟫ ss))
  cong-⟫ (yes refl) (yes refl) = yes refl
  cong-⟫ (yes refl) (no ¬q) = no (¬cong-q ¬q)
    where
    ¬cong-q : {o : ℕ}{t s tt ss : Type o} → ¬ (tt ≡ ss) → ¬ ((t ⟫ tt) ≡ (s ⟫ ss))
    ¬cong-q ¬t=s refl = ¬t=s refl
  cong-⟫ (no ¬p) dq = no (¬cong-p ¬p)
    where
    ¬cong-p : {o : ℕ}{t s tt ss : Type o} → ¬ (t ≡ s) → ¬ ((t ⟫ tt) ≡ (s ⟫ ss))
    ¬cong-p ¬t=s refl = ¬t=s refl
(t ⟪ t₁) ≡T (s ⟪ s₁) = cong-⟪ (t ≡T s) (t₁ ≡T s₁)
  where
  cong-⟪ : {o : ℕ}{t s tt ss : Type o} → Dec (t ≡ s) → Dec (tt ≡ ss) → Dec ((t ⟪ tt) ≡ (s ⟪ ss))
  cong-⟪ (yes refl) (yes refl) = yes refl
  cong-⟪ (yes refl) (no ¬q) = no (¬cong-q ¬q)
    where
    ¬cong-q : {o : ℕ}{t s tt ss : Type o} → ¬ (tt ≡ ss) → ¬ ((t ⟪ tt) ≡ (s ⟪ ss))
    ¬cong-q ¬t=s refl = ¬t=s refl
  cong-⟪ (no ¬p) dq = no (¬cong-p ¬p)
    where
    ¬cong-p : {o : ℕ}{t s tt ss : Type o} → ¬ (t ≡ s) → ¬ ((t ⟪ tt) ≡ (s ⟪ ss))
    ¬cong-p ¬t=s refl = ¬t=s refl
(□ x) ≡T (□ y) = cong-□ (x ≡T y)
  where
  cong-□ : {o : ℕ}{n m : Type o} → Dec (n ≡ m) → Dec ((□ n) ≡ (□ m))
  cong-□ (yes refl) = yes refl
  cong-□ (no ¬p) = no (¬cong-□ ¬p)
    where
    ¬cong-□ : {o : ℕ}{n m : Type o} → ¬ (n ≡ m) → ¬ ((□ n) ≡ (□ m))
    ¬cong-□ ¬n=m refl = ¬n=m refl
(◇ x) ≡T (◇ y) = cong-◇ (x ≡T y)
  where
  cong-◇ : {o : ℕ}{n m : Type o} → Dec (n ≡ m) → Dec ((◇ n) ≡ (◇ m))
  cong-◇ (yes refl) = yes refl
  cong-◇ (no ¬p) = no (¬cong-◇ ¬p)
    where
    ¬cong-◇ : {o : ℕ}{n m : Type o} → ¬ (n ≡ m) → ¬ ((◇ n) ≡ (◇ m))
    ¬cong-◇ ¬n=m refl = ¬n=m refl
--&
var _ ≡T (_ ⟫ _) = no (λ ())
var _ ≡T (_ ⟪ _) = no (λ ())
(_ ⟫ _) ≡T var _ = no (λ ())
(_ ⟪ _) ≡T var _ = no (λ ())
(_ ⟪ _) ≡T (_ ⟫ _) = no (λ ())
(_ ⟫ _) ≡T (_ ⟪ _) = no (λ ())
(□ _) ≡T (_ ⟫ _) = no (λ ())
(□ _) ≡T (_ ⟪ _) = no (λ ())
(◇ _) ≡T (_ ⟫ _) = no (λ ())
(◇ _) ≡T (_ ⟪ _) = no (λ ())
(□ _) ≡T var _ = no (λ ())
(◇ _) ≡T var _ = no (λ ())
(_ ⟫ _) ≡T (□ _) = no (λ ())
(_ ⟪ _) ≡T (□ _) = no (λ ())
(_ ⟫ _) ≡T (◇ _) = no (λ ())
(_ ⟪ _) ≡T (◇ _) = no (λ ())
var _ ≡T (□ _) = no (λ ())
var _ ≡T (◇ _) = no (λ ())
(□ _) ≡T (◇ _) = no (λ ())
(◇ _) ≡T (□ _) = no (λ ())

_≡E_ : {n : ℕ}(t s : Tree (Type n)) → Dec (t ≡ s)
t ≡E s = (t ≡t s) _≡T_
