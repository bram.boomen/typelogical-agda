module NLD.Term where

open import NLD.Base
open import Relation.Binary.PropositionalEquality as Eq using (_≡_; refl; cong; sym)
open Eq.≡-Reasoning
open import Struct.STree public
open import Struct.STree.Order renaming (_≻_ to _≻ʳˡ_) public

Env : (n : ℕ) → Set
Env n = Tree (Type n)

private
  variable
    n : ℕ
    A B : Type n
    Γ Δ Ω : Env n

module NLD (_≻_ : {α : Set} → Tree α → Tree α → Set) where

  data Term : (Γ : Env n) → (A : Type n) → Set where
    $         : Term [ A ] A
    _◂_       : Term Γ (B ⟪ A) → Term Δ A → Term (Γ ∙ Δ) B
    _▸_       : Term Γ A → Term Δ (A ⟫ B) → Term (Γ ∙ Δ) B
    ƛ_▹_      : (A : Type n) → Term ([ A ] ∙ Γ) B → Term Γ (A ⟫ B)
    ƛ_◃_      : (A : Type n) → Term (Γ ∙ [ A ]) B → Term Γ (B ⟪ A)
    ⟨_⟩ᴵ      : Term Γ A → Term ⟨ Γ ⟩ (◇ A)
    ⟦_⟧ᴵ      : Term ⟨ Γ ⟩ A → Term Γ (□ A)
    ⟨_∙_[_]⟩ᴱ : {A∈Δ : ⟨ [ A ] ⟩ ⊆ Δ} → Term Γ (◇ A)
              → Term Δ B → Δ [ A∈Δ / Γ ]≡ Ω → Term Ω B
    ⟦_⟧ᴱ      : Term Γ (□ A) → Term ⟨ Γ ⟩ A
    ↔_[_]     : Term Γ A → Γ ≻ Δ → Term Δ A

module NLDRight where
  open NLD (_≻ʳ_) public

module NLDLeft where
  open NLD (_≻ˡ_) public
