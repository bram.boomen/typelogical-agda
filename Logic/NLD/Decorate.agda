module NLD.Decorate where

open import NLD
open import Base.DecorateUtils
open import Struct.Pair

private
  variable
    n m : ℕ

data RawTerm (m : ℕ) : Set where
  $_        : Fin m → RawTerm m
  _▸_       : RawTerm m → RawTerm m → RawTerm m
  _◂_       : RawTerm m → RawTerm m → RawTerm m
  ƛ▹_       : RawTerm (suc m) → RawTerm m
  ƛ◃_       : RawTerm (suc m) → RawTerm m
  ⟨_⟩ᴵ      : RawTerm m → RawTerm m
  ⟦_⟧ᴵ      : RawTerm m → RawTerm m
  ⟦_⟧ᴱ      : RawTerm m → RawTerm m
  ⟨_∙_[_]⟩ᴱ : RawTerm m → RawTerm m → Fin m → RawTerm m

data DecTerm (m n : ℕ) : Set where
  _,$_        : (A : Type n) → Fin m → DecTerm m n
  _,_◂_       : (A : Type n) → (N : DecTerm m n) → (M : DecTerm m n) → DecTerm m n
  _,_▸_       : (A : Type n) → (M : DecTerm m n) → (N : DecTerm m n) → DecTerm m n
  _,ƛ_▹_      : (A : Type n) → Type n → (M : DecTerm (suc m) n) → DecTerm m n
  _,ƛ_◃_      : (A : Type n) → Type n → (M : DecTerm (suc m) n) → DecTerm m n
  _,⟨_⟩ᴵ      : (A : Type n) → (M : DecTerm m n) → DecTerm m n
  _,⟦_⟧ᴵ      : (A : Type n) → (M : DecTerm m n) → DecTerm m n
  _,⟦_⟧ᴱ      : (A : Type n) → (M : DecTerm m n) → DecTerm m n
  _,⟨_∙_[_]⟩ᴱ : (A : Type n) → (M : DecTerm m n) → (N : DecTerm m n) → Fin m → DecTerm m n

dec-type : DecTerm m n → Type n
dec-type (A ,$ _) = A
dec-type (A , _ ◂ _) = A
dec-type (A , _ ▸ _) = A
dec-type (A ,ƛ _ ▹ _) = A
dec-type (A ,ƛ _ ◃ _) = A
dec-type (A ,⟨ _ ⟩ᴵ) = A
dec-type (A ,⟦ _ ⟧ᴵ) = A
dec-type (A ,⟨ _ ∙ _ [ _ ]⟩ᴱ) = A
dec-type (A ,⟦ _ ⟧ᴱ) = A

lookup-dec-type : DecTerm m n → Fin m → Maybe (Type n)
lookup-dec-type (A ,$ x) f with x ≡F f
lookup-dec-type (A ,$ x) .x | yes refl = yes A
lookup-dec-type (A ,$ x) f  | no ¬p = no
lookup-dec-type (A , t₁ ◂ t₂) f with lookup-dec-type t₁ f
lookup-dec-type (A , t₁ ◂ t₂) f | yes T = yes T
lookup-dec-type (A , t₁ ◂ t₂) f | no = lookup-dec-type t₂ f
lookup-dec-type (A , t₁ ▸ t₂) f with lookup-dec-type t₁ f
lookup-dec-type (A , t₁ ▸ t₂) f | yes T = yes T
lookup-dec-type (A , t₁ ▸ t₂) f | no = lookup-dec-type t₂ f
lookup-dec-type (A ,ƛ _ ▹ t) f = lookup-dec-type t (fs f)
lookup-dec-type (A ,ƛ _ ◃ t) f = lookup-dec-type t (fs f)
lookup-dec-type (A ,⟨ t ⟩ᴵ) f = lookup-dec-type t f
lookup-dec-type (A ,⟦ t ⟧ᴵ) f = lookup-dec-type t f
lookup-dec-type (A ,⟨ t₁ ∙ t₂ [ x ]⟩ᴱ) f with lookup-dec-type t₁ f
lookup-dec-type (A ,⟨ t₁ ∙ t₂ [ x ]⟩ᴱ) f | yes T = yes T
lookup-dec-type (A ,⟨ t₁ ∙ t₂ [ x ]⟩ᴱ) f | no = lookup-dec-type t₂ f
lookup-dec-type (A ,⟦ t ⟧ᴱ) f = lookup-dec-type t f

put-dec-type : DecTerm m n → Type n → DecTerm m n
put-dec-type (A ,$ x) B = B ,$ x
put-dec-type (A , x ◂ x₁)  B = B , x ◂ x₁
put-dec-type (A , x ▸ x₁)  B = B , x ▸ x₁
put-dec-type (A ,ƛ x ▹ x₁) B = B ,ƛ x ▹ x₁
put-dec-type (A ,ƛ x ◃ x₁) B = B ,ƛ x ◃ x₁
put-dec-type (A ,⟨ x ⟩ᴵ)   B = B ,⟨ x ⟩ᴵ
put-dec-type (A ,⟦ x ⟧ᴵ)   B = B ,⟦ x ⟧ᴵ
put-dec-type (A ,⟦ x ⟧ᴱ)   B = B ,⟦ x ⟧ᴱ
put-dec-type (A ,⟨ x ∙ x₁ [ x₂ ]⟩ᴱ) B = B ,⟨ x ∙ x₁ [ x₂ ]⟩ᴱ

abs-type : DecTerm (suc m) n → Maybe (Type n)
abs-type dec = lookup-dec-type dec f0

dec-t-map : ∀ {m n o : ℕ} → (f : Type n → Type o) → DecTerm m n → DecTerm m o
dec-t-map f (A ,$ x) = (f A) ,$ x
dec-t-map f (A , N ◂ M) = (f A) , (dec-t-map f N) ◂ (dec-t-map f M)
dec-t-map f (A , M ▸ N) = (f A) , (dec-t-map f M) ▸ (dec-t-map f N)
dec-t-map f (A ,ƛ B ▹ M) = (f A) ,ƛ (f B) ▹ (dec-t-map f M)
dec-t-map f (A ,ƛ B ◃ M) = (f A) ,ƛ (f B) ▹ (dec-t-map f M)
dec-t-map f (A ,⟨ M ⟩ᴵ) = (f A) ,⟨ dec-t-map f M ⟩ᴵ
dec-t-map f (A ,⟦ M ⟧ᴵ) = (f A) ,⟦ dec-t-map f M ⟧ᴵ
dec-t-map f (A ,⟨ M ∙ N [ x ]⟩ᴱ) = (f A) ,⟨ dec-t-map f M ∙ dec-t-map f N [ x ]⟩ᴱ
dec-t-map f (A ,⟦ M ⟧ᴱ) = (f A) ,⟦ dec-t-map f M ⟧ᴱ

type-n-map : ∀ {n o : ℕ} → (f : Fin n → Fin o) → Type n → Type o
type-n-map f (var x) = var (f x)
type-n-map f (A ⟫ B) = type-n-map f A ⟫ type-n-map f B
type-n-map f (B ⟪ A) = type-n-map f B ⟪ type-n-map f A
type-n-map f (□ A) = □ (type-n-map f A)
type-n-map f (◇ A) = ◇ (type-n-map f A)

dec-n-map : ∀ {n m o : ℕ} → (f : Fin n → Fin o) → DecTerm m n → DecTerm m o
dec-n-map f = dec-t-map (type-n-map f)

inject-dec : (o : ℕ) → DecTerm m n → DecTerm m (n + o)
inject-dec o = dec-n-map (inject+ o)

raise-dec : (o : ℕ) → DecTerm m n → DecTerm m (o + n)
raise-dec o = dec-n-map (raise+ o)

decorate : (ρ : RawTerm m) → Maybe (∃ DecTerm m)
decorate ($ x) = yes ⟨ 1 , (var f0 ,$ x) ⟩
decorate (a ▸ f) with (decorate a) & (decorate f)
decorate (a ▸ f) | no = no
decorate (a ▸ f) | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩)
  = let F' = raise-dec (1 + na) F
        A' = raise-dec 1 (inject-dec nf A)
  in yes ⟨ suc (na + nf) , (var f0) , A' ▸ F' ⟩
decorate (f ◂ a) with (decorate a) & (decorate f)
decorate (f ◂ a) | no = no
decorate (f ◂ a) | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩)
  = let F' = raise-dec (1 + na) F
        A' = raise-dec 1 (inject-dec nf A)
  in yes ⟨ suc (na + nf) , (var f0) , F' ◂ A' ⟩
decorate (ƛ◃ f) with decorate f
decorate (ƛ◃ f) | no = no
decorate (ƛ◃ f) | yes ⟨ nf , F ⟩ with raise-dec 1 F | abs-type F
decorate (ƛ◃ f) | yes ⟨ nf , F ⟩ | F' | no = no
decorate (ƛ◃ f) | yes ⟨ nf , F ⟩ | F' | yes B
  = yes ⟨ (suc nf) , (var f0 ,ƛ type-n-map (inject 1) B ◃ F') ⟩
decorate (ƛ▹ f) with decorate f
decorate (ƛ▹ f) | no = no
decorate (ƛ▹ f) | yes ⟨ nf , F ⟩ with raise-dec 1 F | abs-type F
decorate (ƛ▹ f) | yes ⟨ nf , F ⟩ | F' | no = no
decorate (ƛ▹ f) | yes ⟨ nf , F ⟩ | F' | yes B
  = yes ⟨ (suc nf) , (var f0 ,ƛ type-n-map (inject 1) B ▹ F') ⟩
decorate ⟨ t ⟩ᴵ with decorate t
decorate ⟨ t ⟩ᴵ | no = no
decorate ⟨ t ⟩ᴵ | yes ⟨ nf , F ⟩ with raise-dec 1 F
decorate ⟨ t ⟩ᴵ | yes ⟨ nf , F ⟩ | F'
  = yes ⟨ (suc nf) , var f0 ,⟨ F' ⟩ᴵ ⟩
decorate ⟦ t ⟧ᴵ with decorate t
decorate ⟦ t ⟧ᴵ | no = no
decorate ⟦ t ⟧ᴵ | yes ⟨ nf , F ⟩ with raise-dec 1 F
decorate ⟦ t ⟧ᴵ | yes ⟨ nf , F ⟩ | F'
  = yes ⟨ (suc nf) , var f0 ,⟦ F' ⟧ᴵ ⟩
decorate ⟨ f ∙ a [ x ]⟩ᴱ with (decorate a) & (decorate f)
decorate ⟨ f ∙ a [ x ]⟩ᴱ | no = no
decorate ⟨ f ∙ a [ x ]⟩ᴱ | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩) with raise-dec (1 + na) F
decorate ⟨ f ∙ a [ x ]⟩ᴱ | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩) | F' with lookup-dec-type F' x
decorate ⟨ f ∙ a [ x ]⟩ᴱ | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩) | F' | no = no
decorate ⟨ f ∙ a [ x ]⟩ᴱ | yes (⟨ na , A ⟩ × ⟨ nf , F ⟩) | F' | yes B
  = let A' = raise-dec 1 (inject-dec nf A)
        A* = put-dec-type A' B
  in yes ⟨ suc (na + nf) , (var f0) ,⟨ F' ∙ A* [ x ]⟩ᴱ ⟩
decorate ⟦ t ⟧ᴱ with decorate t
decorate ⟦ t ⟧ᴱ | no = no
decorate ⟦ t ⟧ᴱ | yes ⟨ nf , F ⟩ with raise-dec 1 F
decorate ⟦ t ⟧ᴱ | yes ⟨ nf , F ⟩ | F'
  = yes ⟨ (suc nf) , var f0 ,⟦ F' ⟧ᴱ ⟩
