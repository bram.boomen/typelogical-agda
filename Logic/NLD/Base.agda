module NLD.Base where

open import Base.Imports public

infix 12 var
infix 11 □_ ◇_
infix 10 _⟫_ _⟪_
data Type (n : ℕ) : Set where
  var : Fin n → Type n
  _⟫_ : Type n → Type n → Type n
  _⟪_ : Type n → Type n → Type n
  □_  : Type n → Type n
  ◇_  : Type n → Type n
