module NLD.Solve.Infer where

open import NLD
open import NLD.Decorate

open import NLD.Solve.Check renaming (RawTerm to ChkTerm)
open import Struct.Pair

private
  variable
    n m : ℕ

morris-mgu : (t : DecTerm m n) → Maybe (∃ AList n) → Maybe (∃ AList n)
morris-mgu t no = no
morris-mgu (A ,$ τ)     (yes sub) = yes sub
morris-mgu (A , N ◂ M)  (yes sub) = let subs = amgu (dec-type N) (A ⟪ (dec-type M)) sub
                                    in morris-mgu N (morris-mgu M subs)
morris-mgu (A , M ▸ N)  (yes sub) = let subs = amgu (dec-type N) ((dec-type M) ⟫ A) sub
                                    in morris-mgu N (morris-mgu M subs)
morris-mgu (A ,ƛ B ▹ M) (yes sub) = let subs = amgu A (B ⟫ dec-type M) sub
                                    in morris-mgu M subs
morris-mgu (A ,ƛ B ◃ M) (yes sub) = let subs = amgu A (dec-type M ⟪ B) sub
                                    in morris-mgu M subs
morris-mgu (A ,⟨ M ⟩ᴵ)     (yes sub) = let subs = amgu (◇ (dec-type M)) A sub
                                       in morris-mgu M subs
morris-mgu (A ,⟦ M ⟧ᴵ)     (yes sub) = let subs = amgu (□ (dec-type M)) A sub
                                       in morris-mgu M subs
morris-mgu (A ,⟨ N ∙ M [ x ]⟩ᴱ) (yes sub) = let subs = amgu (dec-type N) A sub
                                       in morris-mgu N (morris-mgu M subs)
morris-mgu (A ,⟦ M ⟧ᴱ)     (yes sub) = let subs = amgu (dec-type M) (□ A) sub
                                       in morris-mgu M subs

morris : (t : DecTerm m n) → Maybe (∃ AList n)
morris t = morris-mgu t (yes ø-sub)

module Infer-NLDRight where
  open NLDRight
  open Check-NLDRight

  Chk : (n : ℕ) → Set
  Chk n = Pair (ChkTerm n) (Env n)

  left right : {A : Set} → Tree A → Maybe (Tree A)
  left [ x ] = no
  left (x ∙ _) = yes x
  left ⟨ x ⟩ = no
  right [ x ] = no
  right (_ ∙ x) = yes x
  right ⟨ x ⟩ = no

  replace : Type n → Env n → Env n → Maybe (Env n)
  replace A [ x ] Δ = no
  replace A ⟨ [ B ] ⟩ Δ with A ≡T B
  replace A ⟨ [ .A ] ⟩ Δ | yes refl = yes Δ
  replace A ⟨ [ B ] ⟩ Δ | no ¬p = no
  replace A ⟨ ⟨ _ ⟩ ⟩ Δ = no
  replace A ⟨ _ ∙ _ ⟩ Δ = no
  replace A (Γ ∙ Γ') Δ with replace A Γ Δ
  replace A (Γ ∙ Γ') Δ | no with replace A Γ' Δ
  replace A (Γ ∙ Γ') Δ | no | no = no
  replace A (Γ ∙ Γ') Δ | no | yes Δ' = yes (Γ ∙ Δ')
  replace A (Γ ∙ Γ') Δ | yes Δ' = yes (Δ' ∙ Γ')

  decChk : DecTerm m n → Maybe (Chk n)
  decChk (A ,$ x) = yes ($ × [ A ])
  decChk (A , N ◂ M) with decChk N & decChk M
  decChk (A , N ◂ M) | yes ((N' × Γ) × (M' × Δ)) = yes ((N' ◂ M') × (Γ ∙ Δ))
  decChk (A , N ◂ M) | no = no
  decChk (A , N ▸ M) with decChk N & decChk M
  decChk (A , N ▸ M) | yes ((N' × Γ) × (M' × Δ)) = yes ((N' ▸ M') × (Γ ∙ Δ))
  decChk (A , N ▸ M) | no = no
  decChk (A ,ƛ B ▹ M) with decChk M
  decChk (A ,ƛ B ▹ M) | yes (M' × Γ) with right Γ
  decChk (A ,ƛ B ▹ M) | yes (M' × Γ) | yes Γ' = yes ((ƛ B ▹ M' ) × Γ')
  decChk (A ,ƛ B ▹ M) | yes (M' × Γ) | no = no
  decChk (A ,ƛ B ▹ M) | no = no
  decChk (A ,ƛ B ◃ M) with decChk M
  decChk (A ,ƛ B ◃ M) | yes (M' × Γ) with left Γ
  decChk (A ,ƛ B ◃ M) | yes (M' × Γ) | yes Γ' = yes ((ƛ B ◃ M' ) × Γ')
  decChk (A ,ƛ B ◃ M) | yes (M' × Γ) | no = no
  decChk (A ,ƛ B ◃ M) | no = no
  decChk (A ,⟨ M ⟩ᴵ) with decChk M
  decChk (A ,⟨ M ⟩ᴵ) | yes (M' × Γ) = yes (⟨ M' ⟩ᴵ × ⟨ Γ ⟩)
  decChk (A ,⟨ M ⟩ᴵ) | no = no
  decChk (A ,⟦ M ⟧ᴵ) with decChk M
  decChk (A ,⟦ M ⟧ᴵ) | yes (M' × ⟨ Γ ⟩) = yes (⟦ M' ⟧ᴵ × Γ)
  decChk (A ,⟦ M ⟧ᴵ) | yes (M' × Γ) = no
  decChk (A ,⟦ M ⟧ᴵ) | no = no
  decChk (A ,⟨ N ∙ M [ x ]⟩ᴱ) with decChk N & decChk M
  decChk (A ,⟨ N ∙ M [ x ]⟩ᴱ) | yes ((N' × Γ) × (M' × Δ)) with replace (dec-type M) Γ Δ
  decChk (A ,⟨ N ∙ M [ x ]⟩ᴱ) | yes ((N' × Γ) × (M' × Δ)) | yes Γ' = yes (⟨ N' ∙ M' ⟩ᴱ × Γ')
  decChk (A ,⟨ N ∙ M [ x ]⟩ᴱ) | yes ((N' × Γ) × (M' × Δ)) | no = no
  decChk (A ,⟨ N ∙ M [ x ]⟩ᴱ) | no = no
  decChk (A ,⟦ M ⟧ᴱ) with decChk M
  decChk (A ,⟦ M ⟧ᴱ) | yes (M' × Γ) = yes (⟦ M' ⟧ᴱ × ⟨ Γ ⟩)
  decChk (A ,⟦ M ⟧ᴱ) | no = no

  morris-chk : RawTerm m → Maybe (∃ Chk)
  morris-chk raw with decorate raw
  morris-chk raw | no = no
  morris-chk raw | yes ⟨ n , dec ⟩ with morris dec
  morris-chk raw | yes ⟨ n , dec ⟩ | no = no
  morris-chk raw | yes ⟨ n , dec ⟩ | yes ⟨ n' , subs ⟩ with decChk (dec-t-map (apply subs) dec)
  morris-chk raw | yes ⟨ n , dec ⟩ | yes ⟨ n' , subs ⟩ | no = no
  morris-chk raw | yes ⟨ n , dec ⟩ | yes ⟨ n' , subs ⟩ | yes chk = yes ⟨ n' , chk ⟩

  ∃Infer : Set
  ∃Infer = ∃ λ n → ∃ λ r → ∃ λ Γ → Infer {n} Γ r

  infer : (r : RawTerm m) → Maybe ∃Infer
  infer r with morris-chk r
  infer r | no = no
  infer r | yes ⟨ n , chk × Γ ⟩
    = yes ⟨ n , ⟨ chk , ⟨ Γ , (check Γ chk) ⟩ ⟩ ⟩
