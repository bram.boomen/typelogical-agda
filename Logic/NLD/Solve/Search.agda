module NLD.Solve.Search where

open import NLD hiding (_+_)
open import Struct.Pair

private
  variable
    n m : ℕ

open import Struct.List hiding ([_]; _⊆_)

all : Type n → List (Type n)
all (var x) = var x ∷ []
all (□ A) = □ A ∷ (all A)
all (◇ A) = ◇ A ∷ (all A)
all (A ⟫ B) = A ⟫ B ∷ (all A ++ all B)
all (B ⟪ A) = B ⟪ A ∷ (all A ++ all B)

get-hypotheses : Env n → Type n → List (Type n)
get-hypotheses Γ A = unique _≡T_ (all A ++ f-collect all Γ)

Judgement : (n : ℕ) → Set
Judgement n = Pair (Env n) (Type n)

_≡J_ : (j1 j2 : Judgement n) → Dec (j1 ≡ j2)
(env1 × type1) ≡J (env2 × type2) with type1 ≡T type2
(env1 × type1) ≡J ( env2 ×  type2) | no ¬t=t = no (¬cong-T ¬t=t)
  where
  ¬cong-T : {t1 t2 : Type n}{e1 e2 : Env n} → ¬ (t1 ≡ t2) → ¬ (e1 × t1 ≡ e2 × t2)
  ¬cong-T ¬t=t refl = ¬t=t refl
(env1 × type1) ≡J ( env2 × .type1) | yes refl with env1 ≡E env2
(env1 × type1) ≡J (.env1 × .type1) | yes refl | yes refl = yes refl
(env1 × type1) ≡J ( env2 × .type1) | yes refl | no ¬e=e = no (¬cong-E ¬e=e)
  where
  ¬cong-E : {t1 t2 : Type n}{e1 e2 : Env n} → ¬ (e1 ≡ e2) → ¬ (e1 × t1 ≡ e2 × t2)
  ¬cong-E ¬e=e refl = ¬e=e refl

_∈J_ : (j : Judgement n) → (js : List (Judgement n)) → Dec (j Struct.List.∈ js)
j ∈J js = j ∈? js [ _≡J_ ]

substructures : {A : Set}(Γ : Tree A) → List (∃ λ Δ → Δ ⊆ Γ)
substructures [ x ] = ⟨ [ x ] , here ⟩ ∷ []
substructures ⟨ Γ ⟩ = ⟨ ⟨ Γ ⟩ , here ⟩ ∷ []
substructures (Γ ∙ Γ')
  =  Lmap (∃map (λ z → z) thereˡ) (substructures Γ)
  ++ Lmap (∃map (λ z → z) thereʳ) (substructures Γ')

module Search-ExtractRight where
  open NLDRight
  open import NLD.DerivedRules
  open StructuralRight
  open import Struct.STree.Utils

  zip-⟫ : {Γ Δ : Env n}{A B : Type n} → List (Term Γ A) → List (Term Δ (A ⟫ B)) → List (Term (Γ ∙ Δ) B)
  zip-⟫ [] _ = []
  zip-⟫ (A ∷ As) A⟫Bs = (Lmap (λ N → A ▸ N) A⟫Bs) ++ (zip-⟫ As A⟫Bs)
  zip-⟪ : {Γ Δ : Env n}{A B : Type n} → List (Term Γ (B ⟪ A)) → List (Term Δ A) → List (Term (Γ ∙ Δ) B)
  zip-⟪ [] _ = []
  zip-⟪ (B⟪A ∷ B⟪As) As = (Lmap (λ M → B⟪A ◂ M) As) ++ (zip-⟪ B⟪As As)

  {-# TERMINATING #-}
  search : (Γ : Env n)(A : Type n)(o : List (Judgement n)) → List (Term Γ A)
  
  search-app : (A : Type n) → {Γ Δ : Env n} → List (Judgement n) → List (Term (Γ ∙ Δ) A)
  search-app A {Γ} {Δ} o = search-app' Γ Δ A (get-hypotheses (Γ ∙ Δ) A) o
    where
    search-app' : (Γ Δ : Env n) → (A : Type n) → List (Type n) → List (Judgement n) → List (Term (Γ ∙ Δ) A)
    search-app' Γ Δ A [] o = []
    search-app' Γ Δ A (H ∷ xs) o
      =  (zip-⟫ (search Γ H o) (search Δ (H ⟫ A) o))
      ++ (zip-⟪ (search Γ (A ⟪ H) o) (search Δ H o))
      ++ search-app' Γ Δ A xs o

  search-abs◃ : {Γ : Env n} → (A : Type n) → {B : Type n} → List (Judgement n) → List (Term Γ (B ⟪ A))
  search-abs◃ {Γ = Γ} A {B} o = Lmap (ƛ A ◃_) (search (Γ ∙ [ A ]) B o)
  search-abs▹ : {Γ : Env n} → (A : Type n) → {B : Type n} → List (Judgement n) → List (Term Γ (A ⟫ B))
  search-abs▹ {Γ = Γ} A {B} o = Lmap (ƛ A ▹_) (search ([ A ] ∙ Γ) B o)

  search-□i : {Γ : Env n} → (A : Type n) → List (Judgement n) → List (Term Γ (□ A))
  search-□i {Γ = Γ} A o = Lmap ⟦_⟧ᴵ (search ⟨ Γ ⟩ A o) 

  search-◇i : {Γ : Env n} → (A : Type n) → List (Judgement n) → List (Term ⟨ Γ ⟩ (◇ A))
  search-◇i {Γ = Γ} A o = Lmap (λ M → ⟨ M ⟩ᴵ) (search Γ A o)

  search-xright : (Ω : Env n){A B : Type n} → List (Judgement n)
                → List (Term Ω (A ⟪ (◇ □ B)))
  search-xright Ω {A = A}{B = B} o = search-xright' Ω A B (substructures Ω) o
    where
    search-xright' : (Ω : Env n)(A B : Type n)(p : List (∃ λ Δ → Δ ⊆ Ω))
                   → List (Judgement n) → List (Term Ω (A ⟪ (◇ □ B)))
    search-xright' Ω A B [] o = []
    search-xright' Ω A B (⟨ Δ , t ⟩ ∷ Ls) o
      =  Lmap (λ x → x-right x subeq) (search (Ω [ t ]≔ (Δ ∙ [ B ])) A o)
      ++ search-xright' Ω A B Ls o
  
  search-ax : (A : Type n) → {Γ : Env n}→ List (Term Γ A)
  search-ax A {[ A' ]} with A' ≡T A
  search-ax A {[ .A ]} | yes refl = $ ∷ []
  search-ax A {[ A' ]} | no ¬p = []
  search-ax A {_ ∙ _} = []
  search-ax A {⟨ _ ⟩} = []
  
  search Γ A o with (Γ × A) ∈J o
  search Γ A o | yes p = []
  search Γ A o | no ¬p with (Γ × A ∷ o)
  
  search [ C ]   (var x) _ | no _ | os
    = search-ax (var x)
  search (Γ ∙ Δ) (var x) _ | no _ | os
    = search-app (var x) os
  search ⟨ Γ ⟩   (var x) _ | no _ | os
    = []

  search [ C ]   (A ⟪ ◇ □ B) _ | no _ | os
    =  search-ax (A ⟪ ◇ □ B)
    ++ search-abs◃ (◇ (□ B)) os
  search (Γ ∙ Δ) (A ⟪ ◇ □ B) _ | no _ | os
    =  search-xright (Γ ∙ Δ) os
    ++ search-abs◃ (◇ (□ B)) os
    ++ search-app (A ⟪ ◇ □ B) os
  search ⟨ Γ ⟩   (A ⟪ ◇ □ B) _ | no _ | os
    =  search-abs◃ (◇ (□ B)) os
  
  search [ C ]   (A ⟫ B) _ | no _ | os
    =  search-ax (A ⟫ B)
    ++ search-abs▹ A os
  search (Γ ∙ Δ) (A ⟫ B) _ | no _ | os
    =  search-abs▹ A os
    ++ search-app (A ⟫ B) os
  search ⟨ Γ ⟩ (A ⟫ B) _ | no _ | os
    =  search-abs▹ A os

  search [ C ]   (B ⟪ A) _ | no _ | os
    =  search-ax (B ⟪ A)
    ++ search-abs◃ A os
  search (Γ ∙ Δ) (B ⟪ A) _ | no _ | os
    =  search-abs◃ A os
    ++ search-app (B ⟪ A) os
  search ⟨ Γ ⟩ (B ⟪ A) _ | no _ | os
    =  search-abs◃ A os

  search [ C ] (□ A) _ | no _ | os
    = search-ax (□ A)
    ++ search-□i A os
  search (Γ ∙ Δ) (□ A) _ | no _ | os
    = search-□i A os
    ++ search-app (□ A) os
  search ⟨ Γ ⟩ (□ A) _ | no _ | os
    = search-□i A os

  search [ C ] (◇ A) _ | no _ | os
    = search-ax (◇ A)
  search ⟨ Γ ⟩ (◇ A) _ | no _ | os
    = search-◇i A os
  search (Γ ∙ Δ) (◇ A) _ | no _ | os
    = search-app (◇ A) os
