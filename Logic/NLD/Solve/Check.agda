module NLD.Solve.Check where

open import NLD
open import Struct.Pair
open import Struct.STree.Order

private
  variable
    n m : ℕ

data RawTerm (n : ℕ) : Set where
  $      : RawTerm n
  _▸_    : RawTerm n → RawTerm n → RawTerm n
  _◂_    : RawTerm n → RawTerm n → RawTerm n
  ƛ_▹_   : Type n → RawTerm n → RawTerm n
  ƛ_◃_   : Type n → RawTerm n → RawTerm n
  ⟨_⟩ᴵ   : RawTerm n → RawTerm n
  ⟦_⟧ᴵ   : RawTerm n → RawTerm n
  ⟨_∙_⟩ᴱ : RawTerm n → RawTerm n → RawTerm n
  ⟦_⟧ᴱ   : RawTerm n → RawTerm n
  ↔_     : RawTerm n → RawTerm n

module Check-NLDRight where
  open NLDRight
  open import Struct.STree
  open import Struct.STree.Utils
  open import Struct.List using (List; _∷_; []; Lmap; _++_)

  erase : ∀ {Γ : Env n}{A : Type n} → Term Γ A → RawTerm n
  erase $ = $
  erase (N ◂ M) = (erase N) ◂ (erase M)
  erase (M ▸ N) = (erase M) ▸ (erase N)
  erase (ƛ A ▹ M) = ƛ A ▹ (erase M)
  erase (ƛ A ◃ M) = ƛ A ◃ (erase M)
  erase ⟨ M ⟩ᴵ = ⟨ erase M ⟩ᴵ
  erase ⟦ M ⟧ᴵ = ⟦ erase M ⟧ᴵ
  erase ⟨ M ∙ N [ _ ]⟩ᴱ = ⟨ erase M ∙ erase N ⟩ᴱ
  erase ⟦ M ⟧ᴱ = ⟦ erase M ⟧ᴱ
  erase ↔ M [ _ ] = ↔ erase M
  
  data Infer (Γ : Env n) : RawTerm n → Set where
    ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
    bad : {r : RawTerm n} → Infer Γ r
  
  all-?≻ʳ_ : {A : Set}(Γ : Tree A) → List (∃ λ Ω → Ω ≻ʳ Γ)
  all-?≻ʳ ((Γ ∙ Δ) ∙ ⟨ Ω ⟩)
    =  ⟨ (Γ ∙ (Δ ∙ ⟨ Ω ⟩)) , ass ⟩
    ∷  ⟨ ((Γ ∙ ⟨ Ω ⟩) ∙ Δ) , comm ⟩
    ∷  Lmap (∃map (Γ ∙_) (λ p → trans (addˡ p) ass))  (all-?≻ʳ (Δ ∙ ⟨ Ω ⟩))
    ++ Lmap (∃map (_∙ Δ) (λ p → trans (addʳ p) comm)) (all-?≻ʳ (Γ ∙ ⟨ Ω ⟩))
    ++ Lmap (∃map (_∙ ⟨ Ω ⟩) (λ p → addʳ p)) (all-?≻ʳ (Γ ∙ Δ))
  all-?≻ʳ (Γ ∙ Δ)
    =  (Lmap (∃map (_∙ Δ) (λ p → addʳ p)) (all-?≻ʳ Γ))
    ++ (Lmap (∃map (Γ ∙_) (λ p → addˡ p)) (all-?≻ʳ Δ))
  all-?≻ʳ _
    = []

  check : (Γ : Env n) → (ρ : RawTerm n) → Infer Γ ρ

  check-↔ : (Γ : Env n) → (ρ : RawTerm n) → List (∃ λ Ω → Ω ≻ʳ Γ) → Maybe (∃ λ Ω → Pair (Ω ≻ʳ Γ) (Infer Ω ρ))
  check-↔ Γ ρ [] = no
  check-↔ Γ ρ (⟨ Ω , Γ≻Ω ⟩ ∷ L) with check Ω ρ
  check-↔ Γ .(erase ω) (⟨ Ω , Γ≻Ω ⟩ ∷ L) | ok A ω = yes ⟨ Ω , (Γ≻Ω × (ok A ω)) ⟩
  check-↔ Γ ρ (⟨ Ω , Γ≻Ω ⟩ ∷ L) | bad = check-↔ Γ ρ L

  check-⟨⟩ᴱ : (Γ : Env n) → (r : RawTerm n) → (∃ λ Ψ → Pair (Ψ ⊆ Γ) (Infer Ψ r))
  check-⟨⟩ᴱ Γ r with check Γ r
  check-⟨⟩ᴱ Γ .(erase t) | ok A t = ⟨ Γ , here × ok A t ⟩
  check-⟨⟩ᴱ (Γ ∙ Δ) r    | bad with check-⟨⟩ᴱ Γ r
  check-⟨⟩ᴱ (Γ ∙ Δ) .(erase t) | bad | ⟨ Ψ , Ψ⊆Γ × ok A t ⟩ = ⟨ Ψ , (thereˡ Ψ⊆Γ × ok A t) ⟩
  check-⟨⟩ᴱ (Γ ∙ Δ) r          | bad | ⟨ Ψ , Ψ⊆Γ × bad ⟩ with check-⟨⟩ᴱ Δ r
  check-⟨⟩ᴱ (Γ ∙ Δ) .(erase t) | bad | ⟨ _ , _ × bad ⟩ | ⟨ Ψ , Ψ⊆Γ × ok A t ⟩ = ⟨ Ψ , thereʳ Ψ⊆Γ × ok A t ⟩
  check-⟨⟩ᴱ (Γ ∙ Δ) r          | bad | ⟨ _ , _ × bad ⟩ | ⟨ Ψ , Ψ⊆Γ × bad ⟩ = ⟨ Γ ∙ Δ , here × bad ⟩
  check-⟨⟩ᴱ Γ r | bad = ⟨ Γ , here × bad ⟩

  check [ A ] $ = ok A $
  check _ $ = bad

  check (Γ ∙ Δ) (N ◂ M) with check Γ N | check Δ M
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok (B ⟪ A') n | ok A m with A ≡T A'
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok (B ⟪ .A) n | ok A m | yes refl = ok B (n ◂ m)
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok (B ⟪ A') n | ok A m | no ¬p = bad
  check (Γ ∙ Δ) (.(erase n) ◂ .(erase m)) | ok _        n | ok B m = bad
  check (Γ ∙ Δ) (.(erase n) ◂ M) | ok A n | bad = bad
  check (Γ ∙ Δ) (N ◂ M) | bad | q = bad
  check _ (N ◂ M) = bad

  check (Γ ∙ Δ) (N ▸ M) with check Γ N | check Δ M
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok A n | ok (A' ⟫ B) m with A ≡T A'
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok A n | ok (.A ⟫ B) m | yes refl = ok B (n ▸ m)
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok A n | ok (A' ⟫ B) m | no ¬p = bad
  check (Γ ∙ Δ) (.(erase n) ▸ .(erase m)) | ok B n | ok _        m = bad
  check (Γ ∙ Δ) (.(erase n) ▸ M) | ok A n | bad = bad
  check (Γ ∙ Δ) (N ▸ M) | bad | q = bad
  check _ (N ▸ M) = bad

  check Γ (ƛ A ◃ M) with check (Γ ∙ [ A ]) M
  check Γ (ƛ A ◃ .(erase m)) | ok B m = ok (B ⟪ A) (ƛ A ◃ m)
  check Γ (ƛ A ◃ M) | bad = bad

  check Γ (ƛ A ▹ M) with check ([ A ] ∙ Γ) M
  check Γ (ƛ A ▹ .(erase m)) | ok B m = ok (A ⟫ B) (ƛ A ▹ m)
  check Γ (ƛ A ▹ M) | bad = bad

  check ⟨ Γ ⟩ ⟨ M ⟩ᴵ with check Γ M
  check ⟨ Γ ⟩ ⟨ .(erase m) ⟩ᴵ | ok A m = ok (◇ A) ⟨ m ⟩ᴵ
  check ⟨ Γ ⟩ ⟨ M ⟩ᴵ | bad = bad
  check   _   ⟨ M ⟩ᴵ = bad
  
  check Γ ⟦ M ⟧ᴵ with check ⟨ Γ ⟩ M
  check Γ ⟦ .(erase m) ⟧ᴵ | ok A m = ok (□ A) ⟦ m ⟧ᴵ
  check Γ ⟦ M ⟧ᴵ | bad = bad
  
  check Γ ⟨ N ∙ M ⟩ᴱ with check-⟨⟩ᴱ Γ N
  check Γ ⟨ .(erase n) ∙ M ⟩ᴱ | ⟨ Ψ , Ψ∈Γ × ok (◇ A) n ⟩ with check (Γ [ Ψ∈Γ ]≔ ⟨ [ A ] ⟩) M
  check Γ ⟨ .(erase n) ∙ .(erase m) ⟩ᴱ | ⟨ Ψ , Ψ∈Γ × ok (◇ A) n ⟩ | ok B m
    = ok B ⟨ n ∙ m [ subeq ]⟩ᴱ
  check Γ ⟨ .(erase n) ∙ M ⟩ᴱ | ⟨ Ψ , Ψ∈Γ × ok (◇ A) n ⟩ | bad = bad
  check Γ ⟨ .(erase n) ∙ M ⟩ᴱ | ⟨ Ψ , Ψ∈Γ × ok _ n ⟩ = bad
  check Γ ⟨ N ∙ M ⟩ᴱ | ⟨ Ψ , Ψ∈Γ × bad ⟩ = bad
  
  check ⟨ Γ ⟩ ⟦ M ⟧ᴱ with check Γ M
  check ⟨ Γ ⟩ ⟦ .(erase m) ⟧ᴱ | ok (□ A) m = ok A ⟦ m ⟧ᴱ
  check ⟨ Γ ⟩ ⟦ .(erase m) ⟧ᴱ | ok _ m = bad
  check ⟨ Γ ⟩ ⟦ M ⟧ᴱ | bad = bad
  check   _   ⟦ M ⟧ᴱ = bad
  check Γ (↔ M) with check-↔ Γ M (all-?≻ʳ Γ)
  check Γ (↔ .(erase ω)) | yes ⟨ Ω , Ω≻Γ × ok A ω ⟩ = ok A ↔ ω [ Ω≻Γ ]
  check Γ (↔ M) | yes ⟨ Ω , Γ≻Ω × bad ⟩ = bad
  check Γ (↔ M) | no = bad
