module NLD.DerivedRules where

open import NLD
open import Struct.STree
open import Struct.STree.Order

private
  variable
    n : ℕ
    A B : Type n

extractʳ-comm : {A : Set}{Γ Γ' Δ Ω : Tree A}(p : (Δ ∙ ⟨ Ω ⟩) ⊆ Γ)
              → (Γ ∙ Γ') ≻ʳ (((Γ [ p ]≔ Δ) ∙ Γ') ∙ ⟨ Ω ⟩)
extractʳ-ass  : {A : Set}{Γ Γ' Δ Ω : Tree A}(p : (Δ ∙ ⟨ Ω ⟩) ⊆ Γ)
              → (Γ' ∙ Γ) ≻ʳ ((Γ' ∙ (Γ [ p ]≔ Δ)) ∙ ⟨ Ω ⟩)
extractʳ-comm here = comm
extractʳ-comm (thereˡ p) = trans (addʳ (extractʳ-comm p)) comm
extractʳ-comm (thereʳ p) = trans (addʳ (extractʳ-ass  p)) comm
extractʳ-ass here = ass
extractʳ-ass (thereˡ p) = trans (addˡ (extractʳ-comm p)) ass
extractʳ-ass (thereʳ p) = trans (addˡ (extractʳ-ass  p)) ass

extractˡ-ass : {A : Set}{Γ Γ' Δ Ω : Tree A}(p : (⟨ Ω ⟩ ∙ Δ) ⊆ Γ)
           → (Γ ∙ Γ') ≻ˡ (⟨ Ω ⟩ ∙ ((Γ [ p ]≔ Δ) ∙ Γ')) 
extractˡ-comm : {A : Set}{Γ Γ' Δ Ω : Tree A}(p : (⟨ Ω ⟩ ∙ Δ) ⊆ Γ)
           → (Γ' ∙ Γ) ≻ˡ (⟨ Ω ⟩ ∙ (Γ' ∙ (Γ [ p ]≔ Δ))) 
extractˡ-comm here = comm
extractˡ-comm (thereˡ p) = trans (addˡ (extractˡ-ass p)) comm
extractˡ-comm (thereʳ p) = trans (addˡ (extractˡ-comm p)) comm
extractˡ-ass here = ass
extractˡ-ass (thereˡ p) = trans (addʳ (extractˡ-ass p)) ass
extractˡ-ass (thereʳ p) = trans (addʳ (extractˡ-comm p)) ass

subst-≻ʳ : {A : Set}{Γ Δ Ω : Tree A} → Δ ≡ Ω → Γ ≻ʳ Δ → Γ ≻ʳ Ω
subst-≻ʳ {Γ = Γ} = subst (_≻ʳ_ Γ)

subst-≻ˡ : {A : Set}{Γ Δ Ω : Tree A} → Δ ≡ Ω → Γ ≻ˡ Δ → Γ ≻ˡ Ω
subst-≻ˡ {Γ = Γ} = subst (_≻ˡ_ Γ)

trans≔∙R : {A : Set}{Δ Δ' Γ Ω Ψ : Tree A}(p : Ω ⊆ Γ)
        → (((Γ [ p ]≔ Δ') [ cong≔ p ]≔ Δ) ∙ Ψ) ≡ ((Γ [ p ]≔ Δ) ∙ Ψ)
trans≔∙R {Ψ = Ψ} p = cong (_∙ Ψ) (trans≔ p)

trans≔∙L : {A : Set}{Δ Δ' Γ Ω Ψ : Tree A}(p : Ω ⊆ Γ)
        → (Ψ ∙ ((Γ [ p ]≔ Δ') [ cong≔ p ]≔ Δ)) ≡ (Ψ ∙ (Γ [ p ]≔ Δ))
trans≔∙L {Ψ = Ψ} p = cong (Ψ ∙_) (trans≔ p)

module StructuralRight where
  open NLDRight

  postulate
    cut : {Γ Δ : Env n}{A B : Type n} → Term Γ A
        → (p : [ B ] ⊆ Γ) → Term Δ B → Term (Γ [ p ]≔ Δ) A

  add⟨□⟩ : {Γ : Env n}(p : [ B ] ⊆ Γ) → Term Γ A
       → Term (Γ [ p ]≔ ⟨ [ □ B ] ⟩) A
  add⟨□⟩ B⊆Γ Γ⊢A = cut Γ⊢A B⊆Γ ⟦ $ ⟧ᴱ

  subst-Term : {Γ Δ : Env n} → Γ ≡ Δ → Term Γ A → Term Δ A
  subst-Term {A = A} = subst (λ Γ → Term Γ A)

  addʳ⟨□⟩ : {Γ Δ : Env n}(p : (Δ ∙ [ B ]) ⊆ Γ) → Term Γ A
         → Term (Γ [ p ]≔ (Δ ∙ ⟨ [ □ B ] ⟩)) A
  addʳ⟨□⟩ {A = A} p T = subst-Term (thereʳ+cong p) (add⟨□⟩ (thereʳ+ p) T)

  add⟨□⟩⇒extractʳ : {Γ Δ : Env n}(p : (Δ ∙ [ B ]) ⊆ Γ) → Term Γ A
                 → Term ((Γ [ p ]≔ Δ) ∙ ⟨ [ □ B ] ⟩) A
  add⟨□⟩⇒extractʳ here T = addʳ⟨□⟩ here T
  add⟨□⟩⇒extractʳ (thereˡ p) T = ↔ addʳ⟨□⟩ (thereˡ p) T
    [ subst-≻ʳ (trans≔∙R (thereˡ p)) (extractʳ-comm (cong≔ p)) ]
  add⟨□⟩⇒extractʳ (thereʳ p) T = ↔ addʳ⟨□⟩ (thereʳ p) T
    [ subst-≻ʳ (trans≔∙R (thereʳ p)) (extractʳ-ass  (cong≔ p)) ]

  x-right' : {Γ Δ : Env n}(p : (Δ ∙ [ B ]) ⊆ Γ)
         → Term Γ A → Term (Γ [ p ]≔ Δ) (A ⟪ (◇ □ B))
  x-right' {B = B} p x = ƛ ◇ (□ B) ◃ ⟨ $ ∙ (add⟨□⟩⇒extractʳ p x) [ thereʳ here ]⟩ᴱ

  x-right : {A B : Type n}{Γ Δ Ω : Env n}{p : (Δ ∙ [ B ]) ⊆ Γ} → Term Γ A → Γ [ p / Δ ]≡ Ω
          → Term Ω (A ⟪ (◇ □ B))
  x-right {A = A} {B = B} {p = p} T sub = subst (λ Ω → Term Ω (A ⟪ ◇ □ B)) (subseq sub) (x-right' p T)

module StructuralLeft where
  open NLDLeft public

  postulate
    cut : {Γ Δ : Env n}{A B : Type n} → Term Γ A
        → (p : [ B ] ⊆ Γ) → Term Δ B → Term (Γ [ p ]≔ Δ) A

  add⟨□⟩ : {Γ : Env n}(p : [ B ] ⊆ Γ) → Term Γ A
       → Term (Γ [ p ]≔ ⟨ [ □ B ] ⟩) A
  add⟨□⟩ B⊆Γ Γ⊢A = cut Γ⊢A B⊆Γ ⟦ $ ⟧ᴱ

  subst-Term : {Γ Δ : Env n} → Γ ≡ Δ → Term Γ A → Term Δ A
  subst-Term {A = A} = subst (λ Γ → Term Γ A)

  addˡ⟨□⟩ : {Γ Δ : Env n}(p : ([ B ] ∙ Δ) ⊆ Γ) → Term Γ A
         → Term (Γ [ p ]≔ (⟨ [ □ B ] ⟩ ∙ Δ)) A
  addˡ⟨□⟩ {A = A} p T = subst-Term (thereˡ+cong p) (add⟨□⟩ (thereˡ+ p) T)

  add⟨□⟩⇒extractˡ : {Γ Δ : Env n}(p : ([ B ] ∙ Δ) ⊆ Γ) → Term Γ A
                 → Term (⟨ [ □ B ] ⟩ ∙ (Γ [ p ]≔ Δ)) A
  add⟨□⟩⇒extractˡ {B = B} here T = ⟦ $ ⟧ᴱ ▸ (ƛ B ▹ T)
  add⟨□⟩⇒extractˡ {B = B} (thereˡ p) T = ↔ (addˡ⟨□⟩ (thereˡ p) T)
    [ subst-≻ˡ (trans≔∙L (thereˡ p)) (extractˡ-ass  (cong≔ p)) ]
  add⟨□⟩⇒extractˡ {B = B} (thereʳ p) T = ↔ (addˡ⟨□⟩ (thereʳ p) T)
    [ subst-≻ˡ (trans≔∙L (thereʳ p)) (extractˡ-comm (cong≔ p)) ]

  x-left' : {Γ Δ : Env n}(p : ([ B ] ∙ Δ) ⊆ Γ)
         → Term Γ A → Term (Γ [ p ]≔ Δ) ((◇ □ B) ⟫ A)
  x-left' {B = B} p x = ƛ ◇ (□ B) ▹ ⟨ $ ∙ (add⟨□⟩⇒extractˡ p x) [ thereˡ here ]⟩ᴱ

  x-left : {A B : Type n}{Γ Δ Ω : Env n}{p : ([ B ] ∙ Δ) ⊆ Γ} → Term Γ A → Γ [ p / Δ ]≡ Ω
          → Term Ω ((◇ □ B) ⟫ A)
  x-left {A = A} {B = B} {p = p} T sub = subst (λ Ω → Term Ω (◇ □ B ⟫ A)) (subseq sub) (x-left' p T)
