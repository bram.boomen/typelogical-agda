module ILL.Term where

open import ILL.Base

private
  variable
    n m o : ℕ
    A B : Type n

module Associative&Commutative where

  open import Struct.List public

  Env : (n : ℕ) → Set
  Env n = List (Type n)

  private
    variable
      Γ Δ Ω : Env n

  data Term : (Γ : Env n) → (A : Type n) → Set where
    $       : Term [ A ] A
    _∙_[_]  : Term Γ (A ⊸ B) → Term Δ A → Ω ≡ Γ + Δ → Term Ω B
    ƛ_∘_[_] : (A : Type n) → Term Ω B
            → Ω ≡ [ A ] + Γ → Term Γ (A ⊸ B)

module NonAssociative&NonCommutative where

  open import Struct.Tree public

  Env : (n : ℕ) → Set
  Env n = Tree (Type n)

  private
    variable
      Γ Δ : Env n

  data Term : (Γ : Env n) → (A : Type n) → Set where
    $    : Term [ A ] A
    _∙_  : Term Γ (A ⊸ B) → Term Δ A → Term (Γ ∙ Δ) B
    ƛ_∘_ : (A : Type n) → Term ([ A ] ∙ Γ) B → Term Γ (A ⊸ B)

module Associative&Commutative-deBruijn where

  open import Struct.Vec.MaybeVec public

  Env : (n m : ℕ) → Set
  Env n m = MVec (Type n) m

  private
    variable
      Γ Δ Ω : Env n m

  data Term : (Γ : Env n m) → (A : Type n) → Set where
    $_     : (x : Fin m) → Term (𝟙 x A) A
    _∙_[_] : Term Γ (A ⊸ B) → Term Δ A → Ω ≡ Γ ⊞ Δ → Term Ω B
    ƛ_∘_   : (A : Type n) → Term (yes A ∷ Γ) B → Term Γ (A ⊸ B)
