module ILL.Solve.Infer where

open import ILL
open import ILL.Decorate
open import ILL.Solve.Check
open import IL.Solve.Infer using (morris)

private
  variable
    n m : ℕ

module Infer-Associative&Commutative where
  open Associative&Commutative
  open Check-Associative&Commutative renaming (RawTerm to ChkTerm)
  open import Struct.Pair

  Chk : (n : ℕ) → Set
  Chk n = Pair (ChkTerm n) (Env n)

  tail : {A : Set} → List A → List A
  tail [] = []
  tail (x ∷ xs) = xs

  decChk : DecTerm m n → Chk n
  decChk (A ,$ x) = $ × [ A ]
  decChk (A , N ∙ M) with decChk N | decChk M
  decChk (A , N ∙ M) | N' × Γ | M' × Δ = (N' ∙ M') × (Γ ++ Δ)
  decChk (A ,ƛ B ∘ M) with decChk M
  decChk (A ,ƛ B ∘ M) | M' × Γ = (ƛ B ∘ M') × tail Γ

  morris-chk : RawTerm m → Maybe (∃ λ n → Chk (suc n))
  morris-chk raw with decorate raw
  morris-chk raw | no = no
  morris-chk raw | yes ⟨ 0 , dec ⟩ = no -- no type variables
  morris-chk raw | yes ⟨ suc n , dec ⟩ with morris dec
  morris-chk raw | yes ⟨ suc n , dec ⟩ | no = no
  morris-chk raw | yes ⟨ suc n , dec ⟩ | yes ⟨ 0 , t ⟩ = no -- no type variables
  morris-chk raw | yes ⟨ suc n , dec ⟩ | yes ⟨ suc n' , subs ⟩
    = yes ⟨ n' , decChk (dec-t-map (apply subs) dec) ⟩

  ∃Infer : Set
  ∃Infer = ∃ λ n → (∃ λ r → (∃ λ Γ → Infer {n} Γ r))
  
  infer : (r : RawTerm (suc m)) → Maybe ∃Infer
  infer r with morris-chk r
  infer r | no = no
  infer r | yes ⟨ n , chk × Γ ⟩ = 
    yes ⟨ (suc n) , ⟨ chk , ⟨ Γ , check Γ chk ⟩ ⟩ ⟩

module Infer-Associative&Commutative-deBruijn where
  open Associative&Commutative-deBruijn
  open Check-Associative&Commutative-deBruijn renaming (RawTerm to ChkTerm)
  open import Struct.Pair

  Chk : (n m : ℕ) → Set
  Chk n m = Pair (ChkTerm n m) (Env n m)

  decChk : DecTerm m (suc n) → Chk (suc n) m
  decChk dec = decChk' dec (replicate no)
    where
    decChk' : DecTerm m n → Env n m → Chk n m
    decChk' (A ,$ x) acc = ($ x) × (acc [ x ]≔ yes A)
    decChk' (A , N ∙ M) acc with decChk' N acc
    decChk' (A , N ∙ M) _ | N' × acc with decChk' M acc
    decChk' (A , N ∙ M) _ | N' × _ | M' × acc = (N' ∙ M') × acc
    decChk' (A ,ƛ B ∘ M) acc with decChk' M (yes B ∷ acc)
    decChk' (A ,ƛ B ∘ M) _ | M' × acc = (ƛ B ∘ M') × (tail acc)

  morris-chk : RawTerm m → Maybe (∃ λ n → Chk (suc n) m)
  morris-chk raw with decorate raw
  morris-chk raw | no = no
  morris-chk raw | yes ⟨ 0 , dec ⟩ = no -- no type variables
  morris-chk raw | yes ⟨ suc n , dec ⟩ with morris dec
  morris-chk raw | yes ⟨ suc n , dec ⟩ | no = no
  morris-chk raw | yes ⟨ suc n , dec ⟩ | yes ⟨ 0 , t ⟩ = no -- no type variables
  morris-chk raw | yes ⟨ suc n , dec ⟩ | yes ⟨ suc n' , subs ⟩
    = yes ⟨ n' , decChk (dec-t-map (apply subs) dec) ⟩

  ∃Infer : (m : ℕ) → Set
  ∃Infer m = ∃ λ n → (∃ λ r → (∃ λ Γ → Infer {n} {m} Γ r))
  
  infer : (r : RawTerm (suc m)) → Maybe (∃Infer (suc m))
  infer r with morris-chk r
  infer r | no = no
  infer r | yes ⟨ n , chk × Γ ⟩ = 
    yes ⟨ (suc n) , ⟨ chk , ⟨ Γ , check Γ chk ⟩ ⟩ ⟩
