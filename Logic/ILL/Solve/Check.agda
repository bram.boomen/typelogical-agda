{-# OPTIONS --rewriting #-}

module ILL.Solve.Check where

open import ILL renaming (_+_ to _ℕ+_)

open import Data.Fin using (toℕ)
open import Data.Maybe using (Maybe) renaming (just to yes; nothing to no)
open import Struct.Pair

module Check-Associative&Commutative where

  open Associative&Commutative

  private
    variable
      n : ℕ

  data RawTerm (n : ℕ) : Set where
    $    : RawTerm n
    _∙_  : RawTerm n → RawTerm n → RawTerm n
    ƛ_∘_ : Type n → RawTerm n → RawTerm n

  erase : ∀ {Γ : Env n}{A : Type n} → (Term Γ A) → RawTerm n
  erase $ = $
  erase (N ∙ M [ split ]) = (erase N) ∙ (erase M)
  erase (ƛ A ∘ M [ split ]) = ƛ A ∘ (erase M)

  data Infer (Γ : Env n) : RawTerm n → Set where
    ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
    bad : {r : RawTerm n} → Infer Γ r

  ∃Split : Env n → Set
  ∃Split Ω = ∃₂ λ Γ Δ → Ω ≡ Γ + Δ

  splits : (Ω : Env n) → List (∃Split Ω) 
  splits [] = ⟨ [] , ⟨ [] , [-] ⟩ ⟩ ∷ []
  splits (x ∷ Ω) = mapL+ (insert x) (splits Ω)
    where
    insert : (x : Type n){Ω : Env n} → ∃Split Ω → List (∃Split (x ∷ Ω))
    insert x ⟨ Γ , ⟨ Δ , split ⟩ ⟩ = ⟨ x ∷ Γ , ⟨ Δ , o- split ⟩ ⟩
                                   ∷ ⟨ Γ , ⟨ x ∷ Δ , -o split ⟩ ⟩ ∷ []

  inserts : (Γ : Env n) → (A : Type n) → List (∃ λ Ω → Ω ≡ [ A ] + Γ)
  inserts [] A = ⟨ A ∷ [] , o- [-] ⟩ ∷ []
  inserts (x ∷ Ω) A = ⟨ (A ∷ (x ∷ Ω)) , (o- rest) ⟩
                    ∷ Lmap (∃map (x ∷_) -o_) (inserts Ω A)
    where
    rest : {Ω : Env n} → Ω ≡ [] + Ω
    rest {Ω = []} = [-]
    rest {Ω = x ∷ Ω} = -o rest

  check : (Γ : Env n) → (r : RawTerm n) → Infer Γ r

  check-splits : {Ω : Env n} → (N M : RawTerm n) → List (∃Split Ω)
               → Infer Ω (N ∙ M)
  check-splits N M [] = bad
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) with check Γ N
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) | ok A n with check Δ M
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) | ok (var x) n | ok A m = check-splits N M xs
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) | ok (A' ⊸ B) n | ok A m with A' ≡T A
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) | ok (.A ⊸ B) n | ok A m | yes refl = ok B (_∙_[_] n m split)
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) | ok (A' ⊸ B) n | ok A m | no ¬p = check-splits N M xs
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) | ok A n | bad = check-splits N M xs
  check-splits N M (⟨ Γ , ⟨ Δ , split ⟩ ⟩ ∷ xs) | bad = check-splits N M xs

  check-abs : {Ω : Env n} → (A : Type n) → (M : RawTerm n) → List (∃ λ Γ → Γ ≡ [ A ] + Ω)
            → Infer Ω (ƛ A ∘ M)
  check-abs A M [] = bad
  check-abs A M (⟨ Γ , Γ=A+Ω ⟩ ∷ xs) with check Γ M
  check-abs A .(erase m) (⟨ Γ , Γ=A+Ω ⟩ ∷ xs) | ok B m = ok (A ⊸ B) ƛ A ∘ m [ Γ=A+Ω ]
  check-abs A M (⟨ Γ , Γ=A+Ω ⟩ ∷ xs) | bad = check-abs A M xs

  check (A ∷ []) $ = ok A $
  check [] $ = bad
  check (_ ∷ _ ∷ Γ) $ = bad

  check Γ (N ∙ M) = check-splits N M (splits Γ)

  check Γ (ƛ A ∘ M) = check-abs A M (inserts Γ A)

module Check-Associative&Commutative-deBruijn where

  open Associative&Commutative-deBruijn hiding (is-𝟙)

  private
    variable
      n m o : ℕ

  data Raw : Set where
    ρ : Raw

  RawEnv : ℕ → Set
  RawEnv n = MVec Raw n

  data RawTerm (n m : ℕ) : Set where
    $_   : Fin m → RawTerm n m
    _∙_  : RawTerm n m → RawTerm n m → RawTerm n m
    ƛ_∘_ : Type n → RawTerm n (suc m) → RawTerm n m

  erase : {Γ : Env n m}{A : Type n} → Term Γ A → RawTerm n m
  erase ($ x) = $ x
  erase (N ∙ M [ _ ]) = erase N ∙ erase M
  erase (ƛ A ∘ M) = ƛ A ∘ erase M

  data Infer (Γ : Env n m) : RawTerm n m → Set where
    ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
    bad : {r : RawTerm n m} → Infer Γ r

  is-𝟙 : (Γ : Env n m) → (x : Fin m) → (A : Type n) → Maybe (Γ ≡ 𝟙 x A)
  is-𝟙 = Associative&Commutative-deBruijn.is-𝟙 _≡T_

  buildRawEnv : RawTerm n m → RawEnv m
  buildRawEnv ($ x) = 𝟘 [ x ]≔ (yes ρ)
  buildRawEnv (N ∙ M) = buildRawEnv N ⊞⊞ buildRawEnv M
  buildRawEnv (ƛ x ∘ M) = tail (buildRawEnv M)

  splitenv : (Γ : Env n m) → (N M : RawTerm n m) → Maybe (∃₂ λ L R → Γ ≡ L ⊞ R)
  splitenv Γ N M = splitRawEnv Γ (buildRawEnv N) (buildRawEnv M)
    where
    splitRawEnv : (Γ : Env n m) → (N M : RawEnv m) → Maybe (∃₂ λ L R → Γ ≡ L ⊞ R)
    splitRawEnv [] [] [] = yes ⟨ [] , ⟨ [] , [-] ⟩ ⟩
    splitRawEnv (A ∷ Γ) (no ∷ N) (no ∷ M) = no
    splitRawEnv (A ∷ Γ) (yes ρ ∷ N) (yes ρ ∷ M) = no
    splitRawEnv (A ∷ Γ) (no ∷ N) (yes ρ ∷ M) with splitRawEnv Γ N M
    ... | yes ⟨ N-env , ⟨ M-env , split ⟩ ⟩ = yes ⟨ no ∷ N-env , ⟨ A ∷ M-env , -o split ⟩ ⟩
    ... | no = no
    splitRawEnv (A ∷ Γ) (yes ρ ∷ N) (no ∷ M) with splitRawEnv Γ N M
    ... | yes ⟨ N-env , ⟨ M-env , split ⟩ ⟩ = yes ⟨ A ∷ N-env , ⟨ no ∷ M-env , o- split ⟩ ⟩
    ... | no = no

  check : (Γ : Env n m)(r : RawTerm n m) → Infer Γ r
  check Γ ($ x) with lookup Γ x
  check Γ ($ x) | yes A with is-𝟙 Γ x A
  check .(𝟙 x A) ($ x) | yes A | yes refl = ok A ($ x)
  check Γ ($ x) | yes A | no = bad
  check Γ ($ x) | no = bad

  check Γ (N ∙ M) with splitenv Γ N M
  check Γ (N ∙ M) | no = bad
  check Γ (N ∙ M) | yes ⟨ N-Env , ⟨ M-Env , split ⟩ ⟩ with check N-Env N | check M-Env M
  check Γ (.(erase n) ∙ .(erase m)) | yes ⟨ N-Env , ⟨ M-Env , split ⟩ ⟩ | ok (var x) n  | ok A m = bad
  check Γ (.(erase n) ∙ .(erase m)) | yes ⟨ N-Env , ⟨ M-Env , split ⟩ ⟩ | ok (A' ⊸ B) n | ok A m with A' ≡T A
  check Γ (.(erase n) ∙ .(erase m)) | yes ⟨ N-Env , ⟨ M-Env , split ⟩ ⟩ | ok (.A ⊸ B) n | ok A m | yes refl = ok B (_∙_[_] n m split)
  check Γ (.(erase n) ∙ .(erase m)) | yes ⟨ N-Env , ⟨ M-Env , split ⟩ ⟩ | ok (A' ⊸ B) n | ok A m | no ¬p = bad
  check Γ (.(erase n) ∙ M) | yes ⟨ N-Env , ⟨ M-Env , split ⟩ ⟩ | ok A n | bad = bad
  check Γ (N ∙ M) | yes ⟨ N-Env , ⟨ M-Env , split ⟩ ⟩ | bad | b = bad

  check Γ (ƛ A ∘ M) with check (yes A ∷ Γ) M
  check Γ (ƛ A ∘ .(erase M)) | ok B M = ok (A ⊸ B) (ƛ A ∘ M)
  check Γ (ƛ A ∘ M) | bad = bad
