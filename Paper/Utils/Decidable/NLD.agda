module Utils.Decidable.NLD where

open import Formalisms.Terms.NLD
open import Formalisms.Structures.STree
open import Base.Imports
open import Base.Decidable public

_≡T_ : {n : ℕ}(t s : Type n) → Dec (t ≡ s)
var x ≡T var y = cong-var (x ≡F y)
  where
  cong-var : {o : ℕ}{n m : Fin o} → Dec (n ≡ m) → Dec ((var n) ≡ (var m))
  cong-var (yes refl) = yes refl
  cong-var (no ¬p) = no (¬cong-var ¬p)
    where
    ¬cong-var : {o : ℕ}{n m : Fin o} → ¬ (n ≡ m) → ¬ ((var n) ≡ (var m))
    ¬cong-var ¬n=m refl = ¬n=m refl
(t ⟫ t₁) ≡T (s ⟫ s₁) = cong-⟫ (t ≡T s) (t₁ ≡T s₁)
  where
  cong-⟫ : {o : ℕ}{t s tt ss : Type o} → Dec (t ≡ s) → Dec (tt ≡ ss) → Dec ((t ⟫ tt) ≡ (s ⟫ ss))
  cong-⟫ (yes refl) (yes refl) = yes refl
  cong-⟫ (yes refl) (no ¬q) = no (¬cong-q ¬q)
    where
    ¬cong-q : {o : ℕ}{t s tt ss : Type o} → ¬ (tt ≡ ss) → ¬ ((t ⟫ tt) ≡ (s ⟫ ss))
    ¬cong-q ¬t=s refl = ¬t=s refl
  cong-⟫ (no ¬p) dq = no (¬cong-p ¬p)
    where
    ¬cong-p : {o : ℕ}{t s tt ss : Type o} → ¬ (t ≡ s) → ¬ ((t ⟫ tt) ≡ (s ⟫ ss))
    ¬cong-p ¬t=s refl = ¬t=s refl
(t ⟪ t₁) ≡T (s ⟪ s₁) = cong-⟪ (t ≡T s) (t₁ ≡T s₁)
  where
  cong-⟪ : {o : ℕ}{t s tt ss : Type o} → Dec (t ≡ s) → Dec (tt ≡ ss) → Dec ((t ⟪ tt) ≡ (s ⟪ ss))
  cong-⟪ (yes refl) (yes refl) = yes refl
  cong-⟪ (yes refl) (no ¬q) = no (¬cong-q ¬q)
    where
    ¬cong-q : {o : ℕ}{t s tt ss : Type o} → ¬ (tt ≡ ss) → ¬ ((t ⟪ tt) ≡ (s ⟪ ss))
    ¬cong-q ¬t=s refl = ¬t=s refl
  cong-⟪ (no ¬p) dq = no (¬cong-p ¬p)
    where
    ¬cong-p : {o : ℕ}{t s tt ss : Type o} → ¬ (t ≡ s) → ¬ ((t ⟪ tt) ≡ (s ⟪ ss))
    ¬cong-p ¬t=s refl = ¬t=s refl
(□ x) ≡T (□ y) = cong-□ (x ≡T y)
  where
  cong-□ : {o : ℕ}{n m : Type o} → Dec (n ≡ m) → Dec ((□ n) ≡ (□ m))
  cong-□ (yes refl) = yes refl
  cong-□ (no ¬p) = no (¬cong-□ ¬p)
    where
    ¬cong-□ : {o : ℕ}{n m : Type o} → ¬ (n ≡ m) → ¬ ((□ n) ≡ (□ m))
    ¬cong-□ ¬n=m refl = ¬n=m refl
(◇ x) ≡T (◇ y) = cong-◇ (x ≡T y)
  where
  cong-◇ : {o : ℕ}{n m : Type o} → Dec (n ≡ m) → Dec ((◇ n) ≡ (◇ m))
  cong-◇ (yes refl) = yes refl
  cong-◇ (no ¬p) = no (¬cong-◇ ¬p)
    where
    ¬cong-◇ : {o : ℕ}{n m : Type o} → ¬ (n ≡ m) → ¬ ((◇ n) ≡ (◇ m))
    ¬cong-◇ ¬n=m refl = ¬n=m refl
--&
var _ ≡T (_ ⟫ _) = no (λ ())
var _ ≡T (_ ⟪ _) = no (λ ())
(_ ⟫ _) ≡T var _ = no (λ ())
(_ ⟪ _) ≡T var _ = no (λ ())
(_ ⟪ _) ≡T (_ ⟫ _) = no (λ ())
(_ ⟫ _) ≡T (_ ⟪ _) = no (λ ())
(□ _) ≡T (_ ⟫ _) = no (λ ())
(□ _) ≡T (_ ⟪ _) = no (λ ())
(◇ _) ≡T (_ ⟫ _) = no (λ ())
(◇ _) ≡T (_ ⟪ _) = no (λ ())
(□ _) ≡T var _ = no (λ ())
(◇ _) ≡T var _ = no (λ ())
(_ ⟫ _) ≡T (□ _) = no (λ ())
(_ ⟪ _) ≡T (□ _) = no (λ ())
(_ ⟫ _) ≡T (◇ _) = no (λ ())
(_ ⟪ _) ≡T (◇ _) = no (λ ())
var _ ≡T (□ _) = no (λ ())
var _ ≡T (◇ _) = no (λ ())
(□ _) ≡T (◇ _) = no (λ ())
(◇ _) ≡T (□ _) = no (λ ())

_≡E_ : {n : ℕ}(t s : Tree (Type n)) → Dec (t ≡ s)
[ t1 ] ≡E [ t2 ] with t1 ≡T t2
[ t1 ] ≡E [ .t1 ] | yes refl = yes refl
[ t1 ] ≡E [ t2 ] | no ¬t1=t2 = no (¬cong-M ¬t1=t2)
  where
  ¬cong-M : {n : ℕ}{t1 t2 : Type n} → ¬ (t1 ≡ t2) → ¬ ([ t1 ] ≡ [ t2 ])
  ¬cong-M ¬m=m refl = ¬m=m refl
⟨ t ⟩ ≡E ⟨ s ⟩ with t ≡E s
⟨ t ⟩ ≡E ⟨ .t ⟩ | yes refl = yes refl
⟨ t ⟩ ≡E ⟨ s ⟩ | no ¬t=s = no (¬cong-⟨⟩ ¬t=s)
  where
  ¬cong-⟨⟩ : {n : ℕ}{e1 e2 : Tree (Type n)} → ¬ (e1 ≡ e2) → ¬ (⟨ e1 ⟩ ≡ ⟨ e2 ⟩)
  ¬cong-⟨⟩ ¬e=e refl = ¬e=e refl
(t1 ∙ t2) ≡E (s1 ∙ s2) with t1 ≡E s1
((t1 ∙ t2) ≡E (s1 ∙ s2)) | yes p with t2 ≡E s2
((t1 ∙ t2) ≡E (.t1 ∙ .t2)) | yes refl | yes refl = yes refl
((t1 ∙ t2) ≡E (s1 ∙ s2)) | _ | no ¬t2=s2 = no (¬cong-R ¬t2=s2)
  where
  ¬cong-R : {n : ℕ}{e1 e2 e3 e4 : Tree (Type n)} → ¬ (e3 ≡ e4) → ¬ ((e1 ∙ e3) ≡ (e2 ∙ e4))
  ¬cong-R ¬e=e refl = ¬e=e refl
((t1 ∙ t2) ≡E (s1 ∙ s2)) | no ¬t1=s1 = no (¬cong-L ¬t1=s1)
  where
  ¬cong-L : {n : ℕ}{e1 e2 e3 e4 : Tree (Type n)} → ¬ (e1 ≡ e2) → ¬ ((e1 ∙ e3) ≡ (e2 ∙ e4))
  ¬cong-L ¬e=e refl = ¬e=e refl

[ _ ] ≡E ⟨ _ ⟩ = no (λ ())
⟨ _ ⟩ ≡E [ _ ] = no (λ ())
[ _ ] ≡E (_ ∙ _) = no (λ ())
⟨ _ ⟩ ≡E (_ ∙ _) = no (λ ())
(_ ∙ _) ≡E [ _ ] = no (λ ())
(_ ∙ _) ≡E ⟨ _ ⟩ = no (λ ())
