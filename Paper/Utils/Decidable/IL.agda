module Utils.Decidable.IL where

open import Formalisms.Terms.IL
open import Base.Imports
open import Base.Decidable public

_≡Tᵇ_ : ∀ {n : ℕ}(A B : Type n) → Bool
var x ≡Tᵇ var y with x ≡F y
(var x ≡Tᵇ var y)  | no ¬p = false
(var x ≡Tᵇ var .x) | yes refl = true
var _ ≡Tᵇ (_ ⊸ _) = false
(_ ⊸ _) ≡Tᵇ var _ = false
(A ⊸ A₁) ≡Tᵇ (B ⊸ B₁) with A ≡Tᵇ B | A₁ ≡Tᵇ B₁
(A ⊸ A₁) ≡Tᵇ (B ⊸ B₁) | false | q = false
(A ⊸ A₁) ≡Tᵇ (B ⊸ B₁) | true  | q = q

_≡T_ : {n : ℕ}(t s : Type n) → Dec (t ≡ s)
var x ≡T var y = cong-var (x ≡F y)
  where
  cong-var : {o : ℕ}{n m : Fin o} → Dec (n ≡ m) → Dec ((var n) ≡ (var m))
  cong-var (yes refl) = yes refl
  cong-var (no ¬p) = no (¬cong-var ¬p)
    where
    ¬cong-var : {o : ℕ}{n m : Fin o} → ¬ (n ≡ m) → ¬ ((var n) ≡ (var m))
    ¬cong-var ¬n=m refl = ¬n=m refl
(t ⊸ t₁) ≡T (s ⊸ s₁) = cong-⊸ (t ≡T s) (t₁ ≡T s₁)
  where
  cong-⊸ : {o : ℕ}{t s tt ss : Type o} → Dec (t ≡ s) → Dec (tt ≡ ss) → Dec ((t ⊸ tt) ≡ (s ⊸ ss))
  cong-⊸ (yes refl) (yes refl) = yes refl
  cong-⊸ (yes refl) (no ¬q) = no (¬cong-q ¬q)
    where
    ¬cong-q : {o : ℕ}{t s tt ss : Type o} → ¬ (tt ≡ ss) → ¬ ((t ⊸ tt) ≡ (s ⊸ ss))
    ¬cong-q ¬t=s refl = ¬t=s refl
  cong-⊸ (no ¬p) dq = no (¬cong-p ¬p)
    where
    ¬cong-p : {o : ℕ}{t s tt ss : Type o} → ¬ (t ≡ s) → ¬ ((t ⊸ tt) ≡ (s ⊸ ss))
    ¬cong-p ¬t=s refl = ¬t=s refl
-- otherwise
var x ≡T (t ⊸ t₁) = no (λ ())
(s ⊸ s₁) ≡T var x = no (λ ())

open import Struct.Vec

_≡E_ : {n m : ℕ}(t s : Vec (Type n) m) → Dec (t ≡ s)
[] ≡E [] = yes refl
(x ∷ t) ≡E (y ∷ s) with x ≡T y
((x ∷ t) ≡E (.x ∷ s)) | yes refl with t ≡E s
((x ∷ t) ≡E (.x ∷ .t)) | yes refl | yes refl = yes refl
((x ∷ t) ≡E (.x ∷ s)) | yes refl | no ¬p = no (¬cong-E ¬p)
  where
  ¬cong-E : {n m : ℕ}{t : Type n}{e1 e2 : Vec (Type n) m} → ¬ (e1 ≡ e2) → ¬ ((t ∷ e1) ≡ (t ∷ e2))
  ¬cong-E ¬e=e refl = ¬e=e refl
((x ∷ t) ≡E (y ∷ s)) | no ¬p = no (¬cong-M ¬p)
  where
  ¬cong-M : {n m : ℕ}{t1 t2 : Type n}{e1 e2 : Vec (Type n) m} → ¬ (t1 ≡ t2) → ¬ ((t1 ∷ e1) ≡ (t2 ∷ e2))
  ¬cong-M ¬m=m refl = ¬m=m refl
