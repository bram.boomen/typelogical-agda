#+BEGIN_SRC agda2
{-# OPTIONS --rewriting #-}

module Formalisms.Structures.Vec where

open import Base.Imports
open import Data.Vec public using
  (Vec; []; [_]; _∷_; _∷ʳ_; _++_
  ; _[_]=_; _[_]≔_; lookup; tail; replicate)
open import Data.Vec.Relation.Unary.Any public using (here; there)
open import Data.Vec.Membership.Propositional public using (_∈_)
open import Data.Vec.Membership.Propositional.Properties public using (∈-lookup)
open import Agda.Builtin.Equality using (refl)

private
 variable
   n m : ℕ
#+END_SRC

#+BEGIN_SRC text
data Vec (A : Set) : ℕ → Set where
  []  : Vec A zero
  _∷_ : ∀ {n}(x : A)(xs : Vec A n) → Vec A (suc n)
#+END_SRC

The vector-module is largely imported from the Agda standard-library. Most basic operations like concatenation (=_++_=), append (=_∷_=) or append at the end (=_∷ʳ_=) do not need any introduction. What does is the interplay between =Vec= and =Fin= as this turns out to be very important to the definition of our Terms. 

#+BEGIN_SRC text
data Fin : ℕ → Set where
  zero : {n : ℕ} → Fin (suc n)
  suc  : {n : ℕ} (i : Fin n) → Fin (suc n)
#+END_SRC

A =Fin= is a bounded natural number, so we can say that a number with type =Fin 3= can have the value 1, 2 or 3. As a =Vec= is simply a length-bounded List, the combination of a =Vec n= and a =Fin n= means that we can consider the =Fin= to point at one of the indices of the =Vec= as it cannot be 0 and point at the empty =Vec= or be greater that the largest index of the =Vec= as they are bounded by the same number. This also means that we can define this membership-like type:

#+BEGIN_SRC text
data _[_]=_ {A : Set} : Vec A n → Fin n → A → Set where
  here  : {x : A}{xs : Vec A n} → x ∷ xs [ zero ]= x
  there : {i : Fin n}{x y : A}{xs : Vec A n}
        → (xs[i]=x : xs [ i ]= x) → y ∷ xs [ suc i ]= x
#+END_SRC

The type \texttt{Γ [ i ]= A} means that =A= is the =i=-th element of =Γ=. Using this mechanism, we can define =lookup=, =remove=, =insert= and =update= functions to work with Vectors. These and more functions are defined in the [[http://agda.github.io/agda-stdlib/Data.Vec.Base.html][Data.Vec.Base]] module.

We also have another membership type that does not use =Fin= for indices, which can in some situations be more workable but is essentially the same as any composition of constructors points to a certain index within the vector:

#+BEGIN_SRC text
data _∈_ {A : Set}(x : A) : Vec A n → Set where
  here  : {X : Vec A n} → x ∈ (x ∷ X)
  there : {X : Vec A n}{y : A} → x ∈ X → x ∈ (y ∷ X)
#+END_SRC

Because the two membership-relations are essentially equivalent it is not hard to translate between them:

#+BEGIN_SRC agda2
index : {A : Set}{x : A}{Γ : Vec A m} → x ∈ Γ → Fin m
index (here px) = f0
index (there x) = fs (index x)
  
A∈Γ→Γ[x]A : {A : Set}{x : A}{Γ : Vec A m} → (p : x ∈ Γ) → Γ [ index p ]= x
A∈Γ→Γ[x]A (here refl) = _[_]=_.here
A∈Γ→Γ[x]A (there p) = _[_]=_.there (A∈Γ→Γ[x]A p)
  
Γ[x]A→A∈Γ : {A : Set}{x : A}{i : Fin m}{Γ : Vec A m} → Γ [ i ]= x → x ∈ Γ
Γ[x]A→A∈Γ _[_]=_.here = here refl
Γ[x]A→A∈Γ (_[_]=_.there x) = there (Γ[x]A→A∈Γ x)

_[_]lookup : {A : Set}(Γ : Vec A m)(x : Fin m) → Γ [ x ]= (lookup Γ x)
(A ∷ Γ) [ f0 ]lookup = _[_]=_.here
(A ∷ Γ) [ fs p ]lookup = _[_]=_.there (Γ [ p ]lookup)
#+END_SRC

Lastly, we want to have a definition for the interleaving relation between vectors. To do this, we have to convince Agda that =m + (suc n)= is equal to =suc (m + n)= as this is not one of the patterns that the =_+_= function matches on. We can easily prove that this equality holds (see =+suc=), but having to manually prove this at every point of relevance gets tiresome very quickly. To get around this, we can use an Agda mechanism called rewrite rules[fn::For more on rewrite rules, I would recommend the excellent introductory article by Jesper Cockx: https://jesper.sikanda.be/posts/hack-your-type-theory.html].

#+BEGIN_SRC agda2
module Rewrite where
  open import Agda.Builtin.Equality
  open import Agda.Builtin.Equality.Rewrite

  cong : ∀ {A B : Set}{x y : A} → (f : A → B) → x ≡ y → f x ≡ f y
  cong f refl = refl
  
  suc+ : {m n : ℕ} → (suc m) + n ≡ suc (m + n)
  suc+ = refl
  
  +suc : {m n : ℕ} → m + (suc n) ≡ suc (m + n)
  +suc {zero}  = refl
  +suc {suc m} = cong suc +suc

  {-# REWRITE +suc #-}
#+END_SRC

The constructors =-o_= and =o-_= might seem cryptic, but they should read as following, =-o=: put the element in the right vector and =no= in the left, =o-=: put the element in the left vector and =no= in the right. This should make a long string of constructors like =-o -o o- -o [-]= somewhat readable.

#+BEGIN_SRC agda2
infix 10 _≡_+_
infixr 10 -o_ o-_
data _≡_+_ {A : Set} : Vec A (n + m) → Vec A n → Vec A m → Set where
  [-] : [] ≡ [] + []
  -o_ : {Γ : Vec A n}{Δ : Vec A m}{Ω : Vec A (n + m)}{x : A}
      → Ω ≡ Γ + Δ → (x ∷ Ω) ≡ Γ + (x ∷ Δ)
  o-_ : {Γ : Vec A n}{Δ : Vec A m}{Ω : Vec A (n + m)}{x : A}
      → Ω ≡ Γ + Δ → (x ∷ Ω) ≡ (x ∷ Γ) + Δ
#+END_SRC

These definitions for membership and mutation give us the tooling to use the =Vec= datatype as a structure for commutative and associative type-logics.
