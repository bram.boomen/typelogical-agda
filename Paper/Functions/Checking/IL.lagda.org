#+BEGIN_SRC agda2
module Functions.Checking.IL where
open import Base.Imports
open import Formalisms.Terms.IL
open import Utils.Decidable.IL

open IL
#+END_SRC
#+BEGIN_SRC agda2 :exports none
private
  variable
    n m : ℕ
#+END_SRC

For IL, the approach is almost entirely identical to that of Norell, for the specifics of the approach I will therefore refer to this paper but as it is useful to compare the approaches for IL, ILL, NL and NLD, I will discuss the implementation shortly.

First, we define the type of an untyped term:

#+begin_src agda2
data RawTerm n m : Set where
  $_   : Fin m → RawTerm n m
  _∙_  : RawTerm n m → RawTerm n m → RawTerm n m
  ƛ_∘_ : Type n → RawTerm n (suc m) → RawTerm n m
#+END_SRC

Note that we do have some typing information in this =RawTerm=, the reason for this is that the type of a variable that is abstracted over is not in the typing environment. For example, say that we want to type-check the following untyped identity term: $λ . 1$, which should type-check with an empty environment. When we type-check this term, we also want to return the well-typed term. But, we have an infinite number of possible typed terms that are a well-typed term for $λ . 1$ as the identity term is well-typed for all possible types. Instead we can check if the term $λ A . 1$ is well-typed for the empty environment, in which case we can return an element of type =Term [ A ] (A ⊸ A)=.

Next, we might be tempted to define our check function with the following type-signature:

#+BEGIN_SRC text
check : (Γ : Env n m) → (r : RawTerm n m) → Maybe (Term Γ A)
#+END_SRC

The problem with this is of course that we cannot predict what A is going to be for any combination of a =RawTerm= and an =Env=. Instead, we define a relation between a =Term= and a =RawTerm=. As we have no issues translating a well-typed term into an untyped term, we can define an =erase= function and an =Infer= type which defines the relation between a =RawTerm= and a =Term= which is translated to a =RawTerm=.

#+BEGIN_SRC agda2
erase : ∀ {Γ : Env n m}{A : Type n} → Term Γ A → RawTerm n m
erase $ x [ p ] = $ x
erase (N ∙ M) = (erase N) ∙ (erase M)
erase (ƛ A ∘ M) = ƛ A ∘ (erase M)

data Infer (Γ : Env n m) : RawTerm n m → Set where
  ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
  bad : {r : RawTerm n m} → Infer Γ r
#+END_SRC

To construct an element of Infer using the =ok= constructor, we need to build a =Term= =T= for which =(erase T)= is definitionally equal to the =RawTerm= that we are type-checking. This will be the goal of the =check= function. We can construct an element of =Infer= for any =RawTerm= by using the constructor =bad=.

#+BEGIN_SRC agda2
check : (Γ : Env n m)(r : RawTerm n m) → Infer Γ r
#+END_SRC

To recursively construct the =Term= that we want to return if the =RawTerm= is type-correct, we pattern match on the constructors of the =RawTerm= and in each case use the =with= syntax to get the extra information that we need to build a =Term= with the corresponding constructor.

#+BEGIN_SRC agda2
check Γ ($ x) with lookup Γ x | Γ [ x ]lookup
check Γ ($ x) | A | Γ[x]=A = ok A $ x [ Γ[x]=A ]
#+END_SRC

The first case is when the =RawTerm= is a single variable. To construct the corresponding =Term= we need a =Type= and a justification that the =Type= at the index that the variable points to is the type of the =Term=. We can get this information using =lookup= and =_[_]lookup= respectively.

#+BEGIN_SRC agda2  
check Γ (ƛ A ∘ M) with check (A ∷ Γ) M
check Γ (ƛ A ∘ .(erase m)) | ok B m = ok (A ⊸ B) (ƛ A ∘ m)
check Γ (ƛ A ∘ M) | bad = bad
#+END_SRC

When we match on an abstraction, we can see why we need the type of the abstraction, as we need to insert it into the environment to check the inner term. If the inner term is type-correct, we can use this term to return an =ok= constructor.

#+BEGIN_SRC agda2  
check Γ (N ∙ M) with check Γ N | check Γ M
check Γ (N ∙ M) | ok (B ⊸ C)  n | ok A m with A ≡T B
check Γ (N ∙ M) | ok (.A ⊸ C) n | ok A m | yes refl = ok C (n ∙ m)
check Γ (N ∙ M) | ok (B ⊸ C)  n | ok A m | no ¬p = bad
check Γ (N ∙ M) | ok (var x)  n | ok A m = bad
check Γ _ | _ | bad = bad
check Γ _ | bad | _ = bad
#+END_SRC

The application case is the most complex, as we need 3 pieces of extra information before we can construct a =Term= using the =_∙_= constructor: 
1. Both of the argument-terms need to be type-correct. This is done by calling =check= recursively.
2. The type of the functor term needs to be a complex formula, which can be done by pattern matching on the result of =check Γ N=.
3. The type of the argument of that formula needs to be equal to the type of the argument term. For this we need a function that compares types. This function =_≡T_= is defined in module =Utils.Decidable.IL=, which is not described in this paper, as the implementation is very repetitive and straightforward.
