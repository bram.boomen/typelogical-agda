#+BEGIN_SRC agda2
module Functions.Checking.NLD where
open import Base.Imports
open import Struct.Pair
open import Struct.List using (List; _∷_; []; _++_; Lmap)
open import Formalisms.Terms.NLD
open import Formalisms.Structures.STree
open import Utils.Decidable.NLD
open NLDRight

private
  variable
    n m : ℕ
#+END_SRC

The implementation of type checking NL and NLD is completely similar apart from the extra constructors that are present in NLD, I have therefore omitted the implementation of the NL =check= function. It can be found in the =NLD.Solve.Check= module. As the definitions of =RawTerm=, =erase= and =Infer= are also entirely similar to their definitions in IL and ILL, they have been omitted as well.

#+BEGIN_SRC agda2
data RawTerm (n : ℕ) : Set where
#+END_SRC
#+BEGIN_SRC agda2 :exports none
  $      : RawTerm n
  _▸_    : RawTerm n → RawTerm n → RawTerm n
  _◂_    : RawTerm n → RawTerm n → RawTerm n
  ƛ_▹_   : Type n → RawTerm n → RawTerm n
  ƛ_◃_   : Type n → RawTerm n → RawTerm n
  ⟨_⟩ᴵ   : RawTerm n → RawTerm n
  ⟦_⟧ᴵ   : RawTerm n → RawTerm n
  ⟨_∙_⟩ᴱ : RawTerm n → RawTerm n → RawTerm n
  ⟦_⟧ᴱ   : RawTerm n → RawTerm n
  ↔_     : RawTerm n → RawTerm n
#+END_SRC
#+BEGIN_SRC agda2
erase : {Γ : Env n}{A : Type n} → Term Γ A → RawTerm n
#+END_SRC
#+BEGIN_SRC agda2 :exports none
erase $ = $
erase (N ◂ M) = (erase N) ◂ (erase M)
erase (M ▸ N) = (erase M) ▸ (erase N)
erase (ƛ A ▹ M) = ƛ A ▹ (erase M)
erase (ƛ A ◃ M) = ƛ A ◃ (erase M)
erase ⟨ M ⟩ᴵ = ⟨ erase M ⟩ᴵ
erase ⟦ M ⟧ᴵ = ⟦ erase M ⟧ᴵ
erase ⟨ M ∙ N [ _ ]⟩ᴱ = ⟨ erase M ∙ erase N ⟩ᴱ
erase ⟦ M ⟧ᴱ = ⟦ erase M ⟧ᴱ
erase ↔ M [ _ ] = ↔ erase M
#+END_SRC
#+BEGIN_SRC agda2
data Infer (Γ : Env n) : RawTerm n → Set where
#+END_SRC
#+BEGIN_SRC agda2 :exports none
  ok  : (A : Type n)(t : Term Γ A) → Infer Γ (erase t)
  bad : {r : RawTerm n} → Infer Γ r
#+END_SRC

When checking an NLD =Term=, there is only one constructor for which we need to build a search-space, which is the structural construtor =↔_[_]=. As we already discussed, we can do this because the structural relation between =STree= elements is a strict ordering.

#+BEGIN_SRC agda2
all-?≻ʳ : {A : Set}(Γ : Tree A) → List (∃ λ Ω → Ω ≻ʳ Γ)
all-?≻ʳ ((Γ ∙ Δ) ∙ ⟨ Ω ⟩)
  =  ⟨ (Γ ∙ (Δ ∙ ⟨ Ω ⟩)) , ass ⟩
  ∷  ⟨ ((Γ ∙ ⟨ Ω ⟩) ∙ Δ) , comm ⟩
  ∷  Lmap (∃map (Γ ∙_) (λ p → trans (addˡ p) ass))  (all-?≻ʳ (Δ ∙ ⟨ Ω ⟩))
  ++ Lmap (∃map (_∙ Δ) (λ p → trans (addʳ p) comm)) (all-?≻ʳ (Γ ∙ ⟨ Ω ⟩))
  ++ Lmap (∃map (_∙ ⟨ Ω ⟩) (λ p → addʳ p)) (all-?≻ʳ (Γ ∙ Δ))
all-?≻ʳ (Γ ∙ Δ)
  =  Lmap (∃map (_∙ Δ) (λ p → addʳ p)) (all-?≻ʳ Γ)
  ++ Lmap (∃map (Γ ∙_) (λ p → addˡ p)) (all-?≻ʳ Δ)
all-?≻ʳ _
  = []
#+END_SRC

=all-?≻ʳ= gives us a list of all structures =Ω= that have an =Ω ≻ʳ Γ= relation with the environment =Γ= that we are checking, including the proof of this relation. Similar to the =app-check= and =abs-check= functions from ILL, we can use this list as a search space to check if there exists a structure for which the inner term is type-correct.

#+BEGIN_SRC agda2
check : (Γ : Env n) → (ρ : RawTerm n) → Infer Γ ρ

check-↔ : (Γ : Env n) → (ρ : RawTerm n) → List (∃ λ Ω → Ω ≻ʳ Γ)
        → Maybe (∃ λ Ω → Pair (Ω ≻ʳ Γ) (Infer Ω ρ))
check-↔ Γ ρ [] = no
check-↔ Γ ρ (⟨ Ω , Γ≻Ω ⟩ ∷ L) with check Ω ρ
check-↔ Γ .(erase ω) (⟨ Ω , Γ≻Ω ⟩ ∷ L) | ok A ω = yes ⟨ Ω , (Γ≻Ω × (ok A ω)) ⟩
check-↔ Γ ρ (⟨ Ω , Γ≻Ω ⟩ ∷ L) | bad = check-↔ Γ ρ L
#+END_SRC

For checking the =⟨_,_⟩ᴱ= constructor we do the same thing, except that we do not need to construct a search space, as the search-space is the environment itself. When type-checking a =RawTerm= =⟨ N , M ⟩ᴱ= with environment =Γ=, we are looking for a sub-environment of =Γ= for which =N= is type-correct. Additionally, we require that the type of the checked =N= term is =◇ A= for any =Type= =A=, but we can pattern match for this on the output of the =check-⟨⟩ᴱ= function. The =check-⟨⟩ᴱ= function traverses down the environment and returns a proof that =N= is type-correct for a certain sub-environment =Ψ= in =Γ=, as well as a proof of =Ψ ⊆ Γ=.

#+BEGIN_SRC agda2
check-⟨⟩ᴱ : (Γ : Env n) → (r : RawTerm n) → (∃ λ Ψ → Pair (Ψ ⊆ Γ) (Infer Ψ r))
check-⟨⟩ᴱ Γ r with check Γ r
check-⟨⟩ᴱ Γ .(erase t) | ok A t = ⟨ Γ , here × ok A t ⟩
check-⟨⟩ᴱ (Γ ∙ Δ) r    | bad with check-⟨⟩ᴱ Γ r
check-⟨⟩ᴱ (Γ ∙ Δ) .(erase t) | bad | ⟨ Ψ , Ψ⊆Γ × ok A t ⟩
  = ⟨ Ψ , (thereˡ Ψ⊆Γ × ok A t) ⟩
check-⟨⟩ᴱ (Γ ∙ Δ) r          | bad | ⟨ Ψ , Ψ⊆Γ × bad ⟩ with check-⟨⟩ᴱ Δ r
check-⟨⟩ᴱ (Γ ∙ Δ) .(erase t) | bad | ⟨ _ , _ × bad ⟩ | ⟨ Ψ , Ψ⊆Γ × ok A t ⟩
  = ⟨ Ψ , thereʳ Ψ⊆Γ × ok A t ⟩
check-⟨⟩ᴱ (Γ ∙ Δ) r          | bad | ⟨ _ , _ × bad ⟩ | ⟨ Ψ , Ψ⊆Γ × bad ⟩
  = ⟨ Γ ∙ Δ , here × bad ⟩
check-⟨⟩ᴱ Γ r | bad = ⟨ Γ , here × bad ⟩
#+END_SRC

With the exception of =⟨_,_⟩= and =↔_[_]=, most cases of =check= are completely predictable, so I will not comment further on them here.

#+BEGIN_SRC agda2
check [ A ] $ = ok A $
check _ $ = bad

check (Γ ∙ Δ) (N ◂ M) with check Γ N | check Δ M
check (Γ ∙ Δ) (N ◂ M) | ok (B ⟪ A') n | ok A m with A ≡T A'
check (Γ ∙ Δ) (N ◂ M) | ok (B ⟪ .A) n | ok A m | yes refl = ok B (n ◂ m)
check (Γ ∙ Δ) (N ◂ M) | ok (B ⟪ A') n | ok A m | no ¬p = bad
check (Γ ∙ Δ) (N ◂ M) | ok _        n | ok B m = bad
check (Γ ∙ Δ) (N ◂ M) | ok A n | bad = bad
check (Γ ∙ Δ) (N ◂ M) | bad | q = bad
check _ (N ◂ M) = bad

check (Γ ∙ Δ) (N ▸ M) with check Γ N | check Δ M
check (Γ ∙ Δ) (N ▸ M) | ok A n | ok (A' ⟫ B) m with A ≡T A'
check (Γ ∙ Δ) (N ▸ M) | ok A n | ok (.A ⟫ B) m | yes refl = ok B (n ▸ m)
check (Γ ∙ Δ) (N ▸ M) | ok A n | ok (A' ⟫ B) m | no ¬p = bad
check (Γ ∙ Δ) (N ▸ M) | ok B n | ok _        m = bad
check (Γ ∙ Δ) (N ▸ M) | ok A n | bad = bad
check (Γ ∙ Δ) (N ▸ M) | bad | q = bad
check _ (N ▸ M) = bad

check Γ (ƛ A ◃ M) with check (Γ ∙ [ A ]) M
check Γ (ƛ A ◃ .(erase m)) | ok B m = ok (B ⟪ A) (ƛ A ◃ m)
check Γ (ƛ A ◃ M) | bad = bad

check Γ (ƛ A ▹ M) with check ([ A ] ∙ Γ) M
check Γ (ƛ A ▹ .(erase m)) | ok B m = ok (A ⟫ B) (ƛ A ▹ m)
check Γ (ƛ A ▹ M) | bad = bad

check ⟨ Γ ⟩ ⟨ M ⟩ᴵ with check Γ M
check ⟨ Γ ⟩ ⟨ .(erase m) ⟩ᴵ | ok A m = ok (◇ A) ⟨ m ⟩ᴵ
check ⟨ Γ ⟩ ⟨ M ⟩ᴵ | bad = bad
check   _   ⟨ M ⟩ᴵ = bad

check Γ ⟦ M ⟧ᴵ with check ⟨ Γ ⟩ M
check Γ ⟦ .(erase m) ⟧ᴵ | ok A m = ok (□ A) ⟦ m ⟧ᴵ
check Γ ⟦ M ⟧ᴵ | bad = bad

check ⟨ Γ ⟩ ⟦ M ⟧ᴱ with check Γ M
check ⟨ Γ ⟩ ⟦ .(erase m) ⟧ᴱ | ok (□ A) m = ok A ⟦ m ⟧ᴱ
check ⟨ Γ ⟩ ⟦ .(erase m) ⟧ᴱ | ok _ m = bad
check ⟨ Γ ⟩ ⟦ M ⟧ᴱ | bad = bad
check   _   ⟦ M ⟧ᴱ = bad
#+END_SRC

In addition to checking whether =N= is type-correct for a sub-environment =Ψ= of =Γ= and that the type of this =Term= is =◇ A= we also need a proof that =M= is type-correct for =Γ= where =Ψ= is replaced with the structure =⟨ [ A ] ⟩=. As =check-⟨⟩ᴱ= returns an element of =Ψ ⊆ Γ=, we can use the =_[_]≔_= function to check =M=. This gives us all the information we need to build an element of =Term= with the constructor =⟨_,_[_]⟩=.

#+BEGIN_SRC agda2
check Γ ⟨ N ∙ M ⟩ᴱ with check-⟨⟩ᴱ Γ N
check Γ ⟨ N ∙ M ⟩ᴱ | ⟨ Ψ , Ψ⊆Γ × ok (◇ A) n ⟩ with check (Γ [ Ψ⊆Γ ]≔ ⟨ [ A ] ⟩) M
check Γ ⟨ N ∙ M ⟩ᴱ | ⟨ Ψ , Ψ⊆Γ × ok (◇ A) n ⟩ | ok B m
  = ok B ⟨ n ∙ m [ subeq ]⟩ᴱ
check Γ ⟨ N ∙ M ⟩ᴱ | ⟨ Ψ , Ψ⊆Γ × ok (◇ A) n ⟩ | bad = bad
check Γ ⟨ N ∙ M ⟩ᴱ | ⟨ Ψ , Ψ⊆Γ × ok _ n ⟩ = bad
check Γ ⟨ N ∙ M ⟩ᴱ | ⟨ Ψ , Ψ⊆Γ × bad ⟩ = bad
#+END_SRC

The all-?≻ʳ function gives us all the information needed to build a =Term= using the constructor =↔_[_]=:

#+BEGIN_SRC agda2
check Γ (↔ M) with check-↔ Γ M (all-?≻ʳ Γ)
check Γ (↔ .(erase ω)) | yes ⟨ Ω , Ω≻Γ × ok A ω ⟩ = ok A ↔ ω [ Ω≻Γ ]
check Γ (↔ M) | yes ⟨ Ω , Γ≻Ω × bad ⟩ = bad
check Γ (↔ M) | no = bad
#+END_SRC
