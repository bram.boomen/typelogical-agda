#+BEGIN_SRC agda2
module Functions.TypeChecking where
#+END_SRC

One of the first practical issues we might want to tackle with our defined types is type-checking. When type-checking, we take a well-formed, untyped term or proof and an typing environment and check whether that combination gives us a well-typed term or proof. For example, in ILL the term $\lambda A . (2\ 1)$ does type-check given the environment $[ A \lolli B ]$, but does not given the environment $[ B \lolli A ]$. We would, however, like to have some more information than a simple yes or no answer out of our type-checking. The following approach, described by Norell and Chapman, gives us a type-checking function that also gives us a proof that the combination of the untyped term and the environment is a well-typed term, by returning the well-typed term if it exists citep:norell2008.

* IL

#+INCLUDE: Checking/IL.lagda.org

* ILL

#+INCLUDE: Checking/ILL.lagda.org

* NL / NLD

#+INCLUDE: Checking/NLD.lagda.org
